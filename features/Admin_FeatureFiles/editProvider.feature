Feature: This feature file describes Edit Provider page to the Dayamed Portal - Admin
@tc_01
Scenario: Edit provider with blanck data only in required fields then verify the error messages
Given navigate to the QA Dayamed web portal
When I'm on edit provider page verify the disabled fields then leave the all required fields as empty and click on submit
Then verify the error messages for which we leave as empty

@tc_02
Scenario: Enter the invalid data in all fields and click on submit then verify the error message
When I'm on edit provider page enter the invalid data in all fields and click on submit button
Then verify the error messages on edit page

@tc_03
Scenario: Edit provier with valid data in all fields
When I'm on edit provider page Enter valid data in all fields and click on submit
Then verify the success message after sumbit 

@tc_04
Scenario: Edit the provider with the valid data in required fields only
Given navigate to the QA Dayamed web portal
When I'm on edit provider page Enter valid data in required fields and click on submit then verify the success message
 










