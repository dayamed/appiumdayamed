Feature: This feature file describes home page to the Dayamed Portal - Admin
@All_Fields
Scenario: Login to the Dayamed portal with valid Admin credententials and verify the all fileds displayed on the page
Given Navigate to the Dayamed QA portal
When I'm on homepage verify the all fields on page

@search_provider
Scenario:Enter provider name on search provider and verify whether provider card displayed 
Given Navigate to the Dayamed QA portal
When enter the provider name and verify the page
@LoadMore_Providers
Scenario: Click on Load more Providers and verify the count displayed
Given Navigate to the Dayamed QA portal
When click on load more Providers then verify the count

    