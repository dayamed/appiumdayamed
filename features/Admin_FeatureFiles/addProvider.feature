Feature: This feature file describes Add Provider page to the Dayamed Portal - Admin
@tc_1
Scenario: Add provider with blanck data only in required fields then verify the error messages
Given navigate to the QA Dayamed web portal
When leave the all required fields as empty and click on submit
Then verify the error messages 

@tc_2
Scenario: Enter the invalid data in all fields and click on submit then verify the error messages
Given navigate to the QA Dayamed web portal
When enter the invalid data in all fields and click on submit button
Then verify the error messages for all fields

@tc_3
Scenario: Add provier with valid data in all fields
Given navigate to the QA Dayamed web portal
When Enter valid data in all fields and click on submit
#Then verify the success message and verify

@tc_4
Scenario: Add the provider with the valid data in required fields only
Given navigate to the QA Dayamed web portal
When Enter valid data in required fields and click on submit then verify the success message
 
@tc_5
Scenario: Search for added provider in the list and search for the selected pharmacy patient 
Given navigate to the QA Dayamed web portal
When Enter the provider name which added and click on view patients
Then Enter the patient name and verify whether patient displayed








