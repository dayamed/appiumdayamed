Feature: This feature file describes Forgot Password page to the Dayamed Portal - Admin

@Invalid_Email
Scenario: Reset password with Invalid Email and verify the error messages
Given I navigate to the Dayamed QA web portal
When verify the all fields on the page
Then Enter the invalid Email and click on submit then verify the error messages 
 
# @valid_Email
# Scenario: Reset password with valid Email and verify the success message
# Given I navigate to the Dayamed QA web portal
# When verify the all fields on the page
# Then Enter the valid Email and click on submit then verify the the success message 
