Feature: This feature file describes Patients list to the Dayamed Portal - Admin

@TC_01
Scenario: Navigate to the patient list under provider, verify the view and lazy loading
Given navigate to the QA Dayamed web portal
When Click on view patient under any provider
Then verify the view and lazy loading
@TC_02
Scenario: search for any patient and test the delete functionality
When Enter patient name then verify whether it is displayed and click on delete button
Then verify the popup
@TC_03
Scenario: Edit patient with blanck data only in required fields then verify the error messages
When I'm on edit patient page verify the disabled fields then leave the all required fields as empty and click on submit
Then verify the error messages for required fields on edit patient page

@TC_04
Scenario: Edit with the invalid data in all fields of edit patient page and click on submit then verify the error message
When I'm on edit patient page enter the invalid data in all fields and click on submit button
Then verify the error messages on edit patient page

@TC_05
Scenario: Edit patient with valid data in all fields
When I'm on edit patient page Enter valid data in all fields and click on submit
#Then verify the success message after sumbit patient details

@TC_06
Scenario: Edit the patient with the valid data in required fields only
Given navigate to the QA Dayamed web portal
When Click on view patient under any provider
When I'm on edit patient page Enter valid data in required fields and click on submit then verify the success message
@TC_07
Scenario: Navigate to the patient edit page and add physician with blanck data in all fields
Given navigate to the QA Dayamed web portal
When Click on view patient under any provider
Then I'm on edit patient page click on add physician and click on submit without entering any data
And verify the error messages on add physician page

@TC_08
Scenario: Enter invalid data in all fields of add physician then and on submit
When I'm on edit patient page click on add physician, enter invalid data in all fields then verify the error messages

@TC_09
Scenario: Add physicia with valid data in all fields
When I'm on add physician page enter the valid data in all the fields and click on submit then verify the success message

@TC_10
Scenario: Enter valid data in required fields of add physician then click on sumbit
When I'm on add physician page enter the valid data in required fields and click on submit then verify the success message





