Feature: This feature file describes Login to the Dayamed Portal - Admin

@login_invalid
Scenario: login with the invalid credendential then verify the page
Given I navigate to the Dayamed QA portal
When I'm on login page verify the fileds
Then enter the invalid credentials,click on login and verify the error messages

@login_valid
Scenario: login with the valid credendential then verify the page
Given I navigate to the Dayamed QA portal
When I'm on login page verify the fileds
Then enter valid credentials and click on login then verify the homepage
@password_visibility_icon
Scenario: enter the password and verify the the visibity when icon visibility On and Off
Given I navigate to the Dayamed QA portal
When enter the password and verify the password visibily icon functionality.
@forgot_password
Scenario: verify forgotpassword link
Given I navigate to the Dayamed QA portal
When Click on forgot password link and verify the page title.







# @valid&invalid
# Scenario: Login with valid and invalid credentials and verify the page
# Given I navigate to the Dayamed QA portal
# When I'm on login page verify the fileds
# Then enter the invalid credentials,click on login and verify the error messages
# Then enter valid credentials and click on login then verify the homepage

# @TC_01
# Scenario:Login without any credentials, Both username and password as empty
# Given I navigate to the Dayamed QA portal
# Then verify the login page
# And I click on the login button
# Then error messages are displayed
# Then verify the error messages under both fields.

# @TC_02
# Scenario: Login by entering valid username and leave the password as empty
# Given I navigate to the Dayamed QA portal
# Then verify the login page
# When enter valid username
# And I click on the login button
# Then verify the error message under password field.
# @TC_03
# Scenario: Login by entering valid  password  and leave the username  as empty
# Given I navigate to the Dayamed QA portal
# Then verify the login page
# When enter valid password
# And I click on the login button
# Then verify the error message under username.
# @TC_04
# Scenario:Provide invalid data in both user name and password fields
# Given I navigate to the Dayamed QA portal
# Then verify the login page
# Then enter the invalid username and password
# And click on login
