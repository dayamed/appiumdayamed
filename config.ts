

// An example configuration file
exports.config = {
  // The address of a running selenium server.
  //seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect:true,
  
  // set to "custom" instead of cucumber.
  framework: 'custom',
  // path relative to the current config file
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome'
   
  },
  specs: ['../features/Admin_FeatureFiles/patientReports.feature'],
  
cucumberOpts: {
  // require step definitions
// tags:"@TC_07",
  format:'json:Output/cucumberreport.json',
  require: [
    './stepsDefinitions/Admin_Module/*.js' // accepts a glob 
  ]
},
plugins: [{
  package: 'protractor-multiple-cucumber-html-reporter-plugin',
  options:{
      // read the options part
      automaticallyGenerateReport: true,
      removeExistingJsonReportFile: true
  }
}],
  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
  },

  onPrepare: function() {
    
    // Load chai assertions
    const chai = require('chai');
    const chaiAsPromised = require('chai-as-promised');
   
    // Load chai-as-promised support
    chai.use(chaiAsPromised);
   
    // Initialise should API (attaches as a property on Object)
    chai.should();
   }
   
};