import { by, element, ElementFinder } from "protractor";

export class reports
{
    notesTreatement_Card:ElementFinder;
    location_card:ElementFinder;
    adherenceReport_card:ElementFinder;
    medicationReport_card:ElementFinder;
    weeklyAdherence_ReportCard:ElementFinder;
    profileName:ElementFinder;
    adherence_OnProfileCard:ElementFinder;
    notification_icon:ElementFinder;
    constructor()
    {

        this.notesTreatement_Card=element(by.xpath('//img[@alt="NotesAndTreatments"]'));
        this.location_card=element(by.xpath('//img[@alt="Location"]'));
        this.adherenceReport_card=element(by.xpath('//img[@alt="Adherence"]'));
        this.medicationReport_card=element(by.xpath('//img[@alt="Medication Report"]'));
        this.weeklyAdherence_ReportCard=element(by.xpath('//img[@alt="Weekly Adherence Report"]'));
        this.profileName=element(by.xpath('//mat-card-title[@class="title mat-card-title"]'));
        this.adherence_OnProfileCard=element(by.xpath('//mat-card-subtitle[@class="current mat-card-subtitle"]'));
        this.notification_icon=element(by.xpath('//img[@class="notif-icon"]'));
        
    }

}
export class notesTreatement 
{
    popup: ElementFinder;
    patientName: ElementFinder;
    overallAdherence: ElementFinder;
    close_icon: ElementFinder;
    constructor()
    {
        this.popup=element(by.xpath('//mat-dialog-container[@role="dialog"]'));
        this.patientName=element(by.xpath('//span[@mattooltip="Patient Name"]'));
        this.overallAdherence=element(by.xpath('//span[@mattooltip="Overall Adherence"]'));
        this.close_icon=element(by.xpath('//mat-icon[text()="clear"]'));
        
    }
}
export class location
{
popup: ElementFinder;
mapView_Btn: ElementFinder;
tableView_Btn: ElementFinder;
mapView: ElementFinder;
tableView: ElementFinder;
close_icon: ElementFinder;
constructor()
{
    this.popup=element(by.xpath('//div[@class="postion-relative"]'));
    this.mapView_Btn=element(by.xpath('//div[text()="Map view"]'));
    this.tableView_Btn=element(by.xpath('//div[text()="Table view"]'));
    this.mapView=element(by.xpath('//div[@id="eventMap"]'));
    this.tableView=element(by.xpath('//mat-tab-body[@class="mat-tab-body ng-tns-c51-1082 ng-star-inserted mat-tab-body-active"]'));
    this.close_icon=element(by.xpath('//mat-icon[text()="clear"]'));
    
}
}
export class adherenceReport{
    patientName: ElementFinder;
    adherence: ElementFinder;
    close_icon: ElementFinder;
    year_dropdown: ElementFinder;
    month_2021: ElementFinder;
    month_2020: ElementFinder;
    month_dropdown: ElementFinder;
    nov: ElementFinder;
    october: ElementFinder;
    dec: ElementFinder;
    adherence_gragh: ElementFinder;
    noData_msg: ElementFinder;
    exportPDF: ElementFinder;
    logo: ElementFinder;
    constructor()
    {
        this.patientName=element(by.xpath('//span[@mattooltip="Patient Name"]'));
        this.adherence=element(by.xpath('//span[@mattooltip="Overall Adherence"]'));
        this.close_icon=element(by.xpath('//mat-icon[text()="clear"]'));
        this.year_dropdown=element(by.xpath('//mat-select[@formcontrolname="year"]'));
        this.month_2021=element(by.xpath('//span[text()=" 2021 "]'));
        this.month_2020=element(by.xpath('//span[text()=" 2020 "]'));
        this.month_dropdown=element(by.xpath('//mat-select[@formcontrolname="month"]'));
        this.nov=element(by.xpath('//div//mat-option[11]'));
        this.october=element(by.xpath('//div//mat-option[10]'));
        this.dec=element(by.xpath('//div//mat-option[12]'));
        this.adherence_gragh=element(by.xpath('//canvas[@id="line-chart"]'));
        this.noData_msg=element(by.xpath('//p[text()=" No data available. "]'));
        this.exportPDF=element(by.xpath('//img[@mattooltip="Export in PDF"]'));
        this.logo=element(by.xpath('//span[@class="powered-by"]/parent::*'));

    }
}