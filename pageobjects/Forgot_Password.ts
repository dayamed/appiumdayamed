import { by, element, ElementFinder } from "protractor";


export class Forgot_Password{
forgotpassword_PageHeading:ElementFinder;
logo:ElementFinder;
Email_TextField:ElementFinder;
submitBtn:ElementFinder;
Goback_toLogin:ElementFinder;
success_ToastMessage:ElementFinder;
invalid_ErrorMessage:ElementFinder;
email_ErrorMessage:ElementFinder;
notExist_ToastMessage:ElementFinder;
    constructor()
    {
this.forgotpassword_PageHeading=element(by.xpath('//mat-card-title[@class="forgot-pswd mat-card-title"]'));
this.logo=element(by.xpath('//img[@class="logo"]'));
this.Email_TextField=element(by.xpath('//input[@placeholder="Email"]'));
this.submitBtn=element(by.xpath('//button[@type="submit"]'));
this.Goback_toLogin=element(by.xpath('//a[@class="back-to-login"]'));
this.success_ToastMessage=element(by.xpath('//div[@id="toast-container"]'));
this.invalid_ErrorMessage=element(by.xpath('//span[text()="Please enter a valid Email."]'));
this.email_ErrorMessage=element(by.xpath('//span[text()="Please enter your Email."]'));
this.notExist_ToastMessage=element(by.xpath('//div[@class="toast-title"]'));
    }
}