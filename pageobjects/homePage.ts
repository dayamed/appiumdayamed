import { by, element, ElementArrayFinder, ElementFinder } from "protractor";

export class homePage{

    heading:ElementFinder;
    menuBtn:ElementFinder;
    logo:ElementFinder;
    search_provider:ElementFinder;
    provider_card:ElementFinder;
    addProvider_fromProviderPage:ElementFinder;
    account_circle:ElementFinder;
    help:ElementFinder;
    account_ChangePassword:ElementFinder;
    Logout:ElementFinder;
    ProviderDropdown:ElementFinder;
    provider_List:ElementFinder;
    Add_Provider:ElementFinder;
    cargiverDropDown:ElementFinder;
    cargiver_list:ElementFinder;
    AddCaregiver:ElementFinder;
    Administration_DropDown:ElementFinder;
    PharmacyList:ElementFinder;
    RelationsManagement:ElementFinder;
    Terms_Conditions:ElementFinder;
    Reports_DropDown:ElementFinder;
    Adherence:ElementFinder;
    AccountSettings_DropDown:ElementFinder;
    ChangePassword:ElementFinder;
    account_Logout:ElementFinder;
    loadMore_ProvidersBtn:ElementFinder;
    pagination:ElementFinder;
    providers_count:ElementArrayFinder;
   
    constructor()
    {
      this.heading=element(by.xpath('//span[text()="Providers"]'));
      this.menuBtn=element(by.xpath('//button[@class="topNavButton-Menu mat-icon-button"]'));
      this.logo=element(by.xpath('//img[@alt="Dayamed logo"]'));
      this.search_provider=element(by.xpath('//*[@id="mat-input-2"]'));
      this.provider_card=element(by.xpath(' //p[text()=" Siva11, Test "]'));
      this.addProvider_fromProviderPage=element(by.xpath('//span[@class="mat-button-wrapper" and text()="Add Provider"]'));
      this.account_circle=element(by.xpath('//mat-icon[text()="account_circle"]'));
      this.help=element(by.xpath('//span[text()="Help"]'));
      this.account_ChangePassword=element(by.xpath('//span[text()="Change Password"]'));
      this.account_Logout=element(by.xpath('//span[text()="Logout"]'));
      this.ProviderDropdown=element(by.xpath('//span[text()="Providers"and @class="list-item ng-star-inserted"]'));
      this.provider_List=element(by.xpath('//div[text()="Provider List"]'));
      this.Add_Provider=element(by.xpath('//div[text()="Add Provider"]'));
      this.cargiverDropDown=element(by.xpath('//span[text()="Caregiver"]'));
      this.cargiver_list=element(by.xpath('//div[text()="Caregiver List"]'));
      this.AddCaregiver=element(by.xpath('//div[text()="Add Caregiver"]'));
      this.Administration_DropDown=element(by.xpath('//span[text()="Administration"]'));
      this.PharmacyList=element(by.xpath('//div[text()="Pharmacy List"]'));
      this.RelationsManagement=element(by.xpath('//div[text()="Relations Management"]'));
      this.Terms_Conditions=element(by.xpath('//div[text()="Terms and Conditions"]'));
      this.Reports_DropDown=element(by.xpath('//span[text()="Reports"]'));
      this.Adherence=element(by.xpath('//div[text()="Adherence"]'));
      this.AccountSettings_DropDown=element(by.xpath('//span[text()="Account Settings"]'));
      this.ChangePassword=element(by.xpath('//div[text()="Change Password"]'));
      this.Logout=element(by.xpath('//div[text()="Logout"]'));
     // this.loadMore_ProvidersBtn=element(by.xpath('//button[@class="load-more mat-stroked-button"]'));
      this.loadMore_ProvidersBtn=element(by.xpath('/html/body/app-root/app-admin/app-layout/div/mat-sidenav-container/mat-sidenav-content/div/app-providers-list/mat-card/div/app-load-more/div[2]/button'));

      this.pagination=element(by.xpath('//span[@class="pagination"]'));
      this.providers_count=element.all(by.xpath('//mat-card[@class="user-card mat-card ng-star-inserted"]'));


    }
}