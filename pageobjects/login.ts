import { ElementFinder,element,by} from "protractor";

export class login{
    heading:ElementFinder
    logo:ElementFinder;
    userName:ElementFinder;
    password:ElementFinder;
    loginButton:ElementFinder;
    visibiltyBtn:ElementFinder;
    forgot_password:ElementFinder;
    error_msg_bothinvalid:ElementFinder;
    toast_mesg_forInvalidData:ElementFinder;
    invalid_username_errorMsg:ElementFinder
    Not_secure:ElementFinder;
    Adv_Btn:ElementFinder;
    Proceed_Btn:ElementFinder;
    Login_Err:ElementFinder;
    username_error_message:ElementFinder;
    password_error_message:ElementFinder;
    appliedClaims:ElementFinder;
    assignClaimPage:ElementFinder;
    adminPage:ElementFinder;
    

    constructor(){
        this.heading=element(by.xpath('//mat-card-title[@class="login mat-card-title"]'));
        this.logo=element(by.xpath('//img[@class="logo"]'));
        this.userName=element(by.xpath("//input[@placeholder='Username']"));
        this.password=element(by.xpath("//input[@placeholder='Password']"));
        this.loginButton=element(by.xpath("//button/span[contains(text(), 'Login')]"));
        this.visibiltyBtn=element(by.xpath('//mat-icon[text()="visibility_off"]'));
        this.forgot_password=element(by.xpath('//a[@class="forgot-pswd"]'));
        this.error_msg_bothinvalid=element(by.xpath('//p[text()="Username or Password incorrect."]'));
        this.toast_mesg_forInvalidData=element(by.xpath('//div[@id="toast-container"]'));
        this.invalid_username_errorMsg=element(by.xpath('//span[text()="Please enter a valid Email."]'));
        this.Not_secure=element(by.xpath("//*[@id='main-message']/h1"));
        this.Adv_Btn=element(by.id('details-button'));
        this.Proceed_Btn=element(by.id('proceed-link'));
        this.Login_Err=element(by.xpath("//mat-form-field//mat-error"));

        this.username_error_message=element(by.xpath('//span[text()="Please enter your Username."]'));
        this.password_error_message=element(by.xpath('//span[text()="Please enter your Password."]'));

        this.appliedClaims=element(by.xpath("//*[contains(text(),'Applied Claims')]"));
        this.assignClaimPage=element(by.xpath("//*[contains(text(),'Assigned Claims')]"));
        this.adminPage=element(by.xpath("//mat-nav-list/app-menu[2]/a"));
        
    }
}
