import { browser, by, element, ElementArrayFinder, ElementFinder } from "protractor";
//import addProviderdata from "../../appiumdayamed/TestData/addProvider.json"

export class editProvider
{   edit_icon:ElementFinder;
    pageHeading:ElementFinder;
    firstName:ElementFinder;
    lastName:ElementFinder;
    nickName:ElementFinder;
    DOB:ElementFinder;
    month_yearBtn:ElementFinder;
    Previos20yearsBtn:ElementFinder;
    select_Year:ElementFinder;
    select_Month:ElementFinder;
    select_Date:ElementFinder;
    gender_Female:ElementFinder;
    gender_Other:ElementFinder;
    gender_Male:ElementFinder;
    ethnicity_dropdown:ElementFinder;
    select_ethnicity:ElementFinder;
    contryCode_dropdown:ElementFinder;
    USA_code:ElementFinder;
    IND_code:ElementFinder;
    mobile_num:ElementFinder;
    Email:ElementFinder;
    address:ElementFinder;
    city:ElementFinder;
    state:ElementFinder;
    country:ElementFinder;
    zip_Code:ElementFinder;
    phoneType_dropdown:ElementFinder;
    select_ios:ElementFinder;
    select_android:ElementFinder;
    select_web:ElementFinder;
    specilization:ElementFinder;
    licence:ElementFinder;
    isPharmacyAdmin_switch:ElementFinder;
    isPharmacist_switch:ElementFinder;
    isNursePractitioner_switch:ElementFinder;
    upload_ImgBtn:ElementFinder;
    submitBtn:ElementFinder;
    cancelBtn:ElementFinder;
    pharmacy_dropdown:ElementFinder;
    select_pharmacy:ElementFinder;
    lname_errorMsg:ElementFinder;
    fname_ErrorMsg:ElementFinder
    mobileNum_errorMsg:ElementFinder;
    email_errorMsg:ElementFinder;
    address_errorMsg:ElementFinder;
    zipcode_error:ElementFinder;
    licence_error:ElementFinder;
    pharmacy_error:ElementFinder;
    nickName_error:ElementFinder;
    city_error:ElementFinder;
    State_errorMsg:ElementFinder;
    Country_errorMsg:ElementFinder;
    invalidlname_errorMsg:ElementFinder;
    invalidfname_ErrorMsg:ElementFinder;
    invalidmobileNum_errorMsg:ElementFinder;
    invalidemail_errorMsg:ElementFinder;
    invalidaddress_errorMsg:ElementFinder;
    invalidzipcode_error:ElementFinder;
    invalidnickName_error:ElementFinder
    invalidcity_error:ElementFinder;
    invalidState_errorMsg:ElementFinder;
    invalidCountry_errorMsg:ElementFinder;
    success_msg:ElementFinder;
    provider_card:ElementFinder;
    viewPatients:ElementFinder;
    searchPatient:ElementFinder;
    petientCard:ElementFinder;

 constructor()
 {


 this.edit_icon= element(by.xpath('//mat-icon[text()="edit"]'));
 this.petientCard=element(by.xpath('//td//span[text()=" Vilasagar, Anusha "]'));
 this.viewPatients=element(by.xpath('//*[@id="mat-input-21"]'));
 this.pageHeading=element(by.xpath('//mat-card-title[@class="title mat-card-title"]'));
 this.firstName=element(by.xpath('//input[@placeholder="First Name"]'));
 this.lastName=element(by.xpath('//input[@placeholder="Last Name"]'));
 this.nickName=element(by.xpath('//input[@placeholder="Nick Name"]'));
 this.DOB=element(by.xpath('//input[@placeholder="Date of Birth"]'));
 this.month_yearBtn=element(by.xpath('//button[@aria-label="Choose month and year"]'));
 this.Previos20yearsBtn=element(by.xpath('//button[@aria-label="Previous 20 years"]'));
 this.select_Year=element(by.xpath('//td[@aria-label="1998"]'));
 this.select_Month=element(by.xpath('//div[text()="DEC"]'));
 this.select_Date=element(by.xpath('//div[text()="4"]'));
 this.gender_Female=element(by.xpath('//mat-radio-button[@value="female"]'));
 this.gender_Other=element(by.xpath('//mat-radio-button[@value="other"]'));
 this.gender_Male=element(by.xpath('//mat-radio-button[@value="male"]'));
 this.ethnicity_dropdown=element(by.xpath('//mat-select[@aria-label="Ethnicity"]'));
 this.select_ethnicity=element(by.xpath('//span[text()=" Native American "]'));
 this.contryCode_dropdown=element(by.xpath('//mat-select[@aria-label="Country Code"]'));
 this.USA_code=element(by.xpath('//mat-option[1]'));
 this.IND_code=element(by.xpath('//mat-option[2]'));
 this.mobile_num=element(by.xpath('//input[@placeholder="Mobile Number"]'));
 this.Email=element(by.xpath('//input[@placeholder="Email"]'));
 this.address=element(by.xpath('//input[@placeholder="Address"]'));
 this.city=element(by.xpath('//input[@placeholder="City"]'));
 this.state=element(by.xpath('//input[@placeholder="State"]'));
 this.country=element(by.xpath('//input[@placeholder="Country"]'));
 this.zip_Code=element(by.xpath('//input[@placeholder="Zip Code"]'));
 this.phoneType_dropdown=element(by.xpath('//mat-select[@formcontrolname="type"]'));
 this.select_ios=element(by.xpath('//span[text()="iOS"]'));
 this.select_android=element(by.xpath('//span[text()="Android"]'));
 this.select_web=element(by.xpath('//span[text()="Web"]'));
 this.specilization=element(by.xpath('//input[@placeholder="Specialization"]'));
 this.licence=element(by.xpath('//input[@placeholder="Licence"]'));
 this.isPharmacyAdmin_switch=element(by.xpath('//mat-slide-toggle[@formcontrolname="isPharmacyAdmin"]'));
 this.isPharmacist_switch=element(by.xpath('//mat-slide-toggle[@formcontrolname="isPharmacist"]'));
 this.isNursePractitioner_switch=element(by.xpath('//mat-slide-toggle[@formcontrolname="isNursePractioner"]'));
 this.upload_ImgBtn=element(by.css('input[type="file"]'));
 this.submitBtn=element(by.xpath('//button[@type="submit"]'));
 this.cancelBtn=element(by.xpath('//span[text()="Cancel"]'));
 //this.pharmacy_dropdown=element(by.xpath('//mat-select[@placeholder="Pharmacy"]'));
 this.pharmacy_dropdown=element(by.xpath('//mat-select[@role="listbox"][@placeholder="Pharmacy"]'));

 this.select_pharmacy=element(by.xpath('//div//mat-option[1]'))//dayamed
 this.lname_errorMsg=element(by.xpath('//span[text()=" Please enter valid Last Name. "]'));
 this.fname_ErrorMsg=element(by.xpath('//span[text()=" Please enter valid First Name. "]'));
 this.mobileNum_errorMsg=element(by.xpath('//span[text()="Please enter Mobile Number.."]'));
 this.email_errorMsg=element(by.xpath('//span[text()=" Please enter Email." and @class="ng-star-inserted"]'));
 this.address_errorMsg=element(by.xpath('//span[text()=" Please enter valid Address. "]'));
 this.zipcode_error=element(by.xpath('//span[text()=" Please enter valid Zip Code."]'));
 this.licence_error=element(by.xpath('//span[text()="Please enter Licence."]'));
 this.pharmacy_error=element(by.xpath('//span[text()="Please select Pharmacy."]'));
 this.nickName_error=element(by.xpath('//span[text()=" Please enter valid Nick Name "]'));
 this.city_error=element(by.xpath('//mat-error[text()=" Please enter valid City. "]'));
 this.State_errorMsg=element(by.xpath('//mat-error[text()=" Please enter valid State. "]'))
 this.Country_errorMsg=element(by.xpath('//mat-error[text()=" Please enter valid Country. "]'));
 this.invalidlname_errorMsg=element(by.xpath('//span[text()=" Please enter valid Last Name. "]'));
 this.invalidfname_ErrorMsg=element(by.xpath('//span[text()=" Please enter valid First Name. "]'));
 this.invalidmobileNum_errorMsg=element(by.xpath('//span[text()="Please enter a valid  Mobile Number."]'));
 this.invalidemail_errorMsg=element(by.xpath('//span[text()="Please enter a valid Email."]'));
 this.invalidaddress_errorMsg=element(by.xpath('//span[text()=" Please enter valid Address. "]'));
 this.invalidzipcode_error=element(by.xpath('//span[text()=" Please enter valid Zip Code."]'));
 this.invalidnickName_error=element(by.xpath('//span[text()=" Please enter valid Nick Name "]'));
 this.invalidcity_error=element(by.xpath('//mat-error[text()=" Please enter valid City. "]'));
 this.invalidState_errorMsg=element(by.xpath('//mat-error[text()=" Please enter valid State. "]'));
 this.invalidCountry_errorMsg=element(by.xpath('//mat-error[text()=" Please enter valid Country. "]'));
 this.success_msg=element(by.xpath('//div[text()="Provider data has been saved successfully"]'));
 this.provider_card=element(by.xpath('//p[text()=" reddy, anuksha "]'));
 this.viewPatients=element(by.xpath('//span[text()=" View Patients "]'));
 this.searchPatient=element(by.xpath('//*[@id="mat-input-41"]'));
 











 

 }
}