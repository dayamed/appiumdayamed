"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.homePage = void 0;
const protractor_1 = require("protractor");
class homePage {
    constructor() {
        this.heading = protractor_1.element(protractor_1.by.xpath('//span[text()="Providers"]'));
        this.menuBtn = protractor_1.element(protractor_1.by.xpath('//button[@class="topNavButton-Menu mat-icon-button"]'));
        this.logo = protractor_1.element(protractor_1.by.xpath('//img[@alt="Dayamed logo"]'));
        this.search_provider = protractor_1.element(protractor_1.by.xpath('//*[@id="mat-input-2"]'));
        this.provider_card = protractor_1.element(protractor_1.by.xpath(' //p[text()=" Siva11, Test "]'));
        this.addProvider_fromProviderPage = protractor_1.element(protractor_1.by.xpath('//span[@class="mat-button-wrapper" and text()="Add Provider"]'));
        this.account_circle = protractor_1.element(protractor_1.by.xpath('//mat-icon[text()="account_circle"]'));
        this.help = protractor_1.element(protractor_1.by.xpath('//span[text()="Help"]'));
        this.account_ChangePassword = protractor_1.element(protractor_1.by.xpath('//span[text()="Change Password"]'));
        this.account_Logout = protractor_1.element(protractor_1.by.xpath('//span[text()="Logout"]'));
        this.ProviderDropdown = protractor_1.element(protractor_1.by.xpath('//span[text()="Providers"and @class="list-item ng-star-inserted"]'));
        this.provider_List = protractor_1.element(protractor_1.by.xpath('//div[text()="Provider List"]'));
        this.Add_Provider = protractor_1.element(protractor_1.by.xpath('//div[text()="Add Provider"]'));
        this.cargiverDropDown = protractor_1.element(protractor_1.by.xpath('//span[text()="Caregiver"]'));
        this.cargiver_list = protractor_1.element(protractor_1.by.xpath('//div[text()="Caregiver List"]'));
        this.AddCaregiver = protractor_1.element(protractor_1.by.xpath('//div[text()="Add Caregiver"]'));
        this.Administration_DropDown = protractor_1.element(protractor_1.by.xpath('//span[text()="Administration"]'));
        this.PharmacyList = protractor_1.element(protractor_1.by.xpath('//div[text()="Pharmacy List"]'));
        this.RelationsManagement = protractor_1.element(protractor_1.by.xpath('//div[text()="Relations Management"]'));
        this.Terms_Conditions = protractor_1.element(protractor_1.by.xpath('//div[text()="Terms and Conditions"]'));
        this.Reports_DropDown = protractor_1.element(protractor_1.by.xpath('//span[text()="Reports"]'));
        this.Adherence = protractor_1.element(protractor_1.by.xpath('//div[text()="Adherence"]'));
        this.AccountSettings_DropDown = protractor_1.element(protractor_1.by.xpath('//span[text()="Account Settings"]'));
        this.ChangePassword = protractor_1.element(protractor_1.by.xpath('//div[text()="Change Password"]'));
        this.Logout = protractor_1.element(protractor_1.by.xpath('//div[text()="Logout"]'));
        // this.loadMore_ProvidersBtn=element(by.xpath('//button[@class="load-more mat-stroked-button"]'));
        this.loadMore_ProvidersBtn = protractor_1.element(protractor_1.by.xpath('/html/body/app-root/app-admin/app-layout/div/mat-sidenav-container/mat-sidenav-content/div/app-providers-list/mat-card/div/app-load-more/div[2]/button'));
        this.pagination = protractor_1.element(protractor_1.by.xpath('//span[@class="pagination"]'));
        this.providers_count = protractor_1.element.all(protractor_1.by.xpath('//mat-card[@class="user-card mat-card ng-star-inserted"]'));
    }
}
exports.homePage = homePage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZVBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlb2JqZWN0cy9ob21lUGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBNEU7QUFFNUUsTUFBYSxRQUFRO0lBK0JqQjtRQUVFLElBQUksQ0FBQyxPQUFPLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsT0FBTyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxzREFBc0QsQ0FBQyxDQUFDLENBQUM7UUFDdkYsSUFBSSxDQUFDLElBQUksR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxlQUFlLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsYUFBYSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLDRCQUE0QixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrREFBK0QsQ0FBQyxDQUFDLENBQUM7UUFDckgsSUFBSSxDQUFDLGNBQWMsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxJQUFJLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsc0JBQXNCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsY0FBYyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLGdCQUFnQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtRUFBbUUsQ0FBQyxDQUFDLENBQUM7UUFDN0csSUFBSSxDQUFDLGFBQWEsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxZQUFZLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsYUFBYSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLFlBQVksR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyx1QkFBdUIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxZQUFZLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsbUJBQW1CLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsZ0JBQWdCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxDQUFDLENBQUMsQ0FBQztRQUNoRixJQUFJLENBQUMsZ0JBQWdCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsU0FBUyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLHdCQUF3QixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7UUFDckYsSUFBSSxDQUFDLGNBQWMsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxNQUFNLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQztRQUN6RCxtR0FBbUc7UUFDbEcsSUFBSSxDQUFDLHFCQUFxQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3SkFBd0osQ0FBQyxDQUFDLENBQUM7UUFFdk0sSUFBSSxDQUFDLFVBQVUsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxlQUFlLEdBQUMsb0JBQU8sQ0FBQyxHQUFHLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywwREFBMEQsQ0FBQyxDQUFDLENBQUM7SUFHekcsQ0FBQztDQUNKO0FBbEVELDRCQWtFQyJ9