"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.claimCreation = void 0;
const protractor_1 = require("protractor");
const claimData_json_1 = __importDefault(require("../TestData/claimData.json"));
const protractor_2 = require("protractor");
const login_1 = require("./login");
let expect = require('chai').expect;
let Login = new login_1.login();
class claimCreation {
    constructor() {
        this.reimburseLink = protractor_1.element(protractor_1.by.xpath("//a[@ng-reflect-router-link='.']")); //developer look into the xpath, once it is fixed it will locate in all users login.
        this.newClaimLink = protractor_1.element(protractor_1.by.xpath("//a[@href='#/reimbursement/add']"));
        this.claimType = protractor_1.element(protractor_1.by.xpath("//div[@class='mat-select-value']"));
        this.lateNightClaim = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='1']"));
        this.add = protractor_1.element(protractor_1.by.xpath("//button[@type='submit']"));
        this.LW_natureOfSpend = protractor_1.element(protractor_1.by.xpath("//mat-select[@formcontrolname='type']"));
        this.LW_Food = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='6']"));
        this.LW_Conveyance = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='7']"));
        this.LW_Eamount = protractor_1.element(protractor_1.by.xpath("//input[@ng-reflect-name='amount']"));
        this.LW_calanderIcon = protractor_1.element(protractor_1.by.xpath("//button[@aria-label='Open calendar']"));
        this.LW_previousMonth = protractor_1.element(protractor_1.by.xpath("//button[@aria-label='Previous month']"));
        this.LW_weekDayDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Mon Jul 13 2020']"));
        this.LW_convyancedDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Tue Jul 14 2020']"));
        this.LW_weekEndDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Sun Jul 12 2020']"));
        this.LW_billNumber = protractor_1.element(protractor_1.by.xpath("//input[@ng-reflect-name='billNo']"));
        this.LW_inTime = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='officeInTime']"));
        this.LW_outTime = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='officeOutTime']"));
        this.LW_Purpose = protractor_1.element(protractor_1.by.xpath("//textarea[@ng-reflect-name='description']"));
        this.LW_uploadImg = protractor_1.element(protractor_1.by.xpath("//input[@class='input_fileupload--hidden']"));
        this.LW_addMember_btn = protractor_1.element(protractor_1.by.xpath("//a[@title='Add Team Member']"));
        this.LW_teamMember2 = protractor_1.element(protractor_1.by.xpath("(//input[@formcontrolname='teammember'])[2]"));
        this.LW_teamMember2_add = protractor_1.element(protractor_1.by.xpath("//mat-option[@role='option']"));
        this.LW_claimCancel = protractor_1.element(protractor_1.by.xpath("//mat-icon[contains(text(),'highlight_off')]"));
        this.LW_claimReset = protractor_1.element(protractor_1.by.xpath("//mat-icon[contains(text(),'refresh')]"));
        this.LW_claimSave = protractor_1.element(protractor_1.by.xpath("(//button[@type='submit'])[2]"));
        this.LW_fromPlace = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='fromPlace']"));
        this.LW_toPlace = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='toPlace']"));
        this.LW_travelDistance = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='travelledDistance']"));
        this.LW_inTime2 = protractor_1.element(protractor_1.by.xpath("(//input[@ng-reflect-name='officeInTime'])[2]"));
        this.LW_outTime2 = protractor_1.element(protractor_1.by.xpath("(//input[@ng-reflect-name='officeOutTime'])[2]"));
        this.sendForApproval = protractor_1.element(protractor_1.by.xpath("//div[@class='bottom']//*[@color='primary']"));
        this.claimOk = protractor_1.element(protractor_1.by.xpath("//span[contains(text(),'OK')]"));
        this.viewClaimLink = protractor_1.element(protractor_1.by.xpath("//a[@ng-reflect-router-link='/reimbursement/assigned']"));
        this.applyClaim = protractor_1.element(protractor_1.by.xpath("//*[@class='mat-tab-labels']/*"));
        this.assignClaimPage = protractor_1.element(protractor_1.by.xpath("//div[@aria-posinset='2']"));
        this.claimNumbers_VC = protractor_1.element.all(protractor_1.by.xpath("//td[1]"));
        this.viewClaimIcon = protractor_1.element(protractor_1.by.xpath("(//mat-icon[contains(text(),'visibility')])[1]"));
        this.claimNumberVC = protractor_1.element.all(protractor_1.by.css('tr')).get(1).all(protractor_1.by.css('td')).get(0);
        this.VC_weekDay = protractor_1.element(protractor_1.by.xpath("(//tr[1]/td[@role='gridcell'])[4]"));
        this.VC_Weekend = protractor_1.element(protractor_1.by.xpath("(//tr[3]/td[@role='gridcell'])[4]"));
        this.VC_Conveyance = protractor_1.element(protractor_1.by.xpath("(//tr[5]/td[@role='gridcell'])[4]"));
        this.VC_outTimeWD = protractor_1.element(protractor_1.by.xpath("(//span[@class='label14'])[13]"));
        this.VC_outTimeWE = protractor_1.element(protractor_1.by.xpath("(//span[@class='label14'])[16]"));
        this.VC_travelPlace = protractor_1.element(protractor_1.by.xpath("(//span[@class='label14'])[17]"));
        this.claimIcon = protractor_1.element(protractor_1.by.xpath("(//mat-icon[contains(text(),'visibility')])[1]"));
        this.AC_billNumber = protractor_1.element(protractor_1.by.xpath("//table[@class='mat-elevation-z8 mat-table']//tr[3]/td[5]"));
        this.approveClaim = protractor_1.element(protractor_1.by.xpath("//div[contains(text(),'Approve')]"));
        this.Comments = protractor_1.element(protractor_1.by.xpath("//textarea[@formcontrolname='comments']"));
        this.VC_claimStatus = protractor_1.element(protractor_1.by.xpath("//tr[1]/td[@role='gridcell'][6]"));
        this.Submit = protractor_1.element(protractor_1.by.xpath("//button[@type='submit']"));
        this.claimSuccessAlert = protractor_1.element(protractor_1.by.css('mat-dialog-container'));
        this.verifyClaim = protractor_1.element(protractor_1.by.xpath("//div[contains(text(),'Verify')]"));
        this.payClaim = protractor_1.element(protractor_1.by.xpath("//div[contains(text(),'Approve/Paid')]"));
        this.AC_claimNumber = protractor_1.element.all(protractor_1.by.xpath("//td[1]"));
        this.cNumber = protractor_1.element(protractor_1.by.xpath("//h4[contains(text(),'SG-2020')]"));
        this.claimAlert = protractor_1.element(protractor_1.by.xpath("//simple-snack-bar[@class='mat-simple-snackbar ng-star-inserted']"));
    }
    //adding "Late Night Weekday Claim" under LateNight Claim
    //LW: Latenight-Weekend
    weekDayClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            yield this.lateNightClaim.click();
            yield this.add.click();
            yield this.LW_natureOfSpend.click();
            yield this.LW_Food.click();
            yield this.LW_Eamount.sendKeys(claimData_json_1.default.validEamount);
            yield this.LW_calanderIcon.click();
            // await this.LW_previousMonth.click();
            yield this.LW_weekDayDate.click();
            yield this.LW_billNumber.sendKeys(claimData_json_1.default.billNo_WD);
            yield this.LW_inTime.sendKeys(claimData_json_1.default.inTimeWD);
            yield this.LW_outTime.sendKeys(claimData_json_1.default.outTimeWD);
            yield this.LW_Purpose.sendKeys(claimData_json_1.default.purpose_WD);
            yield this.LW_uploadImg.sendKeys(claimData_json_1.default.imagePath);
            yield this.LW_claimSave.click();
            protractor_2.browser.sleep(4000);
        });
    }
    //adding "Late Night Weekend Claim" under LateNight Claim
    weekendClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            //  await this.LW_Claim.click();
            yield this.add.click();
            yield this.LW_natureOfSpend.click();
            yield this.LW_Food.click();
            yield this.LW_Eamount.sendKeys(claimData_json_1.default.validEamount);
            yield this.LW_calanderIcon.click();
            //  await this.LW_previousMonth.click();
            yield this.LW_weekEndDate.click();
            yield this.LW_billNumber.sendKeys(claimData_json_1.default.billNo_WE);
            yield this.LW_inTime.sendKeys(claimData_json_1.default.inTime_WE);
            yield this.LW_outTime.sendKeys(claimData_json_1.default.outTime_WE2);
            yield this.LW_Purpose.sendKeys(claimData_json_1.default.purpose_WE);
            yield this.LW_uploadImg.sendKeys(claimData_json_1.default.docPath);
            yield this.LW_claimSave.click();
            protractor_2.browser.sleep(4000);
        });
    }
    //adding "Late Night Conveyance Claim" under LateNight Claim
    conveyanceClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            yield this.add.click();
            protractor_2.browser.sleep(1500);
            yield this.LW_natureOfSpend.click();
            yield this.LW_Conveyance.click();
            yield this.LW_calanderIcon.click();
            //  await this.LW_previousMonth.click();
            yield this.LW_convyancedDate.click();
            yield this.LW_fromPlace.sendKeys(claimData_json_1.default.fromPlace_CE);
            yield this.LW_toPlace.sendKeys(claimData_json_1.default.toPlace_CE);
            yield this.LW_travelDistance.sendKeys(claimData_json_1.default.travelDistance_CE);
            yield this.LW_Eamount.sendKeys(claimData_json_1.default.eamountConveyanceLW);
            yield this.LW_billNumber.sendKeys(claimData_json_1.default.billNo_CE);
            yield this.LW_uploadImg.sendKeys(claimData_json_1.default.excelPath);
            yield this.LW_Purpose.sendKeys(claimData_json_1.default.purpose_CE);
            yield this.LW_inTime.sendKeys(claimData_json_1.default.inTime_CE1);
            yield this.LW_outTime.sendKeys(claimData_json_1.default.outTime_CE1);
            yield this.LW_addMember_btn.click();
            yield this.LW_teamMember2.sendKeys(claimData_json_1.default.team_Mem2);
            yield this.LW_teamMember2_add.click();
            yield this.LW_inTime2.sendKeys(claimData_json_1.default.inTime_CE2);
            yield this.LW_outTime2.sendKeys(claimData_json_1.default.outTime_CE2);
            yield this.LW_claimSave.click();
            protractor_2.browser.sleep(4000);
        });
    }
    //Verifying Week Day claim data in view claim page
    //WD - Week Day
    viewClaimWD() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield this.VC_weekDay.getText().then(function (claimPurpose) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claimPurpose).to.equal(claimData_json_1.default.purpose_WD);
                });
            });
            yield this.VC_weekDay.click();
            protractor_2.browser.sleep(3500);
            yield expect(this.VC_outTimeWD.isPresent()).to.eventually.equal(true);
            yield this.VC_outTimeWD.getText().then(function (ouTime) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(ouTime).to.equal(claimData_json_1.default.expOutTimeWD);
                });
            });
            yield this.VC_weekDay.click();
        });
    }
    //Verifying Weekend claim data in view claim page
    //WE - Weekend
    viewClaimWE() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.VC_Weekend.getText().then(function (claimPurpose) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claimPurpose).to.equal(claimData_json_1.default.purpose_WE);
                });
            });
            yield this.VC_Weekend.click();
            protractor_2.browser.sleep(3500);
            yield expect(this.VC_outTimeWE.isPresent()).to.eventually.equal(true);
            yield this.VC_outTimeWE.getText().then(function (ouTime) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(ouTime).to.equal(claimData_json_1.default.expOutTimeWE); //exp : expected
                });
            });
            yield this.VC_Weekend.click();
        });
    }
    //Verifying conveyance claim data in view claim page
    //CE - Conveyance
    viewClaimCE() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.VC_Conveyance.getText().then(function (claimPurpose) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claimPurpose).to.equal(claimData_json_1.default.purpose_CE);
                });
            });
            protractor_2.browser.sleep(500);
            yield this.VC_Conveyance.click();
            protractor_2.browser.sleep(2500);
            yield expect(this.VC_travelPlace.isPresent()).to.eventually.equal(true);
            yield this.VC_travelPlace.getText().then(function (place) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(place).to.equal(claimData_json_1.default.travelPlace);
                });
            });
            yield this.VC_Conveyance.click();
        });
    }
    //logout from Reimbursement portal
    logout() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield Login.userImage.click();
            protractor_2.browser.sleep(500);
            yield Login.Logout.click();
        });
    }
    //login with Associate role
    associateLogin() {
        return __awaiter(this, void 0, void 0, function* () {
            yield Login.userName.sendKeys(claimData_json_1.default.associateEmail);
            yield Login.loginButton.click();
        });
    }
    //clicking view claim link in view claim page in Associate Role
    //AR - Associate Role
    viewClaims_AR() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.reimburseLink.click();
            protractor_2.browser.sleep(500);
            yield expect(this.viewClaimLink.isDisplayed());
            yield this.viewClaimLink.click();
        });
    }
    //login with Approver Role
    approverLogin() {
        return __awaiter(this, void 0, void 0, function* () {
            yield Login.userName.sendKeys(claimData_json_1.default.approverEmail);
            yield Login.loginButton.click();
        });
    }
    //login with verifier 1 role
    verifier1_Login() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield Login.userName.sendKeys(claimData_json_1.default.verifier1_Email);
            yield Login.loginButton.click();
        });
    }
    //login with verifier 2 role
    verifier2_Login() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield Login.userName.sendKeys(claimData_json_1.default.verifier2_Email);
            yield Login.loginButton.click();
        });
    }
    //validating the claim number and clicking on approve claim icon
    selectClaim_AC() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield this.assignClaimPage.click();
            protractor_2.browser.sleep(2500);
            yield this.AC_claimNumber.then(function (itemList) {
                for (let i = 0; i < itemList.length; i++) {
                    itemList[i].getText().then(function (vcNumber) {
                        return __awaiter(this, void 0, void 0, function* () {
                            const claimNumber = yield viewClaimNumber();
                            if (vcNumber == claimNumber) {
                                expect(vcNumber).to.equal(claimNumber);
                            }
                        });
                    });
                }
                ;
            });
            //clicking on respective claim icon to naviagte to Approve/Verify/Pay claim page
            yield this.claimIcon.click();
            protractor_2.browser.sleep(2000);
            // getting claim details in approve claim page to validate the approving claim is expected one or not
            yield this.AC_billNumber.getText().then(function (billNumber) {
                expect(billNumber).to.equal(claimData_json_1.default.billNo_WE);
            });
        });
    }
    // comparing the claim number in view claim page
    VC_claimNumber() {
        return __awaiter(this, void 0, void 0, function* () {
            this.claimNumbers_VC.then(function (itemList) {
                for (let i = 0; i < itemList.length; i++) {
                    itemList[i].getText().then(function (claimNumber) {
                        return __awaiter(this, void 0, void 0, function* () {
                            const vcNumber = yield viewClaimNumber();
                            if (claimNumber == vcNumber) {
                                expect(claimNumber).to.equal(vcNumber);
                            }
                        });
                    });
                }
                ;
            });
        });
    }
    // verifying the claim status in view claims page after claim Submitted, Approved, Verified and Paid
    statusVerify(status) {
        return __awaiter(this, void 0, void 0, function* () {
            yield expect(this.VC_claimStatus.isPresent()).to.eventually.equal(true);
            yield this.VC_claimStatus.getText().then(function (actualStatus) {
                return __awaiter(this, void 0, void 0, function* () {
                    expect(actualStatus).to.equal(status);
                });
            });
        });
    }
    successAlert() {
        return __awaiter(this, void 0, void 0, function* () {
            return protractor_1.element(protractor_1.by.xpath("//simple-snack-bar[@class='mat-simple-snackbar ng-star-inserted']")).getText().then(function (claimMessage) {
                let number = claimMessage.substr(8, 12);
                return number;
            });
        });
    }
}
exports.claimCreation = claimCreation;
//getting claim number from view claim page
function viewClaimNumber() {
    return __awaiter(this, void 0, void 0, function* () {
        return protractor_1.element(protractor_1.by.xpath("//tr[1]/td[1]")).getText().then(function (vcNumber) {
            //VC - view claim
            return vcNumber;
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xhaW1DcmVhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VvYmplY3RzL2NsYWltQ3JlYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQTRFO0FBQzVFLGdGQUFtRDtBQUNuRCwyQ0FBcUM7QUFDckMsbUNBQWdDO0FBRWhDLElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDcEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUd4QixNQUFhLGFBQWE7SUFtRXhCO1FBRUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDLENBQUcsb0ZBQW9GO1FBQ2xLLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsU0FBUyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxHQUFHLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQzlFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsZUFBZSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLGdCQUFnQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLENBQUM7UUFDcEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDN0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw0Q0FBNEMsQ0FBQyxDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQyxDQUFDO1FBQ3BGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUMsQ0FBQztRQUN2RixJQUFJLENBQUMsa0JBQWtCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDhCQUE4QixDQUFDLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDLENBQUM7UUFDeEYsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUMsQ0FBQztRQUNyRixJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDLENBQUM7UUFFdkYsSUFBSSxDQUFDLGVBQWUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkNBQTZDLENBQUMsQ0FBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQTtRQUNqRSxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3REFBd0QsQ0FBQyxDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxlQUFlLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsZUFBZSxHQUFHLG9CQUFPLENBQUMsR0FBRyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDLENBQUM7UUFDekYsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRS9FLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDO1FBRTVFLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO1FBRTFFLElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGdEQUFnRCxDQUFDLENBQUMsQ0FBQztRQUNyRixJQUFJLENBQUMsYUFBYSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyREFBMkQsQ0FBQyxDQUFDLENBQUM7UUFDcEcsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxRQUFRLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7UUFDM0UsSUFBSSxDQUFDLE1BQU0sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxXQUFXLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsUUFBUSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG1FQUFtRSxDQUFDLENBQUMsQ0FBQztJQUUzRyxDQUFDO0lBR0QseURBQXlEO0lBQ3pELHVCQUF1QjtJQUNqQixjQUFjOztZQUVsQixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDN0IsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2xDLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN2QixNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDM0IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQyx1Q0FBdUM7WUFDdEMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2xDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2RCxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEQsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3BELE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLENBQUM7S0FBQTtJQUVBLHlEQUF5RDtJQUNwRCxjQUFjOztZQUNsQixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDN0IsZ0NBQWdDO1lBQ2hDLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN2QixNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDM0IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNyQyx3Q0FBd0M7WUFDdEMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2xDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2RCxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkQsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3RELE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDcEQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLENBQUM7S0FBQTtJQUVELDREQUE0RDtJQUN0RCxpQkFBaUI7O1lBQ3JCLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUM3QixNQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDdkIsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDcEMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2pDLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNyQyx3Q0FBd0M7WUFDdEMsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDckMsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3pELE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQ25FLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzlELE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2RCxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdEQsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3JELE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNwRCxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdEQsTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDcEMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxDQUFBO1lBQ3JDLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNyRCxNQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDdkQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RCLENBQUM7S0FBQTtJQUVELGtEQUFrRDtJQUNsRCxlQUFlO0lBQ1QsV0FBVzs7WUFFZixvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQWdCLFlBQVk7O29CQUMvRCxNQUFNLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzVELENBQUM7YUFBQSxDQUFDLENBQUE7WUFDRixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDOUIsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsTUFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RFLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsTUFBTTs7b0JBQzNELE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDeEQsQ0FBQzthQUFBLENBQUMsQ0FBQTtZQUNGLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVoQyxDQUFDO0tBQUE7SUFFRCxpREFBaUQ7SUFDakQsY0FBYztJQUNSLFdBQVc7O1lBRWYsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixZQUFZOztvQkFDL0QsTUFBTSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM1RCxDQUFDO2FBQUEsQ0FBQyxDQUFBO1lBQ0YsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzlCLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RSxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQWdCLE1BQU07O29CQUMzRCxNQUFNLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0I7Z0JBQ3pFLENBQUM7YUFBQSxDQUFDLENBQUE7WUFDRixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsQ0FBQztLQUFBO0lBRUQsb0RBQW9EO0lBQ3BELGlCQUFpQjtJQUNYLFdBQVc7O1lBRWYsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixZQUFZOztvQkFDbEUsTUFBTSxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM1RCxDQUFDO2FBQUEsQ0FBQyxDQUFBO1lBQ0Ysb0JBQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2pDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RSxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQWdCLEtBQUs7O29CQUM1RCxNQUFNLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3RELENBQUM7YUFBQSxDQUFDLENBQUE7WUFDRixNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsQ0FBQztLQUFBO0lBRUQsa0NBQWtDO0lBQzVCLE1BQU07O1lBQ1Ysb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsTUFBTSxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzlCLG9CQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLE1BQU0sS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixDQUFDO0tBQUE7SUFFRCwyQkFBMkI7SUFDckIsY0FBYzs7WUFDbEIsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxDQUFDO0tBQUE7SUFFRCwrREFBK0Q7SUFDL0QscUJBQXFCO0lBQ2YsYUFBYTs7WUFFakIsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2pDLG9CQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUMvQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsQ0FBQztLQUFBO0lBRUQsMEJBQTBCO0lBQ3BCLGFBQWE7O1lBQ2pCLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2RCxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsQ0FBQztLQUFBO0lBRUQsNEJBQTRCO0lBQ3RCLGVBQWU7O1lBQ25CLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RCxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsQ0FBQztLQUFBO0lBRUQsNEJBQTRCO0lBQ3RCLGVBQWU7O1lBQ25CLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUN6RCxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsQ0FBQztLQUFBO0lBRUQsZ0VBQWdFO0lBQzFELGNBQWM7O1lBQ2xCLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNqQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV0QixNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtnQkFDN0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsUUFBUTs7NEJBQ2pELE1BQU0sV0FBVyxHQUFHLE1BQU0sZUFBZSxFQUFFLENBQUM7NEJBQzVDLElBQUksUUFBUSxJQUFJLFdBQVcsRUFBRTtnQ0FDM0IsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUE7NkJBQ3ZDO3dCQUNILENBQUM7cUJBQUEsQ0FBQyxDQUFDO2lCQUNKO2dCQUFBLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQztZQUdMLGdGQUFnRjtZQUNoRixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFN0Isb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIscUdBQXFHO1lBQ3JHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxVQUFVO2dCQUMxRCxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ25ELENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUFBO0lBRUQsZ0RBQWdEO0lBQzFDLGNBQWM7O1lBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtnQkFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsV0FBVzs7NEJBQ3BELE1BQU0sUUFBUSxHQUFHLE1BQU0sZUFBZSxFQUFFLENBQUM7NEJBQ3pDLElBQUksV0FBVyxJQUFJLFFBQVEsRUFBRTtnQ0FDM0IsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7NkJBQ3hDO3dCQUNILENBQUM7cUJBQUEsQ0FBQyxDQUFDO2lCQUNKO2dCQUFBLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7S0FBQTtJQUVGLG9HQUFvRztJQUM3RixZQUFZLENBQUMsTUFBTTs7WUFDdkIsTUFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hFLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsWUFBWTs7b0JBQ25FLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxDQUFDO2FBQUEsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUFBO0lBRUssWUFBWTs7WUFDaEIsT0FBTyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsbUVBQW1FLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLFlBQVk7Z0JBQ2pJLElBQUksTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLE1BQU0sQ0FBQztZQUNoQixDQUFDLENBQUMsQ0FBQTtRQUVKLENBQUM7S0FBQTtDQUVGO0FBcFdELHNDQW9XQztBQUNELDJDQUEyQztBQUMzQyxTQUFlLGVBQWU7O1FBQzVCLE9BQU8sb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtZQUN6RSxpQkFBaUI7WUFDakIsT0FBTyxRQUFRLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0NBQUEifQ==