"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const ClaimData_json_1 = __importDefault(require("../TestData/ClaimData.json"));
const protractor_2 = require("protractor");
const login_1 = require("./login");
let expect = require('chai').expect;
let RLogin = new login_1.login();
class createClaimLW {
    constructor() {
        this.reimburseLink = protractor_1.element(protractor_1.by.xpath("//a[@ng-reflect-router-link='.']"));
        this.newClaimLink = protractor_1.element(protractor_1.by.xpath("//a[@href='#/reimbursement/add']"));
        this.claimType = protractor_1.element(protractor_1.by.xpath("//div[@class='mat-select-value']"));
        this.lateNightClaim = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='1']"));
        this.add = protractor_1.element(protractor_1.by.xpath("//button[@type='submit']"));
        this.LW_natureOfSpend = protractor_1.element(protractor_1.by.xpath("//mat-select[@formcontrolname='type']"));
        this.LW_Food = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='6']"));
        this.LW_Conveyance = protractor_1.element(protractor_1.by.xpath("//mat-option[@ng-reflect-value='7']"));
        this.LW_Eamount = protractor_1.element(protractor_1.by.xpath("//input[@ng-reflect-name='amount']"));
        this.LW_calanderIcon = protractor_1.element(protractor_1.by.xpath("//button[@aria-label='Open calendar']"));
        this.LW_previousMonth = protractor_1.element(protractor_1.by.css("Previous month"));
        this.LW_weekDayDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Mon May 11 2020']"));
        this.LW_convyancedDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Tue May 12 2020']"));
        this.LW_weekEndDate = protractor_1.element(protractor_1.by.xpath("//td[@aria-label='Sun May 10 2020']"));
        this.LW_billNumber = protractor_1.element(protractor_1.by.xpath("//input[@ng-reflect-name='billNo']"));
        this.LW_inTime = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='officeInTime']"));
        this.LW_outTime = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='officeOutTime']"));
        this.LW_Purpose = protractor_1.element(protractor_1.by.xpath("//textarea[@ng-reflect-name='description']"));
        this.LW_uploadImg = protractor_1.element(protractor_1.by.xpath("//input[@class='input_fileupload--hidden']"));
        this.LW_addMember_btn = protractor_1.element(protractor_1.by.xpath("//a[@title='Add Team Member']"));
        this.LW_teamMember2 = protractor_1.element(protractor_1.by.xpath("//app-add-edit-reimbursement//tr[2]//td[1]//input"));
        this.LW_teamMember2_add = protractor_1.element(protractor_1.by.xpath("//mat-option[@role='option']"));
        this.LW_claimCancel = protractor_1.element(protractor_1.by.xpath("//button[@class='mat-raised-button mat-button-base'][1]"));
        this.LW_claimReset = protractor_1.element(protractor_1.by.xpath("//button[@class='mat-raised-button mat-button-base'][2]"));
        this.LW_claimSave = protractor_1.element(protractor_1.by.xpath("//button[@class='mat-raised-button mat-button-base'][3]"));
        this.LW_fromPlace = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='fromPlace']"));
        this.LW_toPlace = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='toPlace']"));
        this.LW_travelDistance = protractor_1.element(protractor_1.by.xpath("//input[@formcontrolname='travelledDistance']"));
        this.LW_inTime2 = protractor_1.element(protractor_1.by.xpath("//tr[2]/td[2]/mat-form-field[1]//input"));
        this.LW_outTime2 = protractor_1.element(protractor_1.by.xpath("//tr[2]/td[2]/mat-form-field[2]//input"));
        this.sendForApproval = protractor_1.element(protractor_1.by.xpath("//div[@class='bottom']//*[@color='primary']"));
        this.claimOk = protractor_1.element(protractor_1.by.xpath("//span[contains(text(),'OK')]"));
        this.viewClaimLink = protractor_1.element(protractor_1.by.xpath("//a[@ng-reflect-router-link='/reimbursement/assigned']"));
        this.applyClaim = protractor_1.element(protractor_1.by.xpath("//*[@class='mat-tab-labels']/*"));
        this.assignClaimPage = protractor_1.element(protractor_1.by.xpath("//div[@aria-posinset='2']"));
        this.claimNumbers_VC = protractor_1.element.all(protractor_1.by.xpath("//td[1]"));
        this.viewClaimIcon = protractor_1.element(protractor_1.by.xpath("//tr[1]/td[7]/a[@title='View Claim']"));
        this.claimNumberVC = protractor_1.element.all(protractor_1.by.css('tr')).get(1).all(protractor_1.by.css('td')).get(0);
        this.VC_weekDay = protractor_1.element(protractor_1.by.xpath("//div[4]//tbody/tr[1]/td[4]"));
        this.VC_Weekend = protractor_1.element(protractor_1.by.xpath("//div[4]//tbody/tr[3]/td[4]"));
        this.VC_Conveyance = protractor_1.element(protractor_1.by.xpath("//div[4]//tbody/tr[5]/td[4]"));
        this.VC_outTimeWD = protractor_1.element(protractor_1.by.xpath("//tbody/tr[2]/td//div[3]//div[3]/span"));
        this.VC_outTimeWE = protractor_1.element(protractor_1.by.xpath("//tbody/tr[4]/td//div[3]//div[3]/span"));
        this.VC_travelPlace = protractor_1.element(protractor_1.by.xpath("//tbody/tr[6]/td//div[1]/div[1]/span[2]"));
        this.claimIcon = protractor_1.element(protractor_1.by.xpath("//tr[1]/td[@role='gridcell'][9]"));
        this.AC_billNumber = protractor_1.element(protractor_1.by.xpath("//table[@class='mat-elevation-z8 mat-table']//tr[3]/td[5]"));
        this.approveClaim = protractor_1.element(protractor_1.by.xpath("//div[@class='mat-radio-label-content'][text()='Approve']"));
        this.AC_Comments = protractor_1.element(protractor_1.by.xpath("//textarea[@formcontrolname='comments']"));
        this.VC_claimStatus = protractor_1.element(protractor_1.by.xpath("//tr[1]/td[@role='gridcell'][6]"));
        this.Submit = protractor_1.element(protractor_1.by.className("sgblueBtn mat-raised-button mat-button-base"));
        this.claimSuccessAlert = protractor_1.element(protractor_1.by.css('mat-dialog-container'));
        this.verifyClaim = protractor_1.element(protractor_1.by.xpath("//div[@class='mat-radio-label-content'][text()='Verify']"));
        this.payClaim = protractor_1.element(protractor_1.by.xpath("//div[@class='mat-radio-label-content'][text()='Approve/Paid']"));
        this.AC_claimNumber = protractor_1.element.all(protractor_1.by.xpath("//td[1]"));
        this.AC_cNumber = protractor_1.element(protractor_1.by.xpath("//h4[@style='color:#367fa9;margin:0px;']"));
    }
    //adding "Late Night Weekday Claim" under LateNight Claim
    //LW: Latenight-Weekend
    weekDayClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            yield this.lateNightClaim.click();
            yield this.add.click();
            yield this.LW_natureOfSpend.click();
            yield this.LW_Food.click();
            yield this.LW_Eamount.sendKeys(ClaimData_json_1.default.validEamount);
            yield this.LW_calanderIcon.click();
            yield this.LW_weekDayDate.click();
            yield this.LW_billNumber.sendKeys(ClaimData_json_1.default.billNo_WD);
            yield this.LW_inTime.sendKeys(ClaimData_json_1.default.inTimeWD);
            yield this.LW_outTime.sendKeys(ClaimData_json_1.default.outTimeWD);
            yield this.LW_Purpose.sendKeys(ClaimData_json_1.default.Purpose_WDFood);
            yield this.LW_uploadImg.sendKeys(ClaimData_json_1.default.Image_Path);
            yield this.LW_claimSave.click();
        });
    }
    //adding "Late Night Weekend Claim" under LateNight Claim
    weekendClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            //  await this.LW_Claim.click();
            yield this.add.click();
            yield this.LW_natureOfSpend.click();
            yield this.LW_Food.click();
            yield this.LW_Eamount.sendKeys(ClaimData_json_1.default.validEamount);
            yield this.LW_calanderIcon.click();
            yield this.LW_weekEndDate.click();
            yield this.LW_billNumber.sendKeys(ClaimData_json_1.default.billNo_WE);
            yield this.LW_inTime.sendKeys(ClaimData_json_1.default.In_timeWE);
            yield this.LW_outTime.sendKeys(ClaimData_json_1.default.Out_timeWE2);
            yield this.LW_Purpose.sendKeys(ClaimData_json_1.default.Purpose_WEFood);
            yield this.LW_uploadImg.sendKeys(ClaimData_json_1.default.Doc_Path);
            yield this.LW_claimSave.click();
        });
    }
    //adding "Late Night Conveyance Claim" under LateNight Claim
    conveyanceClaimLW() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.claimType.click();
            // await this.LW_Claim.click();
            yield this.add.click();
            yield this.LW_natureOfSpend.click();
            yield this.LW_Conveyance.click();
            yield this.LW_calanderIcon.click();
            yield this.LW_convyancedDate.click();
            yield this.LW_fromPlace.sendKeys(ClaimData_json_1.default.LW_FPlace);
            yield this.LW_toPlace.sendKeys(ClaimData_json_1.default.LW_ToPlace);
            yield this.LW_travelDistance.sendKeys(ClaimData_json_1.default.LW_travelDistance);
            yield this.LW_Eamount.sendKeys(ClaimData_json_1.default.eamountConveyanceLW);
            yield this.LW_billNumber.sendKeys(ClaimData_json_1.default.billNo_CE);
            yield this.LW_uploadImg.sendKeys(ClaimData_json_1.default.Excel_Path);
            yield this.LW_Purpose.sendKeys(ClaimData_json_1.default.Purpose_WDcon);
            yield this.LW_inTime.sendKeys(ClaimData_json_1.default.In_timeCE1);
            yield this.LW_outTime.sendKeys(ClaimData_json_1.default.Out_timeCE1);
            yield this.LW_addMember_btn.click();
            yield this.LW_teamMember2.sendKeys(ClaimData_json_1.default.team_mem2);
            yield this.LW_teamMember2_add.click();
            yield this.LW_inTime2.sendKeys(ClaimData_json_1.default.In_timeCE2);
            yield this.LW_outTime2.sendKeys(ClaimData_json_1.default.Out_timeCE2);
            yield this.LW_claimSave.click();
        });
    }
    //Verifying Week Day claim data in view claim page
    //WD - Week Day
    viewClaimWD() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield this.VC_weekDay.getText().then(function (claim1) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claim1).to.equal(ClaimData_json_1.default.Purpose_WDFood);
                });
            });
            yield this.VC_weekDay.click();
            protractor_2.browser.sleep(3500);
            yield expect(this.VC_outTimeWD.isPresent()).to.eventually.equal(true);
            yield this.VC_outTimeWD.getText().then(function (Outime1) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(Outime1).to.equal(ClaimData_json_1.default.expOutTimeWD);
                });
            });
            yield this.VC_weekDay.click();
        });
    }
    //Verifying Weekend claim data in view claim page
    //WE - Weekend
    viewClaimWE() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.VC_Weekend.getText().then(function (claim2) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claim2).to.equal(ClaimData_json_1.default.Purpose_WEFood);
                });
            });
            yield this.VC_Weekend.click();
            protractor_2.browser.sleep(3500);
            yield expect(this.VC_outTimeWE.isPresent()).to.eventually.equal(true);
            yield this.VC_outTimeWE.getText().then(function (Outime2) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(Outime2).to.equal(ClaimData_json_1.default.expOutTimeWE); //exp : expected
                });
            });
            yield this.VC_Weekend.click();
        });
    }
    //Verifying conveyance claim data in view claim page
    //CE - Conveyance
    viewClaimCE() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.VC_Conveyance.getText().then(function (claim3) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(claim3).to.equal(ClaimData_json_1.default.Purpose_WDcon);
                });
            });
            protractor_2.browser.sleep(500);
            yield this.VC_Conveyance.click();
            protractor_2.browser.sleep(2500);
            yield expect(this.VC_travelPlace.isPresent()).to.eventually.equal(true);
            yield this.VC_travelPlace.getText().then(function (place) {
                return __awaiter(this, void 0, void 0, function* () {
                    yield expect(place).to.equal(ClaimData_json_1.default.travelPlace);
                });
            });
            yield this.VC_Conveyance.click();
        });
    }
    //logout from Reimbursement portal
    logout() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield RLogin.UserImage.click();
            protractor_2.browser.sleep(500);
            yield RLogin.Logoutbtn.click();
        });
    }
    //login with Associate role
    Associate_login() {
        return __awaiter(this, void 0, void 0, function* () {
            yield RLogin.UserEmail.sendKeys(ClaimData_json_1.default.associateEmail);
            yield RLogin.loginButton.click();
        });
    }
    //clicking view claim link in view claim page in Associate Role
    //AR - Associate Role
    ViewClaims_AR() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.reimburseLink.click();
            protractor_2.browser.sleep(500);
            yield expect(this.viewClaimLink.isDisplayed());
            yield this.viewClaimLink.click();
        });
    }
    //login with Approver Role
    Approver_login() {
        return __awaiter(this, void 0, void 0, function* () {
            yield RLogin.UserEmail.sendKeys(ClaimData_json_1.default.approverEmail);
            yield RLogin.loginButton.click();
        });
    }
    //login with verifier 1 role
    Verifier1_login() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield RLogin.UserEmail.sendKeys(ClaimData_json_1.default.verifier1_Email);
            yield RLogin.loginButton.click();
        });
    }
    //login with verifier 2 role
    Verifier2_login() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield RLogin.UserEmail.sendKeys(ClaimData_json_1.default.verifier2_Email);
            yield RLogin.loginButton.click();
        });
    }
    //validating the claim number and clicking on approve claim icon
    selectClaim_AC() {
        return __awaiter(this, void 0, void 0, function* () {
            protractor_2.browser.sleep(1000);
            yield this.assignClaimPage.click().then(function () {
                return __awaiter(this, void 0, void 0, function* () {
                    protractor_2.browser.sleep(2000);
                    this.AC_claimNumber.then(function (itemList) {
                        for (let i = 0; i < itemList.length; i++) {
                            itemList[i].getText().then(function (vcnumber) {
                                return __awaiter(this, void 0, void 0, function* () {
                                    const claimNumber = yield viewClaimNumber();
                                    if (vcnumber == claimNumber) {
                                        expect(vcnumber).to.equal(claimNumber);
                                    }
                                });
                            });
                        }
                        ;
                    });
                });
            });
            //clicking on respective claim icon to naviagte to Approve/Verify/Pay claim page
            yield this.claimIcon.click();
            protractor_2.browser.sleep(2000);
            // getting claim details in approve claim page to validate the approving claim is expected one or not
            yield this.AC_billNumber.getText().then(function (billNumber) {
                expect(billNumber).to.equal(ClaimData_json_1.default.billNo_WE);
            });
        });
    }
    // comparing the claim number in view claim page
    VC_ClaimNumber() {
        return __awaiter(this, void 0, void 0, function* () {
            this.claimNumbers_VC.then(function (itemList) {
                for (let i = 0; i < itemList.length; i++) {
                    itemList[i].getText().then(function (claimNumber) {
                        return __awaiter(this, void 0, void 0, function* () {
                            const vcnumber = yield viewClaimNumber();
                            if (claimNumber == vcnumber) {
                                expect(claimNumber).to.equal(vcnumber);
                            }
                        });
                    });
                }
                ;
            });
        });
    }
    // verifying the claim status in view claims page after claim Submitted, Approved, Verified and Paid
    statusVerfy(status) {
        return __awaiter(this, void 0, void 0, function* () {
            yield expect(this.VC_claimStatus.isPresent()).to.eventually.equal(true);
            yield this.VC_claimStatus.getText().then(function (actualstatus) {
                return __awaiter(this, void 0, void 0, function* () {
                    expect(actualstatus).to.equal(status);
                });
            });
        });
    }
}
exports.createClaimLW = createClaimLW;
//getting claim number from view claim page
function viewClaimNumber() {
    return __awaiter(this, void 0, void 0, function* () {
        return this.claimNumberVC.getText().then(function (VCnumber) {
            //VC - view claim
            return VCnumber;
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQ3JlYXRlQ2xhaW1fTFcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlb2JqZWN0cy9DcmVhdGVDbGFpbV9MVy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLDJDQUE0RTtBQUM1RSxnRkFBbUQ7QUFDbkQsMkNBQXFDO0FBQ3JDLG1DQUFnQztBQUVoQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQ3BDLElBQUksTUFBTSxHQUFHLElBQUksYUFBSyxFQUFFLENBQUM7QUFFekIsTUFBYSxhQUFhO0lBdUV4QjtRQUVFLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGtDQUFrQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsR0FBRyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLENBQUM7UUFDbkYsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLGVBQWUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxTQUFTLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNENBQTRDLENBQUMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUMsQ0FBQztRQUNwRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsY0FBYyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxtREFBbUQsQ0FBQyxDQUFDLENBQUM7UUFDN0YsSUFBSSxDQUFDLGtCQUFrQixHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMseURBQXlELENBQUMsQ0FBQyxDQUFDO1FBQ25HLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUMsQ0FBQztRQUNsRyxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5REFBeUQsQ0FBQyxDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxDQUFDLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLCtDQUErQyxDQUFDLENBQUMsQ0FBQztRQUM1RixJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDLENBQUM7UUFDOUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1FBRS9FLElBQUksQ0FBQyxlQUFlLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUMsQ0FBQztRQUN4RixJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDLENBQUE7UUFDakUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0RBQXdELENBQUMsQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsZUFBZSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGVBQWUsR0FBRyxvQkFBTyxDQUFDLEdBQUcsQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsc0NBQXNDLENBQUMsQ0FBQyxDQUFBO1FBQzlFLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxHQUFHLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsZUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUUvRSxJQUFJLENBQUMsVUFBVSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLFVBQVUsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxhQUFhLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQztRQUV0RSxJQUFJLENBQUMsWUFBWSxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDLENBQUM7UUFDL0UsSUFBSSxDQUFDLFlBQVksR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUMsQ0FBQztRQUVuRixJQUFJLENBQUMsU0FBUyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGFBQWEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMkRBQTJELENBQUMsQ0FBQyxDQUFDO1FBQ3BHLElBQUksQ0FBQyxZQUFZLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDJEQUEyRCxDQUFDLENBQUMsQ0FBQztRQUNuRyxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxNQUFNLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsU0FBUyxDQUFDLDZDQUE2QyxDQUFDLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsV0FBVyxHQUFHLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywwREFBMEQsQ0FBQyxDQUFDLENBQUM7UUFDakcsSUFBSSxDQUFDLFFBQVEsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsZ0VBQWdFLENBQUMsQ0FBQyxDQUFDO1FBQ3BHLElBQUksQ0FBQyxjQUFjLEdBQUcsb0JBQU8sQ0FBQyxHQUFHLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUMsQ0FBQztJQUVsRixDQUFDO0lBR0QseURBQXlEO0lBQ3pELHVCQUF1QjtJQUNqQixjQUFjOztZQUVsQixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDN0IsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2xDLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN2QixNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDM0IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNuQyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDbEMsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsRCxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDcEQsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3pELE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN2RCxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsQ0FBQztLQUFBO0lBRUEseURBQXlEO0lBQ3BELGNBQWM7O1lBQ2xCLE1BQU0sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUM3QixnQ0FBZ0M7WUFDaEMsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3ZCLE1BQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3BDLE1BQU0sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMzQixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdkQsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ25DLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNsQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkQsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ25ELE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0RCxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDekQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3JELE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVsQyxDQUFDO0tBQUE7SUFFRCw0REFBNEQ7SUFDdEQsaUJBQWlCOztZQUNyQixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDN0IsK0JBQStCO1lBQy9CLE1BQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN2QixNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDakMsTUFBTSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ25DLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3JDLE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0RCxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDckQsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUNuRSxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUM5RCxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkQsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4RCxNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDcEQsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3RELE1BQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ3BDLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN4RCxNQUFNLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUNyQyxNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDckQsTUFBTSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxDQUFDO0tBQUE7SUFFRCxrREFBa0Q7SUFDbEQsZUFBZTtJQUNULFdBQVc7O1lBRWYsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixNQUFNOztvQkFDekQsTUFBTSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxDQUFDO2FBQUEsQ0FBQyxDQUFBO1lBQ0YsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzlCLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0RSxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQWdCLE9BQU87O29CQUM1RCxNQUFNLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3pELENBQUM7YUFBQSxDQUFDLENBQUE7WUFDRixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFaEMsQ0FBQztLQUFBO0lBRUQsaURBQWlEO0lBQ2pELGNBQWM7SUFDUixXQUFXOztZQUVmLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsTUFBTTs7b0JBQ3pELE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsQ0FBQzthQUFBLENBQUMsQ0FBQTtZQUNGLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUM5QixvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdEUsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixPQUFPOztvQkFDNUQsTUFBTSxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsZ0JBQWdCO2dCQUMxRSxDQUFDO2FBQUEsQ0FBQyxDQUFBO1lBQ0YsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLENBQUM7S0FBQTtJQUVELG9EQUFvRDtJQUNwRCxpQkFBaUI7SUFDWCxXQUFXOztZQUVmLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsTUFBTTs7b0JBQzVELE1BQU0sTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztnQkFDekQsQ0FBQzthQUFBLENBQUMsQ0FBQTtZQUNGLG9CQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNqQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEUsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixLQUFLOztvQkFDNUQsTUFBTSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN0RCxDQUFDO2FBQUEsQ0FBQyxDQUFBO1lBQ0YsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7S0FBQTtJQUVELGtDQUFrQztJQUM1QixNQUFNOztZQUNWLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMvQixvQkFBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixNQUFNLE1BQU0sQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsQ0FBQztLQUFBO0lBRUQsMkJBQTJCO0lBQ3JCLGVBQWU7O1lBQ25CLE1BQU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMxRCxNQUFNLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsQ0FBQztLQUFBO0lBRUQsK0RBQStEO0lBQy9ELHFCQUFxQjtJQUNmLGFBQWE7O1lBRWpCLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNqQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNuQixNQUFNLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDL0MsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7S0FBQTtJQUVELDBCQUEwQjtJQUNwQixjQUFjOztZQUNsQixNQUFNLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDekQsTUFBTSxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7S0FBQTtJQUVELDRCQUE0QjtJQUN0QixlQUFlOztZQUNuQixvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0QsTUFBTSxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7S0FBQTtJQUVELDRCQUE0QjtJQUN0QixlQUFlOztZQUNuQixvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLE1BQU0sQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsZUFBZSxDQUFDLENBQUM7WUFDM0QsTUFBTSxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7S0FBQTtJQUVELGdFQUFnRTtJQUMxRCxjQUFjOztZQUNsQixvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNwQixNQUFNLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDOztvQkFDdEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTt3QkFDekMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3hDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsUUFBUTs7b0NBQ2pELE1BQU0sV0FBVyxHQUFHLE1BQU0sZUFBZSxFQUFFLENBQUM7b0NBQzVDLElBQUksUUFBUSxJQUFJLFdBQVcsRUFBRTt3Q0FDM0IsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUE7cUNBQ3ZDO2dDQUNILENBQUM7NkJBQUEsQ0FBQyxDQUFDO3lCQUNKO3dCQUFBLENBQUM7b0JBQ0osQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQzthQUFBLENBQUMsQ0FBQTtZQUVGLGdGQUFnRjtZQUNoRixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFN0Isb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEIscUdBQXFHO1lBQ3JHLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxVQUFVO2dCQUMxRCxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ25ELENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUFBO0lBRUQsZ0RBQWdEO0lBQzFDLGNBQWM7O1lBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtnQkFDMUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsV0FBVzs7NEJBQ3BELE1BQU0sUUFBUSxHQUFHLE1BQU0sZUFBZSxFQUFFLENBQUM7NEJBQ3pDLElBQUksV0FBVyxJQUFJLFFBQVEsRUFBRTtnQ0FDM0IsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7NkJBQ3hDO3dCQUNILENBQUM7cUJBQUEsQ0FBQyxDQUFDO2lCQUNKO2dCQUFBLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7S0FBQTtJQUVGLG9HQUFvRztJQUM3RixXQUFXLENBQUMsTUFBTTs7WUFDdEIsTUFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hFLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsWUFBWTs7b0JBQ25FLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxDQUFDO2FBQUEsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUFBO0NBQ0Y7QUF4VkQsc0NBd1ZDO0FBRUQsMkNBQTJDO0FBQ3pDLFNBQWUsZUFBZTs7UUFDOUIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLFFBQVE7WUFDekQsaUJBQWlCO1lBQ2pCLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztDQUFBIn0=