"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Forgot_Password = void 0;
const protractor_1 = require("protractor");
class Forgot_Password {
    constructor() {
        this.forgotpassword_PageHeading = protractor_1.element(protractor_1.by.xpath('//mat-card-title[@class="forgot-pswd mat-card-title"]'));
        this.logo = protractor_1.element(protractor_1.by.xpath('//img[@class="logo"]'));
        this.Email_TextField = protractor_1.element(protractor_1.by.xpath('//input[@placeholder="Email"]'));
        this.submitBtn = protractor_1.element(protractor_1.by.xpath('//button[@type="submit"]'));
        this.Goback_toLogin = protractor_1.element(protractor_1.by.xpath('//a[@class="back-to-login"]'));
        this.success_ToastMessage = protractor_1.element(protractor_1.by.xpath('//div[@id="toast-container"]'));
        this.invalid_ErrorMessage = protractor_1.element(protractor_1.by.xpath('//span[text()="Please enter a valid Email."]'));
        this.email_ErrorMessage = protractor_1.element(protractor_1.by.xpath('//span[text()="Please enter your Email."]'));
        this.notExist_ToastMessage = protractor_1.element(protractor_1.by.xpath('//div[@class="toast-title"]'));
    }
}
exports.Forgot_Password = Forgot_Password;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9yZ290X1Bhc3N3b3JkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vcGFnZW9iamVjdHMvRm9yZ290X1Bhc3N3b3JkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLDJDQUF3RDtBQUd4RCxNQUFhLGVBQWU7SUFVeEI7UUFFSixJQUFJLENBQUMsMEJBQTBCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHVEQUF1RCxDQUFDLENBQUMsQ0FBQztRQUMzRyxJQUFJLENBQUMsSUFBSSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxTQUFTLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsY0FBYyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLG9CQUFvQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7UUFDNUUsSUFBSSxDQUFDLG9CQUFvQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDLENBQUM7UUFDNUYsSUFBSSxDQUFDLGtCQUFrQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDLENBQUM7UUFDdkYsSUFBSSxDQUFDLHFCQUFxQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztDQUNKO0FBdEJELDBDQXNCQyJ9