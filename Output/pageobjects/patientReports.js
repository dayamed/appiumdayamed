"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adherenceReport = exports.location = exports.notesTreatement = exports.reports = void 0;
const protractor_1 = require("protractor");
class reports {
    constructor() {
        this.notesTreatement_Card = protractor_1.element(protractor_1.by.xpath('//img[@alt="NotesAndTreatments"]'));
        this.location_card = protractor_1.element(protractor_1.by.xpath('//img[@alt="Location"]'));
        this.adherenceReport_card = protractor_1.element(protractor_1.by.xpath('//img[@alt="Adherence"]'));
        this.medicationReport_card = protractor_1.element(protractor_1.by.xpath('//img[@alt="Medication Report"]'));
        this.weeklyAdherence_ReportCard = protractor_1.element(protractor_1.by.xpath('//img[@alt="Weekly Adherence Report"]'));
        this.profileName = protractor_1.element(protractor_1.by.xpath('//mat-card-title[@class="title mat-card-title"]'));
        this.adherence_OnProfileCard = protractor_1.element(protractor_1.by.xpath('//mat-card-subtitle[@class="current mat-card-subtitle"]'));
        this.notification_icon = protractor_1.element(protractor_1.by.xpath('//img[@class="notif-icon"]'));
    }
}
exports.reports = reports;
class notesTreatement {
    constructor() {
        this.popup = protractor_1.element(protractor_1.by.xpath('//mat-dialog-container[@role="dialog"]'));
        this.patientName = protractor_1.element(protractor_1.by.xpath('//span[@mattooltip="Patient Name"]'));
        this.overallAdherence = protractor_1.element(protractor_1.by.xpath('//span[@mattooltip="Overall Adherence"]'));
        this.close_icon = protractor_1.element(protractor_1.by.xpath('//mat-icon[text()="clear"]'));
    }
}
exports.notesTreatement = notesTreatement;
class location {
    constructor() {
        this.popup = protractor_1.element(protractor_1.by.xpath('//div[@class="postion-relative"]'));
        this.mapView_Btn = protractor_1.element(protractor_1.by.xpath('//div[text()="Map view"]'));
        this.tableView_Btn = protractor_1.element(protractor_1.by.xpath('//div[text()="Table view"]'));
        this.mapView = protractor_1.element(protractor_1.by.xpath('//div[@id="eventMap"]'));
        this.tableView = protractor_1.element(protractor_1.by.xpath('//mat-tab-body[@class="mat-tab-body ng-tns-c51-1082 ng-star-inserted mat-tab-body-active"]'));
        this.close_icon = protractor_1.element(protractor_1.by.xpath('//mat-icon[text()="clear"]'));
    }
}
exports.location = location;
class adherenceReport {
    constructor() {
        this.patientName = protractor_1.element(protractor_1.by.xpath('//span[@mattooltip="Patient Name"]'));
        this.adherence = protractor_1.element(protractor_1.by.xpath('//span[@mattooltip="Overall Adherence"]'));
        this.close_icon = protractor_1.element(protractor_1.by.xpath('//mat-icon[text()="clear"]'));
        this.year_dropdown = protractor_1.element(protractor_1.by.xpath('//mat-select[@formcontrolname="year"]'));
        this.month_2021 = protractor_1.element(protractor_1.by.xpath('//span[text()=" 2021 "]'));
        this.month_2020 = protractor_1.element(protractor_1.by.xpath('//span[text()=" 2020 "]'));
        this.month_dropdown = protractor_1.element(protractor_1.by.xpath('//mat-select[@formcontrolname="month"]'));
        this.nov = protractor_1.element(protractor_1.by.xpath('//div//mat-option[11]'));
        this.october = protractor_1.element(protractor_1.by.xpath('//div//mat-option[10]'));
        this.dec = protractor_1.element(protractor_1.by.xpath('//div//mat-option[12]'));
        this.adherence_gragh = protractor_1.element(protractor_1.by.xpath('//canvas[@id="line-chart"]'));
        this.noData_msg = protractor_1.element(protractor_1.by.xpath('//p[text()=" No data available. "]'));
        this.exportPDF = protractor_1.element(protractor_1.by.xpath('//img[@mattooltip="Export in PDF"]'));
        this.logo = protractor_1.element(protractor_1.by.xpath('//span[@class="powered-by"]/parent::*'));
    }
}
exports.adherenceReport = adherenceReport;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF0aWVudFJlcG9ydHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlb2JqZWN0cy9wYXRpZW50UmVwb3J0cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBd0Q7QUFFeEQsTUFBYSxPQUFPO0lBVWhCO1FBR0ksSUFBSSxDQUFDLG9CQUFvQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLGFBQWEsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxvQkFBb0IsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxxQkFBcUIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaUNBQWlDLENBQUMsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQywwQkFBMEIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQyxDQUFDO1FBQzNGLElBQUksQ0FBQyxXQUFXLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLGlEQUFpRCxDQUFDLENBQUMsQ0FBQztRQUN0RixJQUFJLENBQUMsdUJBQXVCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlEQUF5RCxDQUFDLENBQUMsQ0FBQztRQUMxRyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztJQUUzRSxDQUFDO0NBRUo7QUF4QkQsMEJBd0JDO0FBQ0QsTUFBYSxlQUFlO0lBTXhCO1FBRUksSUFBSSxDQUFDLEtBQUssR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxXQUFXLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsZ0JBQWdCLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUMsQ0FBQztRQUNuRixJQUFJLENBQUMsVUFBVSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7SUFFcEUsQ0FBQztDQUNKO0FBZEQsMENBY0M7QUFDRCxNQUFhLFFBQVE7SUFRckI7UUFFSSxJQUFJLENBQUMsS0FBSyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLFdBQVcsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1FBQy9ELElBQUksQ0FBQyxhQUFhLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsT0FBTyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsNEZBQTRGLENBQUMsQ0FBQyxDQUFDO1FBQy9ILElBQUksQ0FBQyxVQUFVLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztJQUVwRSxDQUFDO0NBQ0E7QUFsQkQsNEJBa0JDO0FBQ0QsTUFBYSxlQUFlO0lBZXhCO1FBRUksSUFBSSxDQUFDLFdBQVcsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxTQUFTLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUMsQ0FBQztRQUM1RSxJQUFJLENBQUMsVUFBVSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLGFBQWEsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUNBQXVDLENBQUMsQ0FBQyxDQUFDO1FBQzlFLElBQUksQ0FBQyxVQUFVLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsVUFBVSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLGNBQWMsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxHQUFHLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsT0FBTyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLEdBQUcsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxlQUFlLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsVUFBVSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFNBQVMsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsb0NBQW9DLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxJQUFJLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxDQUFDLENBQUMsQ0FBQztJQUV6RSxDQUFDO0NBQ0o7QUFqQ0QsMENBaUNDIn0=