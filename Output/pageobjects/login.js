"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.login = void 0;
const protractor_1 = require("protractor");
class login {
    constructor() {
        this.heading = protractor_1.element(protractor_1.by.xpath('//mat-card-title[@class="login mat-card-title"]'));
        this.logo = protractor_1.element(protractor_1.by.xpath('//img[@class="logo"]'));
        this.userName = protractor_1.element(protractor_1.by.xpath("//input[@placeholder='Username']"));
        this.password = protractor_1.element(protractor_1.by.xpath("//input[@placeholder='Password']"));
        this.loginButton = protractor_1.element(protractor_1.by.xpath("//button/span[contains(text(), 'Login')]"));
        this.visibiltyBtn = protractor_1.element(protractor_1.by.xpath('//mat-icon[text()="visibility_off"]'));
        this.forgot_password = protractor_1.element(protractor_1.by.xpath('//a[@class="forgot-pswd"]'));
        this.error_msg_bothinvalid = protractor_1.element(protractor_1.by.xpath('//p[text()="Username or Password incorrect."]'));
        this.toast_mesg_forInvalidData = protractor_1.element(protractor_1.by.xpath('//div[@id="toast-container"]'));
        this.invalid_username_errorMsg = protractor_1.element(protractor_1.by.xpath('//span[text()="Please enter a valid Email."]'));
        this.Not_secure = protractor_1.element(protractor_1.by.xpath("//*[@id='main-message']/h1"));
        this.Adv_Btn = protractor_1.element(protractor_1.by.id('details-button'));
        this.Proceed_Btn = protractor_1.element(protractor_1.by.id('proceed-link'));
        this.Login_Err = protractor_1.element(protractor_1.by.xpath("//mat-form-field//mat-error"));
        this.username_error_message = protractor_1.element(protractor_1.by.xpath('//span[text()="Please enter your Username."]'));
        this.password_error_message = protractor_1.element(protractor_1.by.xpath('//span[text()="Please enter your Password."]'));
        this.appliedClaims = protractor_1.element(protractor_1.by.xpath("//*[contains(text(),'Applied Claims')]"));
        this.assignClaimPage = protractor_1.element(protractor_1.by.xpath("//*[contains(text(),'Assigned Claims')]"));
        this.adminPage = protractor_1.element(protractor_1.by.xpath("//mat-nav-list/app-menu[2]/a"));
    }
}
exports.login = login;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9wYWdlb2JqZWN0cy9sb2dpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSwyQ0FBcUQ7QUFFckQsTUFBYSxLQUFLO0lBc0JkO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsaURBQWlELENBQUMsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxJQUFJLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsUUFBUSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFFBQVEsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxXQUFXLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDBDQUEwQyxDQUFDLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsWUFBWSxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDLENBQUM7UUFDM0UsSUFBSSxDQUFDLGVBQWUsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxxQkFBcUIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsK0NBQStDLENBQUMsQ0FBQyxDQUFDO1FBQzlGLElBQUksQ0FBQyx5QkFBeUIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsOEJBQThCLENBQUMsQ0FBQyxDQUFDO1FBQ2pGLElBQUksQ0FBQyx5QkFBeUIsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsOENBQThDLENBQUMsQ0FBQyxDQUFDO1FBQ2pHLElBQUksQ0FBQyxVQUFVLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsT0FBTyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLHNCQUFzQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDLENBQUM7UUFDOUYsSUFBSSxDQUFDLHNCQUFzQixHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDLENBQUM7UUFFOUYsSUFBSSxDQUFDLGFBQWEsR0FBQyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxlQUFlLEdBQUMsb0JBQU8sQ0FBQyxlQUFFLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsU0FBUyxHQUFDLG9CQUFPLENBQUMsZUFBRSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLENBQUM7SUFFckUsQ0FBQztDQUNKO0FBOUNELHNCQThDQyJ9