"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const calculator_1 = require("../pageobjects/calculator");
const angularHomePage_1 = require("../pageobjects/angularHomePage");
var expect = require('chai').expect;
let calc = new calculator_1.calculator();
let angularPge = new angularHomePage_1.angularHomePage();
cucumber_1.Given('I will navigate to {string} website', (url) => __awaiter(void 0, void 0, void 0, function* () {
    yield protractor_1.browser.get(url);
}));
cucumber_1.When('I add two numbers {string} and {string}', (a, b) => __awaiter(void 0, void 0, void 0, function* () {
    yield calc.firstEditBox.sendKeys(a);
    yield calc.secondEditBox.sendKeys(b);
}));
cucumber_1.Then('the output displayed should be {string}', (c) => __awaiter(void 0, void 0, void 0, function* () {
    yield calc.go.click();
    yield calc.getResult.getText().then(function (text) {
        expect(text).to.equal(c);
    });
}));
cucumber_1.When('I enter {string} in search box', (searchText) => __awaiter(void 0, void 0, void 0, function* () {
    yield angularPge.searchBox.sendKeys(searchText);
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RlcHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwc0RlZmluaXRpb25zL3N0ZXBzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFtQztBQUduQywwREFBcUQ7QUFDckQsb0VBQStEO0FBRS9ELElBQUksTUFBTSxHQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFFbkMsSUFBSSxJQUFJLEdBQUMsSUFBSSx1QkFBVSxFQUFFLENBQUM7QUFDMUIsSUFBSSxVQUFVLEdBQUMsSUFBSSxpQ0FBZSxFQUFFLENBQUM7QUFDckMsZ0JBQUssQ0FBQyxxQ0FBcUMsRUFBRSxDQUFPLEdBQUcsRUFBQyxFQUFFO0lBQ3RELE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDM0IsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx5Q0FBeUMsRUFBRSxDQUFNLENBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRTtJQUN6RCxNQUFNLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BDLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDekMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyx5Q0FBeUMsRUFBRSxDQUFPLENBQUMsRUFBRSxFQUFFO0lBQ3hELE1BQU0sSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN0QixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVMsSUFBSTtRQUM3QyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3QixDQUFDLENBQUMsQ0FBQTtBQUNOLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFHSCxlQUFJLENBQUMsZ0NBQWdDLEVBQUUsQ0FBTyxVQUFVLEVBQUMsRUFBRTtJQUMxRCxNQUFNLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQ2pELENBQUMsQ0FBQSxDQUFDLENBQUMifQ==