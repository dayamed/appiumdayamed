"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const homePage_1 = require("../../pageobjects/homePage");
const editProvider_json_1 = __importDefault(require("../../TestData/editProvider.json"));
const patients_json_1 = __importDefault(require("../../TestData/patients.json"));
const addProvider_1 = require("../../pageobjects/addProvider");
const editProvider_1 = require("../../pageobjects/editProvider");
const patients_1 = require("../../pageobjects/patients");
const ptor_1 = require("protractor/built/ptor");
let expect = require('chai').expect;
let Login = new login_1.login();
let Homepage = new homePage_1.homePage();
let AddProvider = new addProvider_1.addProvider();
let EditProvider = new editProvider_1.editProvider();
let Patients = new patients_1.patients();
let AddPhysician = new patients_1.addPhysician();
cucumber_1.When('Click on view patient under any provider', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.search_provider.sendKeys(editProvider_json_1.default.providerName); //enter the provider name
        yield EditProvider.viewPatients.click(); //click on view patients
        yield protractor_1.browser.sleep(5000);
    });
});
cucumber_1.Then('verify the view and lazy loading', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //1.verify patients page view
        expect(yield protractor_1.browser.getTitle()).to.equal(patients_json_1.default.patientpage_title);
        //2.verify the list view, it should be displayed in list view
        expect(yield Patients.patient_listview.isPresent()).to.equal(true);
        //3.click on grid view and verify the view
        yield Patients.gridView_Btn.click();
        yield protractor_1.browser.sleep(5000);
        expect(yield Patients.patient_listview.isPresent()).to.equal(false);
        yield Patients.listView_Btn.click();
        yield protractor_1.browser.sleep(2000);
        //verify the lazy loading
        //1. verify the initial patients count on page, it should be 10 of max
        expect(yield Patients.patientsCount.count()).to.equal(10);
        //click on load more patients button untill all prroviders list displayed then verify the count
        // for(let i=2;expect(await Patients.loadMore_PatientBtn.isPresent());i++)
        // { 
        //     await browser.sleep(5000);
        //     await Patients.loadMore_PatientBtn.click();//click on load more patient button
        //     await  browser.sleep(5000);
        //     let max_count=await Patients.patientsCount.count();
        //     console.log(await max_count);
        //     await browser.actions().sendKeys(protractor.Key.END).perform();
        //     let element_status=expect(await Patients.loadMore_PatientBtn.isPresent())
        //     if(element_status)
        //     {
        //     expect(await Patients.patientsCount.count()).to.equal(i*10);
        //     }
        //    else
        //      {
        //          //verify the final count
        //          expect(await Patients.pagination.getText()).to.equal(patientData.max_count);
        //         // expect(await Homepage.pagination.getText()).to.contain.text(homepageData.ProvidersMax_Count);
        //       }
        //   }
    });
});
cucumber_1.When('Enter patient name then verify whether it is displayed and click on delete button', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //1.Enter patient name and verify whether it is displayed
        yield Patients.patientSearchBox.sendKeys(patients_json_1.default.patient_name); //enter name
        yield protractor_1.browser.sleep(20000);
        expect(yield Patients.patientDetails.getText()).to.equal(patients_json_1.default.fullName); //verify the name displayed
        //2.Then click on delete button
        yield Patients.delete_icon.click();
    });
});
cucumber_1.Then('verify the popup', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.sleep(3000);
        expect(yield Patients.delete_popup.isDisplayed()).to.equal(true); //verify whether popup is displayed
        expect(yield Patients.question_onDeletepopup.getText()).to.equal(patients_json_1.default.question_ondeletePopup); //verify the question
        //3.click on cancel button then vrify whether it closed
        yield Patients.cancelBtn_onDeletepopup.click();
        yield protractor_1.browser.sleep(3000);
        expect(yield Patients.delete_popup.isPresent()).to.equal(false);
    });
});
//Edit provider validations
//1.Edit provider with blank data in required fields then verify the error messages
cucumber_1.When('I\'m on edit patient page verify the disabled fields then leave the all required fields as empty and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //clear the data in required fields
        yield Patients.edit_icon.click();
        yield protractor_1.browser.sleep(5000);
        yield Patients.firstName.clear();
        yield Patients.lastName.clear();
        yield Patients.mobile_num.clear();
        yield Patients.emergencyContact_num.clear();
        yield Patients.emergencyContact_name.clear();
        yield Patients.health_planID.clear();
        yield Patients.insuranceAgency.clear();
        yield Patients.address.clear();
        yield Patients.zipCode.clear();
        yield Patients.diagnosis.clear();
        //Click on submit button
        yield Patients.submitBtn.click();
    });
});
//then verify the error messages
cucumber_1.Then('verify the error messages for required fields on edit patient page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield Patients.fname_ErrorMsg.getText()).to.equal(patients_json_1.default.error_Msg.fname_error);
        expect(yield Patients.lname_errorMsg.getText()).to.equal(patients_json_1.default.error_Msg.lname_error);
        expect(yield Patients.mobileNum_errorMsg.getText()).to.equal(patients_json_1.default.error_Msg.mobileNum_error);
        expect(yield Patients.ermergency_contactNumErr.getText()).to.equal(patients_json_1.default.error_Msg.ermergency_contactNumErr);
        expect(yield Patients.emergency_contactNameErr.getText()).to.equal(patients_json_1.default.error_Msg.emergency_contactNameErr);
        expect(yield Patients.health_planIDErr.getText()).to.equal(patients_json_1.default.error_Msg.health_planIDErr);
        expect(yield Patients.insuranceAgency_ErrMsg.getText()).to.equal(patients_json_1.default.error_Msg.insuranceAgency_ErrMsg);
        expect(yield Patients.address_errorMsg.getText()).to.equal(patients_json_1.default.error_Msg.address_error);
        expect(yield Patients.zipcode_error.getText()).to.equal(patients_json_1.default.error_Msg.zipcode_error);
        expect(yield Patients.diagnosis_ErrMsg.getText()).to.equal(patients_json_1.default.error_Msg.diagnosis_ErrMsg);
        yield protractor_1.browser.navigate().refresh();
        yield protractor_1.browser.sleep(7000);
    });
});
//2.Edit the all fields with invalid data then verify the error messages
cucumber_1.When('I\'m on edit patient page enter the invalid data in all fields and click on submit button', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //edit fields with invalid data
        yield Patients.firstName.click();
        yield Patients.firstName.clear();
        yield Patients.firstName.sendKeys(patients_json_1.default.invalid.first_name);
        yield Patients.lastName.click();
        yield Patients.lastName.clear();
        yield Patients.lastName.sendKeys(patients_json_1.default.invalid.last_name);
        yield Patients.nickName.clear();
        yield Patients.nickName.sendKeys(patients_json_1.default.invalid.nick_name);
        yield Patients.mobile_num.clear();
        yield Patients.mobile_num.sendKeys(patients_json_1.default.invalid.mobile_number);
        yield Patients.emergencyContact_num.clear();
        yield Patients.emergencyContact_num.sendKeys(patients_json_1.default.invalid.emergency_ContactNum);
        yield Patients.emergencyContact_name.clear();
        yield Patients.emergencyContact_name.sendKeys(patients_json_1.default.invalid.emergency_ContactName);
        yield Patients.health_planID.clear();
        yield Patients.health_planID.sendKeys(patients_json_1.default.invalid.health_planID);
        yield Patients.insuranceAgency.clear();
        yield Patients.insuranceAgency.sendKeys(patients_json_1.default.invalid.insuranceAgency);
        yield Patients.medicare_PlanID.clear();
        yield Patients.medicare_PlanID.sendKeys(patients_json_1.default.invalid.medicare_planId);
        yield Patients.address.clear();
        yield Patients.address.sendKeys(patients_json_1.default.invalid.address);
        yield Patients.city.clear();
        yield Patients.city.sendKeys(patients_json_1.default.invalid.city);
        yield Patients.state.clear();
        yield Patients.state.sendKeys(patients_json_1.default.invalid.state);
        yield Patients.zipCode.clear();
        yield Patients.zipCode.sendKeys(patients_json_1.default.invalid.zipCode);
        yield Patients.diagnosis.clear();
        yield Patients.diagnosis.sendKeys(patients_json_1.default.invalid.dignosis);
        //Click on submit button
        yield Patients.submitBtn.click();
    });
});
//then verify the error messages
cucumber_1.Then('verify the error messages on edit patient page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield Patients.invalidfname_ErrorMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.fname);
        expect(yield Patients.invalidlname_errorMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.lname);
        expect(yield Patients.invalidnickName_error.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.nickName);
        expect(yield Patients.invalidmobileNum_errorMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.mobileNum_error);
        expect(yield Patients.invalidermergency_contactNumErr.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.emergency_contactNum);
        expect(yield Patients.invalidemergency_contactNameErr.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.emergency_contactName);
        expect(yield Patients.invalidhealth_planIDErr.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.health_PlanID);
        expect(yield Patients.invalidinsuranceAgency_ErrMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.insuranceAgency);
        expect(yield Patients.invalidMedicarePlanID.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.medicare_planId);
        expect(yield Patients.invalidaddress_errorMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.address_error);
        expect(yield Patients.invalidcity_error.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.city);
        expect(yield Patients.invalidState_errorMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.State);
        expect(yield Patients.invalidzipcode_error.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.zipcode_error);
        expect(yield Patients.invaliddiagnosis_ErrMsg.getText()).to.equal(patients_json_1.default.invalid_ErrorMsg.diagnosis);
        yield protractor_1.browser.navigate().refresh();
        yield protractor_1.browser.sleep(7000);
    });
});
//3.edit the all the fields with valid data and click on submit then verify the success message
cucumber_1.When('I\'m on edit patient page Enter valid data in all fields and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //edit the fields with valid data
        yield Patients.firstName.click();
        yield Patients.firstName.clear();
        yield Patients.firstName.sendKeys(patients_json_1.default.valid_info1.fname);
        yield Patients.lastName.click();
        yield Patients.lastName.clear();
        yield Patients.lastName.sendKeys(patients_json_1.default.valid_info1.lname);
        yield Patients.nickName.clear();
        yield Patients.nickName.sendKeys(patients_json_1.default.valid_info1.nickName);
        //edit DOB
        yield Patients.DOB.click();
        yield Patients.select_Date4.click();
        yield Patients.gender_Male.click();
        yield Patients.medication_dropdown.click();
        yield Patients._7dayPill.click();
        yield Patients.contryCode_dropdown.click();
        yield protractor_1.browser.sleep(2000);
        yield Patients.IND_code.click();
        yield Patients.mobile_num.clear();
        yield Patients.mobile_num.sendKeys(patients_json_1.default.valid_info1.mobileNum);
        yield Patients.emergencyContact_num.clear();
        yield Patients.emergencyContact_num.sendKeys(patients_json_1.default.valid_info1.emergency_contactNum);
        yield Patients.emergencyContact_name.clear();
        yield Patients.emergencyContact_name.sendKeys(patients_json_1.default.valid_info1.emergency_contactName);
        yield protractor_1.browser.sleep(5000);
        yield Patients.contact_relation.click();
        yield Patients.option_father.click(); //FATHER
        yield Patients.health_planID.clear();
        yield Patients.health_planID.sendKeys(patients_json_1.default.valid_info1.healthPlanId);
        yield Patients.insuranceAgency.clear();
        yield Patients.insuranceAgency.sendKeys(patients_json_1.default.valid_info1.insuranceAgency);
        yield Patients.medicare_PlanID.clear();
        yield Patients.medicare_PlanID.sendKeys(patients_json_1.default.valid_info1.medicare_planId);
        yield Patients.address.clear();
        yield Patients.address.sendKeys(patients_json_1.default.valid_info1.address);
        yield Patients.city.clear();
        yield Patients.city.sendKeys(patients_json_1.default.valid_info1.city);
        yield Patients.state.clear();
        yield Patients.state.sendKeys(patients_json_1.default.valid_info1.State);
        yield Patients.zipCode.clear();
        yield Patients.zipCode.sendKeys(patients_json_1.default.valid_info1.zipcode);
        yield Patients.diagnosis.clear();
        yield Patients.diagnosis.sendKeys(patients_json_1.default.valid_info1.diagnosis);
        //smart phone type
        yield Patients.phoneType_dropdown.click();
        yield Patients.select_ios.click();
        yield Patients.upload_ImgBtn.sendKeys(patients_json_1.default.img_path);
        // await Patients.email_notificationSwitch.click();
        //await Patients.text_notificationSwitch.click();
        //Click on submit button
        yield Patients.submitBtn.click();
        yield protractor_1.browser.sleep(8000);
    });
});
// Then('verify the success message after sumbit patient details',async function () {
//  // Page should returned to patient list page and success message should displayed
//  expect(await browser.getTitle()).to.equal(patientData.patientpage_title);
//  //verify the success message
//  expect(await Patients.successMsg.isDisplayed()).to.equal(true);
//  expect(await Patients.successMsg.getText()).to.equal(patientData.succsess_msg);
//});
cucumber_1.When('I\'m on edit patient page Enter valid data in required fields and click on submit then verify the success message', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Patients.patientSearchBox.sendKeys(patients_json_1.default.patient_name); //enter name
        yield protractor_1.browser.sleep(20000);
        yield Patients.edit_icon.click();
        yield protractor_1.browser.sleep(5000);
        yield Patients.firstName.click();
        yield Patients.firstName.clear();
        yield Patients.firstName.sendKeys(patients_json_1.default.valid_info2.fname);
        yield Patients.lastName.click();
        yield Patients.lastName.clear();
        yield Patients.lastName.sendKeys(patients_json_1.default.valid_info2.lname);
        yield protractor_1.browser.sleep(2000);
        yield Patients.DOB.click();
        yield protractor_1.browser.sleep(2000);
        yield Patients.select_Date5.click();
        yield Patients.contryCode_dropdown.click();
        yield Patients.USA_code.click();
        yield Patients.mobile_num.clear();
        yield Patients.mobile_num.sendKeys(patients_json_1.default.valid_info2.mobileNum);
        yield Patients.emergencyContact_num.clear();
        yield Patients.emergencyContact_num.sendKeys(patients_json_1.default.valid_info2.emergency_contactNum);
        yield Patients.emergencyContact_name.clear();
        yield Patients.emergencyContact_name.sendKeys(patients_json_1.default.valid_info2.emergency_contactName);
        yield Patients.health_planID.clear();
        yield Patients.health_planID.sendKeys(patients_json_1.default.valid_info2.healthPlanId);
        yield Patients.insuranceAgency.clear();
        yield Patients.insuranceAgency.sendKeys(patients_json_1.default.valid_info2.insuranceAgency);
        yield Patients.address.clear();
        yield Patients.address.sendKeys(patients_json_1.default.valid_info2.address);
        yield Patients.zipCode.clear();
        yield Patients.zipCode.sendKeys(patients_json_1.default.valid_info2.zipcode);
        yield Patients.diagnosis.clear();
        yield Patients.diagnosis.sendKeys(patients_json_1.default.valid_info2.diagnosis);
        yield Patients.submitBtn.click();
    });
});
//1.add physician with blank data
cucumber_1.Then('I\'m on edit patient page click on add physician and click on submit without entering any data', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Patients.patientSearchBox.sendKeys(patients_json_1.default.patient_name); //enter name
        yield protractor_1.browser.sleep(20000);
        yield Patients.edit_icon.click();
        yield protractor_1.browser.sleep(5000);
        //leave fields as empty then click on submit button
        yield AddPhysician.add_physicianBtn.click();
        yield protractor_1.browser.sleep(2000);
        yield protractor_1.browser.actions().sendKeys(ptor_1.protractor.Key.END).perform();
        yield AddPhysician.submitBtn.click();
    });
});
cucumber_1.Then('verify the error messages on add physician page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield AddPhysician.fname_ErrorMsg.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.fname_error);
        expect(yield AddPhysician.lname_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.lname_error);
        expect(yield AddPhysician.mobileNum_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.mobileNum_error);
        expect(yield AddPhysician.email_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.Email_Error);
        expect(yield AddPhysician.address_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.address_error);
        expect(yield AddPhysician.zipCode_error.getText()).to.equal(patients_json_1.default.addPhysician.error_Msg.zipcode_error);
        yield AddPhysician.cancelBtn.click();
    });
});
//2.Enter invalid data in all fields then verify the error messages
cucumber_1.When('I\'m on edit patient page click on add physician, enter invalid data in all fields then verify the error messages', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //Enter invalid data in all
        yield AddPhysician.add_physicianBtn.click();
        yield protractor_1.browser.sleep(2000);
        yield AddPhysician.firstName.sendKeys(patients_json_1.default.addPhysician.invalid.firstName);
        yield AddPhysician.lastName.sendKeys(patients_json_1.default.addPhysician.invalid.lastName);
        yield AddPhysician.nickName.sendKeys(patients_json_1.default.addPhysician.invalid.nickName);
        yield AddPhysician.npi_Num.sendKeys(patients_json_1.default.addPhysician.invalid.NPI_num);
        yield AddPhysician.dea_Num.sendKeys(patients_json_1.default.addPhysician.invalid.DEA_Num);
        yield AddPhysician.mobile_num.sendKeys(patients_json_1.default.addPhysician.invalid.mobile_num);
        yield AddPhysician.Email.sendKeys(patients_json_1.default.addPhysician.invalid.email);
        yield AddPhysician.address.sendKeys(patients_json_1.default.addPhysician.invalid.address);
        yield AddPhysician.city.sendKeys(patients_json_1.default.addPhysician.invalid.city);
        yield AddPhysician.state.sendKeys(patients_json_1.default.addPhysician.invalid.state);
        yield AddPhysician.country.sendKeys(patients_json_1.default.addPhysician.invalid.country);
        yield AddPhysician.zipCode.sendKeys(patients_json_1.default.addPhysician.invalid.zipcode);
        yield protractor_1.browser.actions().sendKeys(ptor_1.protractor.Key.END).perform();
        yield AddPhysician.submitBtn.click();
        //verify the all error messages:
        expect(yield AddPhysician.invalidfname_ErrorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.fname);
        expect(yield AddPhysician.invalidlname_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.lname);
        expect(yield AddPhysician.invalidnickName_error.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.nickName);
        expect(yield AddPhysician.invalidmobileNum_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.mobileNum);
        expect(yield AddPhysician.invalidEmail_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.Email);
        expect(yield AddPhysician.invalidaddress_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.address);
        expect(yield AddPhysician.invalidcity_error.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.city);
        expect(yield AddPhysician.invalidState_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.state);
        expect(yield AddPhysician.invalidCoutry_errorMsg.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.country);
        expect(yield AddPhysician.invalidzipcode_error.getText()).to.equal(patients_json_1.default.addPhysician.invalid_ErrMsg.zipcode);
        yield AddPhysician.cancelBtn.click();
    });
});
cucumber_1.When('I\'m on add physician page enter the valid data in all the fields and click on submit then verify the success message', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield AddPhysician.add_physicianBtn.click();
        yield protractor_1.browser.sleep(2000);
        yield AddPhysician.firstName.sendKeys(patients_json_1.default.addPhysician.valid_info1.firstName);
        yield AddPhysician.lastName.sendKeys(patients_json_1.default.addPhysician.valid_info1.lastName);
        yield AddPhysician.nickName.sendKeys(patients_json_1.default.addPhysician.valid_info1.nickName);
        yield AddPhysician.npi_Num.sendKeys(patients_json_1.default.addPhysician.valid_info1.NPI_num);
        yield AddPhysician.gender_Female.click();
        yield AddPhysician.dea_Num.sendKeys(patients_json_1.default.addPhysician.valid_info1.DEA_Num);
        yield AddPhysician.mobile_num.sendKeys(patients_json_1.default.addPhysician.valid_info1.mobile_num);
        yield AddPhysician.Email.sendKeys(patients_json_1.default.addPhysician.valid_info1.email);
        yield AddPhysician.address.sendKeys(patients_json_1.default.addPhysician.valid_info1.address);
        yield AddPhysician.city.sendKeys(patients_json_1.default.addPhysician.valid_info1.city);
        yield AddPhysician.state.sendKeys(patients_json_1.default.addPhysician.valid_info1.state);
        yield AddPhysician.country.sendKeys(patients_json_1.default.addPhysician.valid_info1.country);
        yield AddPhysician.zipCode.sendKeys(patients_json_1.default.addPhysician.valid_info1.zipcode);
        yield AddPhysician.phoneType_dropdown.click();
        yield AddPhysician.select_android.click();
        yield protractor_1.browser.sleep(2000);
        yield protractor_1.browser.actions().sendKeys(ptor_1.protractor.Key.END).perform();
        yield AddPhysician.email_notificationSwitch.click(); //enabled
        yield AddPhysician.text_notificationSwitch.click(); //enabled
        yield AddPhysician.submitBtn.click();
        //verify the success message:
        yield protractor_1.browser.sleep(5000);
        expect(yield AddPhysician.success_msg.isDisplayed()).to.equal(true);
    });
});
cucumber_1.When('I\'m on add physician page enter the valid data in required fields and click on submit then verify the success message', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //enter the valid data in required fields
        yield AddPhysician.add_physicianBtn.click();
        yield protractor_1.browser.sleep(2000);
        yield AddPhysician.firstName.sendKeys(patients_json_1.default.addPhysician.valid_info2.firstName);
        yield AddPhysician.lastName.sendKeys(patients_json_1.default.addPhysician.valid_info2.lastName);
        yield AddPhysician.mobile_num.sendKeys(patients_json_1.default.addPhysician.valid_info2.mobile_num);
        yield AddPhysician.Email.sendKeys(patients_json_1.default.addPhysician.valid_info2.email);
        yield AddPhysician.address.sendKeys(patients_json_1.default.addPhysician.valid_info2.address);
        yield AddPhysician.zipCode.sendKeys(patients_json_1.default.addPhysician.valid_info2.zipcode);
        yield protractor_1.browser.actions().sendKeys(ptor_1.protractor.Key.END).perform();
        yield AddPhysician.submitBtn.click(); //click on submit
        //verify the success message:
        yield protractor_1.browser.sleep(5000);
        expect(yield AddPhysician.success_msg.isDisplayed()).to.equal(true);
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF0aWVudHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9QYXRpZW50cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUNBLHVDQUE2QztBQUM3QywyQ0FBb0M7QUFDcEMsbURBQWdEO0FBQ2hELHlEQUFvRDtBQUNwRCx5RkFBZ0U7QUFDaEUsaUZBQXVEO0FBQ3ZELCtEQUEwRDtBQUMxRCxpRUFBNEQ7QUFFNUQseURBQW1FO0FBQ25FLGdEQUFtRDtBQUNuRCxJQUFJLE1BQU0sR0FBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQ25DLElBQUksS0FBSyxHQUFDLElBQUksYUFBSyxFQUFFLENBQUM7QUFDdEIsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxXQUFXLEdBQUMsSUFBSSx5QkFBVyxFQUFFLENBQUM7QUFDbEMsSUFBSSxZQUFZLEdBQUMsSUFBSSwyQkFBWSxFQUFFLENBQUM7QUFDcEMsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxZQUFZLEdBQUMsSUFBSSx1QkFBWSxFQUFFLENBQUM7QUFFcEMsZUFBSSxDQUFDLDBDQUEwQyxFQUFFOztRQUMvQyxNQUFNLFFBQVEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUEseUJBQXlCO1FBQ2hHLE1BQU0sWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHdCQUF3QjtRQUNoRSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxlQUFJLENBQUMsa0NBQWtDLEVBQUU7O1FBRXpDLDZCQUE2QjtRQUMxQixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDMUUsNkRBQTZEO1FBQzVELE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkUsMENBQTBDO1FBQzFDLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEUsTUFBTSxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFM0IseUJBQXlCO1FBQ3pCLHNFQUFzRTtRQUN0RSxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUxRCwrRkFBK0Y7UUFDL0YsMEVBQTBFO1FBQzFFLEtBQUs7UUFDTCxpQ0FBaUM7UUFDakMscUZBQXFGO1FBQ3JGLGtDQUFrQztRQUNsQywwREFBMEQ7UUFDMUQsb0NBQW9DO1FBQ3BDLHNFQUFzRTtRQUV0RSxnRkFBZ0Y7UUFDaEYseUJBQXlCO1FBQ3pCLFFBQVE7UUFDUixtRUFBbUU7UUFDbkUsUUFBUTtRQUVSLFVBQVU7UUFDVixTQUFTO1FBQ1Qsb0NBQW9DO1FBQ3BDLHdGQUF3RjtRQUN4RiwyR0FBMkc7UUFDM0csVUFBVTtRQUVWLE1BQU07SUFFTixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsZUFBSSxDQUFDLG1GQUFtRixFQUFFOztRQUN4Rix5REFBeUQ7UUFDekQsTUFBTSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQSxZQUFZO1FBQy9FLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBLDJCQUEyQjtRQUMxRywrQkFBK0I7UUFDL0IsTUFBTSxRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3JDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsa0JBQWtCLEVBQUM7O1FBQ3RCLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxtQ0FBbUM7UUFDcEcsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQSxxQkFBcUI7UUFDMUgsdURBQXVEO1FBQ3ZELE1BQU0sUUFBUSxDQUFDLHVCQUF1QixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQy9DLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFbEUsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNMLDJCQUEyQjtBQUMzQixtRkFBbUY7QUFDbkYsZUFBSSxDQUFDLHNIQUFzSCxFQUFDOztRQUMzSCxtQ0FBbUM7UUFDbkMsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsTUFBTSxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsTUFBTSxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDL0IsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQy9CLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyx3QkFBd0I7UUFDeEIsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBR2xDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxnQ0FBZ0M7QUFDaEMsZUFBSSxDQUFDLG9FQUFvRSxFQUFDOztRQUV4RSxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1RixNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM1RixNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxTQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNuSCxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsd0JBQXdCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsU0FBUyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDbkgsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ25HLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxTQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUMvRyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hHLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzdGLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNuRyxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsd0VBQXdFO0FBQ3hFLGVBQUksQ0FBQywyRkFBMkYsRUFBRTs7UUFDaEcsK0JBQStCO1FBQy9CLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNsRSxNQUFNLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsTUFBTSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEUsTUFBTSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEUsTUFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2xDLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEUsTUFBTSxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdkYsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDekYsTUFBTSxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JDLE1BQU0sUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDekUsTUFBTSxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDN0UsTUFBTSxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDN0UsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQy9CLE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0QsTUFBTSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVCLE1BQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkQsTUFBTSxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzdCLE1BQU0sUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDekQsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQy9CLE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0QsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEUsd0JBQXdCO1FBQ3hCLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUVsQyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsZ0NBQWdDO0FBQ2hDLGVBQUksQ0FBQyxnREFBZ0QsRUFBQzs7UUFDcEQsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwRyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkcsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHlCQUF5QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ2xILE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQywrQkFBK0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQzdILE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQywrQkFBK0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlILE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5RyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsNkJBQTZCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdEgsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzlHLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5RyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0YsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMzRyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUcsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ25DLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDNUIsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILCtGQUErRjtBQUMvRixlQUFJLENBQUMsOEVBQThFLEVBQUM7O1FBQ25GLGlDQUFpQztRQUNqQyxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakUsTUFBTSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hFLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRW5FLFVBQVU7UUFDVixNQUFNLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0IsTUFBTSxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BDLE1BQU0sUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQyxNQUFNLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMzQyxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxRQUFRLENBQUMsbUJBQW1CLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0MsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsTUFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2xDLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEUsTUFBTSxRQUFRLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxRQUFRLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDM0YsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDN0YsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QyxNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSxRQUFRO1FBQzdDLE1BQU0sUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQyxNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVFLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxNQUFNLFFBQVEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ2pGLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxNQUFNLFFBQVEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ2pGLE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMvQixNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pFLE1BQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1QixNQUFNLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNELE1BQU0sUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixNQUFNLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdELE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMvQixNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pFLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JFLGtCQUFrQjtRQUNsQixNQUFNLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMxQyxNQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsTUFBTSxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdELG1EQUFtRDtRQUNsRCxpREFBaUQ7UUFDakQsd0JBQXdCO1FBQ3hCLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRzNCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxxRkFBcUY7QUFDckYscUZBQXFGO0FBQ3JGLDZFQUE2RTtBQUM3RSxnQ0FBZ0M7QUFDaEMsbUVBQW1FO0FBQ25FLG1GQUFtRjtBQUVuRixLQUFLO0FBQ0wsZUFBSSxDQUFDLG1IQUFtSCxFQUFDOztRQUN2SCxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFBLFlBQVk7UUFDL0UsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakUsTUFBTSxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hFLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzNCLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BDLE1BQU0sUUFBUSxDQUFDLG1CQUFtQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzNDLE1BQU0sUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbEMsTUFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0RSxNQUFNLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1QyxNQUFNLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUMzRixNQUFNLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxNQUFNLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUM3RixNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckMsTUFBTSxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1RSxNQUFNLFFBQVEsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkMsTUFBTSxRQUFRLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNqRixNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDL0IsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqRSxNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDL0IsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNqRSxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDakMsTUFBTSxRQUFRLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyRSxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFFbkMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILGlDQUFpQztBQUNqQyxlQUFJLENBQUMsZ0dBQWdHLEVBQUU7O1FBQ3JHLE1BQU0sUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUEsWUFBWTtRQUMvRSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLE1BQU0sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLG1EQUFtRDtRQUNuRCxNQUFNLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1QyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsaUJBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDL0QsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsaURBQWlELEVBQUM7O1FBQ3JELE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM3RyxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDN0csTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDckgsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzdHLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pILE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5RyxNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDdkMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILG1FQUFtRTtBQUVuRSxlQUFJLENBQUMsbUhBQW1ILEVBQUM7O1FBQ3pILDJCQUEyQjtRQUMxQixNQUFNLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1QyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xGLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hGLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hGLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLE1BQU0sWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BGLE1BQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFFLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLE1BQU0sWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hFLE1BQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFFLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsdUJBQVcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzlFLE1BQU0sb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsaUJBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDOUQsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLGdDQUFnQztRQUNoQyxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuSCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuSCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN0SCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMseUJBQXlCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMzSCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuSCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsdUJBQXVCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2SCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5RyxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuSCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsc0JBQXNCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0SCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNwSCxNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7SUFFdEMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILGVBQUksQ0FBQyx1SEFBdUgsRUFBQzs7UUFDM0gsTUFBTSxZQUFZLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0RixNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRixNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRixNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRixNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDekMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEYsTUFBTSxZQUFZLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEYsTUFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUUsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEYsTUFBTSxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUUsTUFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUUsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEYsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEYsTUFBTSxZQUFZLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUMsTUFBTSxZQUFZLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMvRCxNQUFNLFlBQVksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLFNBQVM7UUFDN0QsTUFBTSxZQUFZLENBQUMsdUJBQXVCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSxTQUFTO1FBQzVELE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUVyQyw2QkFBNkI7UUFDNUIsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV2RSxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHdIQUF3SCxFQUFDOztRQUM5SCx5Q0FBeUM7UUFDdkMsTUFBTSxZQUFZLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0RixNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNwRixNQUFNLFlBQVksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4RixNQUFNLFlBQVksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RSxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRixNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLHVCQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRixNQUFNLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLGlCQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQy9ELE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLGlCQUFpQjtRQUVyRCw2QkFBNkI7UUFDN0IsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV2RSxDQUFDO0NBQUEsQ0FBQyxDQUFDIn0=