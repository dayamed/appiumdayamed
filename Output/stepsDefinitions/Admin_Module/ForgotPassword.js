"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const loginData_json_1 = __importDefault(require("../../TestData/loginData.json"));
const Forgot_Password_1 = require("../../pageobjects/Forgot_Password");
const forgotPasswordPage_json_1 = __importDefault(require("../../TestData/forgotPasswordPage.json"));
let expect = require('chai').expect;
let Login = new login_1.login();
let forgotpwd = new Forgot_Password_1.Forgot_Password();
//Enter Url on browser and navigate to the Dayamed QA web portal 
cucumber_1.Given('I navigate to the Dayamed QA web portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.waitForAngularEnabled(false);
        yield protractor_1.browser.manage().window().maximize();
        yield protractor_1.browser.get(loginData_json_1.default.URL);
        try {
            yield Login.Adv_Btn.click();
            yield Login.Proceed_Btn.click();
            protractor_1.browser.sleep(5000);
        }
        catch (error) {
            console.log(error);
        }
    });
});
// verify the page title and fields present on the page
cucumber_1.When('verify the all fields on the page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.forgot_password.click(); // click on forgot password to navigate to the forgaot password page
        expect(yield protractor_1.browser.getTitle()).to.equal(forgotPasswordPage_json_1.default.forgotPassword_PageTitle); //verify the page title
        expect(yield forgotpwd.forgotpassword_PageHeading.isPresent()).to.equal(true); //hesding
        expect(yield forgotpwd.logo.isPresent()).to.equal(true); //Logo
        expect(yield forgotpwd.Email_TextField.isPresent()).to.equal(true); //Email Text field
        expect(yield forgotpwd.submitBtn.isPresent()).to.equal(true); //sumbit button 
        expect(yield forgotpwd.Goback_toLogin.isPresent()).to.equal(true); //Go back to Login Link
        // Click on "Go back to Login Link" and verify whether it returned to Login page 
        yield forgotpwd.Goback_toLogin.click(); //click on "Go back to Login Link"
        expect(yield protractor_1.browser.getTitle()).to.equal(loginData_json_1.default.LoginPage_Title); //verify the page title
        //return to the forgot password page again
        // await browser.navigate().forward();// 
        yield Login.forgot_password.click();
        yield protractor_1.browser.sleep(5000);
    });
});
// Try to click on submit  with Invalid Email
cucumber_1.Then('Enter the invalid Email and click on submit then verify the error messages', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //1.Leave the Email fiels as empty and click on sumbit
        yield forgotpwd.submitBtn.click(); //click on submit button
        protractor_1.browser.sleep(5000);
        expect(yield forgotpwd.email_ErrorMessage.isDisplayed()).to.equal(true); //verify whether error message displayed
        expect(yield forgotpwd.email_ErrorMessage.getText()).to.equal(forgotPasswordPage_json_1.default.Email_ErrorMessage); //verify the error message 
        yield protractor_1.browser.navigate().refresh(); // page refresh
        //2.Enter Invalid username and verify the error message
        yield forgotpwd.Email_TextField.sendKeys(loginData_json_1.default.invalid_userName); //Enter the Email
        yield forgotpwd.submitBtn.click(); //Click on submit button
        expect(yield forgotpwd.invalid_ErrorMessage.isDisplayed()).to.equal(true); //verify whether message displayed
        expect(yield forgotpwd.invalid_ErrorMessage.getText()).to.equal(forgotPasswordPage_json_1.default.Invalid_ErrorMessage); //verify the message
        yield protractor_1.browser.navigate().refresh();
        //3.Enter the Email which does not exixt and click on submit then verify the toast message
        yield forgotpwd.Email_TextField.sendKeys(loginData_json_1.default.notExist_userName); //Enter the Email which not exist
        yield forgotpwd.submitBtn.click(); //click on submit button
        protractor_1.browser.sleep(2000);
        expect(yield forgotpwd.notExist_ToastMessage.isDisplayed()).to.equal(true); //verify whether toast message displayed
        expect(yield forgotpwd.notExist_ToastMessage.getText()).to.equal(forgotPasswordPage_json_1.default.NotExist_ToastMessage); //verify the toast message
    });
});
// click on submit  with valid Email and verify the Success toast Message
// Then('Enter the valid Email and click on submit then verify the the success message', async function () {
//   await forgotpwd.Email_TextField.sendKeys(loginData.valid_userName);//enter valid Email
//   await forgotpwd.submitBtn.click();// Click on Submit Button
//   //page should returned to login page and diaplayed the message
//   expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);//verify the page title
//   expect(await forgotpwd.success_ToastMessage.isDisplayed()).to.equal(true);//verify whether Success toast Message diaplayed
//   expect(await forgotpwd.success_ToastMessage.getText()).to.equal(forgotPassword.Success_Message);//verify the message
// });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRm9yZ290UGFzc3dvcmQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9Gb3Jnb3RQYXNzd29yZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBb0M7QUFDcEMsbURBQWdEO0FBQ2hELG1GQUF3RDtBQUN4RCx1RUFBaUU7QUFDakUscUdBQW9FO0FBQ3BFLElBQUksTUFBTSxHQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFFbkMsSUFBSSxLQUFLLEdBQUMsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUN0QixJQUFJLFNBQVMsR0FBQyxJQUFJLGlDQUFlLEVBQUUsQ0FBQztBQUNwQyxpRUFBaUU7QUFDaEUsZ0JBQUssQ0FBQyx5Q0FBeUMsRUFBRTs7UUFDaEQsb0JBQU8sQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUNwQyxNQUFNLG9CQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0MsTUFBTSxvQkFBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pDLElBQUc7WUFDQyxNQUFNLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDNUIsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BCO1FBQ0osT0FBTSxLQUFLLEVBQ1I7WUFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO0lBRUosQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILHVEQUF1RDtBQUN2RCxlQUFJLENBQUMsbUNBQW1DLEVBQUM7O1FBQ3ZDLE1BQU0sS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLG9FQUFvRTtRQUN4RyxNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQSx1QkFBdUI7UUFDMUcsTUFBTSxDQUFDLE1BQU0sU0FBUyxDQUFDLDBCQUEwQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDdkYsTUFBTSxDQUFDLE1BQU0sU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxNQUFNO1FBQzlELE1BQU0sQ0FBQyxNQUFNLFNBQVMsQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsa0JBQWtCO1FBQ3JGLE1BQU0sQ0FBQyxNQUFNLFNBQVMsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsZ0JBQWdCO1FBQzdFLE1BQU0sQ0FBQyxNQUFNLFNBQVMsQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsdUJBQXVCO1FBQ3pGLGlGQUFpRjtRQUNqRixNQUFNLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSxrQ0FBa0M7UUFDekUsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFBLHVCQUF1QjtRQUM1RiwwQ0FBMEM7UUFDMUMseUNBQXlDO1FBQ3pDLE1BQU0sS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTVCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCw2Q0FBNkM7QUFDN0MsZUFBSSxDQUFDLDRFQUE0RSxFQUFFOztRQUVoRixzREFBc0Q7UUFDdEQsTUFBTSxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsd0JBQXdCO1FBQzFELG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxNQUFNLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSx3Q0FBd0M7UUFDaEgsTUFBTSxDQUFDLE1BQU0sU0FBUyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQSwyQkFBMkI7UUFDNUgsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUEsZUFBZTtRQUVuRCx1REFBdUQ7UUFDdkQsTUFBTSxTQUFTLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQSxpQkFBaUI7UUFDdEYsTUFBTSxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsd0JBQXdCO1FBQzFELE1BQU0sQ0FBQyxNQUFNLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxrQ0FBa0M7UUFDNUcsTUFBTSxDQUFDLE1BQU0sU0FBUyxDQUFDLG9CQUFvQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBYyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQSxvQkFBb0I7UUFDekgsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRW5DLDBGQUEwRjtRQUMxRixNQUFNLFNBQVMsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFBLGlDQUFpQztRQUN2RyxNQUFNLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSx3QkFBd0I7UUFDMUQsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsTUFBTSxDQUFDLE1BQU0sU0FBUyxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLHdDQUF3QztRQUNuSCxNQUFNLENBQUMsTUFBTSxTQUFTLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLGlDQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFBLDBCQUEwQjtJQUVuSSxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gseUVBQXlFO0FBQ3pFLDRHQUE0RztBQUM1RywyRkFBMkY7QUFDM0YsZ0VBQWdFO0FBQ2hFLG1FQUFtRTtBQUNuRSxpR0FBaUc7QUFDakcsK0hBQStIO0FBQy9ILHlIQUF5SDtBQUd6SCxNQUFNIn0=