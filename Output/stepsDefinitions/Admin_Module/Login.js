"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const loginData_json_1 = __importDefault(require("../../TestData/loginData.json"));
const homePage_json_1 = __importDefault(require("../../TestData/homePage.json"));
const forgotPasswordPage_json_1 = __importDefault(require("../../TestData/forgotPasswordPage.json"));
let expect = require('chai').expect;
let Login = new login_1.login();
cucumber_1.Given('I navigate to the Dayamed QA portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.waitForAngularEnabled(false);
        yield protractor_1.browser.manage().window().maximize();
        yield protractor_1.browser.get(loginData_json_1.default.URL);
        try {
            yield Login.Adv_Btn.click();
            yield Login.Proceed_Btn.click();
            protractor_1.browser.sleep(5000);
        }
        catch (error) {
            console.log(error);
        }
    });
});
//verify the all fileds present on the page:
cucumber_1.When('I\'m on login page verify the fileds', function () {
    return __awaiter(this, void 0, void 0, function* () {
        // browser.waitForAngularEnabled(true);
        protractor_1.browser.sleep(5000);
        //verify the login page title
        expect(yield protractor_1.browser.getTitle()).to.equal(loginData_json_1.default.LoginPage_Title);
        // verify the all fields whethr they are present aaaaare not 
        expect(yield Login.heading.isPresent()).to.equal(true); //heading
        expect(yield Login.logo.isPresent()).to.equal(true); //Dayamed Logo
        expect(yield Login.userName.isPresent()).to.equal(true); //user name Text field
        expect(yield Login.password.isPresent()).to.equal(true); //password text field
        expect(yield Login.forgot_password.isPresent()).to.equal(true); //forgot password link
        expect(yield Login.visibiltyBtn.isPresent()).to.equal(true); //password visibility Icon
        expect(yield Login.loginButton.isPresent()).to.equal(true); //login button
    });
});
//login with invaluid credentials:
cucumber_1.Then('enter the invalid credentials,click on login and verify the error messages', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //1.leave the username and password as empty and click on login then verify the error messages:
        yield Login.loginButton.click(); //click on login button
        protractor_1.browser.sleep(5000);
        expect(yield Login.username_error_message.getText()).to.equal(loginData_json_1.default.Username_Error_Message); //verify the error message under username field
        expect(yield Login.password_error_message.getText()).to.equal(loginData_json_1.default.password_Error_Message); //verify the error ,essage under the password field
        yield protractor_1.browser.navigate().refresh();
        //2.enter valid username and leave the password filed as empty then click on login
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //enter only password
        yield Login.loginButton.click(); //click on login button
        protractor_1.browser.sleep(5000);
        expect(yield Login.password_error_message.getText()).to.equal(loginData_json_1.default.password_Error_Message); //verify the error ,essage under the password field
        yield protractor_1.browser.navigate().refresh();
        //3.login by entering the passord and leave the username as empty
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //enter password
        yield Login.loginButton.click(); //click on login
        protractor_1.browser.sleep(5000);
        expect(yield Login.username_error_message.getText()).to.equal(loginData_json_1.default.Username_Error_Message); //verify the error message under username field
        yield protractor_1.browser.navigate().refresh();
        //4.login with the username wich is not exist and invalid password then verify the error message:
        yield Login.userName.sendKeys(loginData_json_1.default.notExist_userName); //enter invalid username
        yield Login.password.sendKeys(loginData_json_1.default.invalid_password); //enter invalid password
        yield Login.loginButton.click(); // click on login button
        protractor_1.browser.sleep(2000);
        expect(yield Login.error_msg_bothinvalid.getText()).to.equal(loginData_json_1.default.error_Msg_invalidData); //verify the error message.
        expect(yield Login.toast_mesg_forInvalidData.getText()).to.equal(loginData_json_1.default.toastMesg_forInvalidData); //verify the toast message
        yield protractor_1.browser.navigate().refresh();
        //5.Login with valid username and invalid password
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //valid username
        yield Login.password.sendKeys(loginData_json_1.default.notExist_userName); //invalid password
        yield Login.loginButton.click();
        protractor_1.browser.sleep(2000);
        expect(yield Login.error_msg_bothinvalid.getText()).to.equal(loginData_json_1.default.error_Msg_invalidData); //verify the error message.
        expect(yield Login.toast_mesg_forInvalidData.getText()).to.equal(loginData_json_1.default.toastMesg_forInvalidData); //verify the toast message
        yield protractor_1.browser.navigate().refresh();
        //6.Login with invalid username and valid password
        yield Login.userName.sendKeys(loginData_json_1.default.notExist_userName); //invalid username
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //valid password
        yield Login.loginButton.click();
        protractor_1.browser.sleep(2000);
        expect(yield Login.error_msg_bothinvalid.getText()).to.equal(loginData_json_1.default.error_Msg_invalidData); //verify the error message.
        expect(yield Login.toast_mesg_forInvalidData.getText()).to.equal(loginData_json_1.default.toastMesg_forInvalidData); //verify the toast message
        yield protractor_1.browser.navigate().refresh();
        //7.Enter invalid username and navigate to the other field then verify the error message under username filedcd 
        yield Login.userName.sendKeys(loginData_json_1.default.invalid_userName); //Enter invalid username
        yield Login.password.click(); //click on the password
        protractor_1.browser.sleep(2000);
        expect(yield Login.invalid_username_errorMsg.getText()).to.equal(loginData_json_1.default.invalid_UserName_Error); //verify the error message.
    });
});
//login with valid credentials:
cucumber_1.Then('enter valid credentials and click on login then verify the homepage', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //enter valid username
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //Enter valid password
        yield Login.loginButton.click(); //click on login button
        //page should navigate to the home page, verify the admin home page
        protractor_1.browser.sleep(5000);
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.homepage_title); //verify the home page title
    });
});
// verify the functionality of the password visibilty icon
cucumber_1.When('enter the password and verify the password visibily icon functionality.', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.password.sendKeys(loginData_json_1.default.invalid_password); //Enter password
        expect(yield Login.password.getAttribute('type')).to.equal('password'); //verify password,it should be invisible
        yield Login.visibiltyBtn.click(); //click on icon
        expect(yield Login.password.getAttribute('type')).to.equal('text'); //verify password,it should be visible
    });
});
//verify the forgot password link
cucumber_1.When('Click on forgot password link and verify the page title.', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.forgot_password.click(); // click on forgot password
        expect(yield protractor_1.browser.getTitle()).to.equal(forgotPasswordPage_json_1.default.forgotPassword_PageTitle);
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9naW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9Mb2dpbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBb0M7QUFDcEMsbURBQWdEO0FBQ2hELG1GQUF3RDtBQUN4RCxpRkFBb0Q7QUFDcEQscUdBQW9FO0FBQ3BFLElBQUksTUFBTSxHQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFFbkMsSUFBSSxLQUFLLEdBQUMsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUV0QixnQkFBSyxDQUFDLHFDQUFxQyxFQUFFOztRQUMzQyxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ2hDLE1BQU0sb0JBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQyxNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLHdCQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsSUFBRztZQUNDLE1BQU0sS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUM1QixNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEI7UUFDSixPQUFNLEtBQUssRUFDUjtZQUNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkI7SUFFUixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsNENBQTRDO0FBQzVDLGVBQUksQ0FBQyxzQ0FBc0MsRUFBQzs7UUFDM0MsdUNBQXVDO1FBQ3ZDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLDZCQUE2QjtRQUM1QixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RFLDZEQUE2RDtRQUM1RCxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDaEUsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxjQUFjO1FBQ2xFLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsc0JBQXNCO1FBQzlFLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEscUJBQXFCO1FBQzdFLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsc0JBQXNCO1FBQ3JGLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsMEJBQTBCO1FBQ3RGLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsY0FBYztJQUMzRSxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsa0NBQWtDO0FBRWxDLGVBQUksQ0FBQyw0RUFBNEUsRUFBRTs7UUFFakYsK0ZBQStGO1FBQy9GLE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHVCQUF1QjtRQUN2RCxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsc0JBQXNCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFBLCtDQUErQztRQUMvSSxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsc0JBQXNCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFBLG1EQUFtRDtRQUNuSixNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkMsa0ZBQWtGO1FBQ2xGLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLHFCQUFxQjtRQUM3RSxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSx1QkFBdUI7UUFDdkQsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLHNCQUFzQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQSxtREFBbUQ7UUFDbkosTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRW5DLGlFQUFpRTtRQUNqRSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQSxnQkFBZ0I7UUFDeEUsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsZ0JBQWdCO1FBQ2hELG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUEsK0NBQStDO1FBQy9JLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVuQyxpR0FBaUc7UUFDaEcsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQSx3QkFBd0I7UUFDbkYsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQSx3QkFBd0I7UUFDbEYsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsd0JBQXdCO1FBQ3hELG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUEsMkJBQTJCO1FBQ3pILE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUEsMEJBQTBCO1FBQy9ILE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVwQyxrREFBa0Q7UUFDakQsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUEsZ0JBQWdCO1FBQ3hFLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUEsa0JBQWtCO1FBQzdFLE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFBLDJCQUEyQjtRQUN6SCxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMseUJBQXlCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFBLDBCQUEwQjtRQUMvSCxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7UUFFcEMsa0RBQWtEO1FBQ2xELE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUEsa0JBQWtCO1FBQzdFLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLGdCQUFnQjtRQUN4RSxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQSwyQkFBMkI7UUFFekgsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLHlCQUF5QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQSwwQkFBMEI7UUFDL0gsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRW5DLGdIQUFnSDtRQUNoSCxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFBLHdCQUF3QjtRQUNsRixNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUEsQ0FBQSx1QkFBdUI7UUFDbkQsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLHlCQUF5QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx3QkFBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQSwyQkFBMkI7SUFFaEksQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILCtCQUErQjtBQUUvQixlQUFJLENBQUMscUVBQXFFLEVBQUU7O1FBQzFFLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLHNCQUFzQjtRQUM5RSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQSxzQkFBc0I7UUFDOUUsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsdUJBQXVCO1FBQ3ZELG1FQUFtRTtRQUNuRSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUEsNEJBQTRCO0lBRWpHLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCwwREFBMEQ7QUFDMUQsZUFBSSxDQUFDLHlFQUF5RSxFQUFDOztRQUM3RSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFBLGdCQUFnQjtRQUMxRSxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUEsQ0FBQSx3Q0FBd0M7UUFDOUcsTUFBTSxLQUFLLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsZUFBZTtRQUNoRCxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUEsQ0FBQSxzQ0FBc0M7SUFDMUcsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILGlDQUFpQztBQUNqQyxlQUFJLENBQUMsMERBQTBELEVBQUU7O1FBQy9ELE1BQU0sS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLDJCQUEyQjtRQUMvRCxNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxpQ0FBYyxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDckYsQ0FBQztDQUFBLENBQUMsQ0FBQyJ9