"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const homePage_1 = require("../../pageobjects/homePage");
const editProvider_json_1 = __importDefault(require("../../TestData/editProvider.json"));
const addProvider_1 = require("../../pageobjects/addProvider");
const editProvider_1 = require("../../pageobjects/editProvider");
const homePage_json_1 = __importDefault(require("../../TestData/homePage.json"));
let expect = require('chai').expect;
let Login = new login_1.login();
let Homepage = new homePage_1.homePage();
let AddProvider = new addProvider_1.addProvider();
let EditProvider = new editProvider_1.editProvider();
//1.leave the required fields as empty and click on sumbit then verify the error messages
cucumber_1.When('I\'m on edit provider page verify the disabled fields then leave the all required fields as empty and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //enter provider and click on edit icon
        yield Homepage.search_provider.sendKeys(editProvider_json_1.default.providerName); //enter the provider name
        yield protractor_1.browser.sleep(5000);
        yield EditProvider.edit_icon.click();
        yield protractor_1.browser.sleep(2000);
        //then verify the disabled fields
        // expect(await EditProvider.Email.isEnabled()).to.equal(false);//email field should be desabled
        expect(yield EditProvider.Email.getAttribute('disabled')).to.equal('true');
        //expect(await EditProvider.pharmacy_dropdown.getAttribute('disabled')).to.equal('true');
        expect(yield EditProvider.pharmacy_dropdown.isEnabled()).to.equal(false); //pharmacy dropdown should be disabled
        //clear the data
        yield EditProvider.firstName.clear();
        yield EditProvider.lastName.clear();
        yield EditProvider.mobile_num.clear();
        yield EditProvider.address.clear();
        yield EditProvider.zip_Code.clear();
        yield EditProvider.licence.clear();
        yield EditProvider.submitBtn.click();
    });
});
// verify the error messages for rquired fields
cucumber_1.Then('verify the error messages for which we leave as empty', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield EditProvider.fname_ErrorMsg.getText()).to.equal(editProvider_json_1.default.error_Msg.fname_error);
        expect(yield EditProvider.lname_errorMsg.getText()).to.equal(editProvider_json_1.default.error_Msg.lname_error);
        expect(yield EditProvider.mobileNum_errorMsg.getText()).to.equal(editProvider_json_1.default.error_Msg.mobileNum_error);
        expect(yield EditProvider.address_errorMsg.getText()).to.equal(editProvider_json_1.default.error_Msg.address_error);
        expect(yield EditProvider.zipcode_error.getText()).to.equal(editProvider_json_1.default.error_Msg.zipcode_error);
        expect(yield EditProvider.licence_error.getText()).to.equal(editProvider_json_1.default.error_Msg.licence_error);
        yield protractor_1.browser.navigate().refresh();
    });
});
cucumber_1.When('I\'m on edit provider page enter the invalid data in all fields and click on submit button', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.sleep(5000);
        yield EditProvider.firstName.clear();
        yield EditProvider.firstName.sendKeys(editProvider_json_1.default.invalid.first_name); //first name
        yield EditProvider.lastName.clear();
        yield EditProvider.lastName.sendKeys(editProvider_json_1.default.invalid.last_name); //last name
        yield EditProvider.nickName.clear();
        yield EditProvider.nickName.sendKeys(editProvider_json_1.default.invalid.nick_name); //nick name
        yield EditProvider.mobile_num.clear();
        yield EditProvider.mobile_num.sendKeys(editProvider_json_1.default.invalid.mobile_number); //mobile number
        yield EditProvider.address.clear();
        yield EditProvider.address.sendKeys(editProvider_json_1.default.invalid.address); //address
        yield EditProvider.city.clear();
        yield EditProvider.city.sendKeys(editProvider_json_1.default.invalid.city); //city
        yield EditProvider.state.clear();
        yield EditProvider.state.sendKeys(editProvider_json_1.default.invalid.state); //state
        yield EditProvider.country.clear();
        yield EditProvider.country.sendKeys(editProvider_json_1.default.invalid.country); //country
        yield EditProvider.zip_Code.clear();
        yield EditProvider.zip_Code.sendKeys(editProvider_json_1.default.invalid.zipCode); //zip code
        yield EditProvider.specilization.clear();
        yield EditProvider.specilization.sendKeys(editProvider_json_1.default.invalid.specilization); //spectilization
        yield EditProvider.licence.clear();
        yield EditProvider.licence.sendKeys(editProvider_json_1.default.invalid.licence); //licence
        //And click on submit button
        yield AddProvider.submitBtn.click();
    });
});
cucumber_1.Then('verify the error messages on edit page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield EditProvider.invalidfname_ErrorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.fname_error);
        expect(yield EditProvider.invalidlname_errorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.lname_error);
        expect(yield EditProvider.invalidnickName_error.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.nickName_errorMsg);
        expect(yield EditProvider.invalidmobileNum_errorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.mobileNum_error);
        expect(yield EditProvider.invalidaddress_errorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.address_error);
        expect(yield EditProvider.invalidcity_error.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.city_errorMsg);
        expect(yield EditProvider.invalidState_errorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.StateError_msg);
        expect(yield EditProvider.invalidCountry_errorMsg.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.countryErrorMsg);
        expect(yield EditProvider.invalidzipcode_error.getText()).to.equal(editProvider_json_1.default.invalid_ErrorMsg.zipcode_error);
        yield protractor_1.browser.navigate().refresh();
    });
});
//3.edit details with valid data in all fields
cucumber_1.When('I\'m on edit provider page Enter valid data in all fields and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //edit the fileds with valid data in all fields
        yield EditProvider.firstName.clear();
        yield EditProvider.firstName.sendKeys(editProvider_json_1.default.valid.first_name); //first name
        yield EditProvider.lastName.clear();
        yield EditProvider.lastName.sendKeys(editProvider_json_1.default.valid.last_name); //last name
        yield EditProvider.nickName.clear();
        yield EditProvider.nickName.sendKeys(editProvider_json_1.default.valid.nick_name); //nick name
        //select date of birth
        yield EditProvider.DOB.click();
        //  await EditProvider.month_yearBtn.click();
        //  await EditProvider.Previos20yearsBtn.click();
        //  await EditProvider.select_Year.click();
        //  await EditProvider.select_Month.click();
        yield EditProvider.select_Date.click();
        yield EditProvider.gender_Female.click();
        yield EditProvider.ethnicity_dropdown.click();
        yield EditProvider.select_ethnicity.click(); //select ethncity
        yield EditProvider.mobile_num.clear();
        yield EditProvider.mobile_num.sendKeys(editProvider_json_1.default.valid.mobile_number); //mobile number
        yield EditProvider.address.clear();
        yield EditProvider.address.sendKeys(editProvider_json_1.default.valid.address); //address
        yield EditProvider.city.clear();
        yield EditProvider.city.sendKeys(editProvider_json_1.default.valid.city); //city
        yield EditProvider.state.clear();
        yield EditProvider.state.sendKeys(editProvider_json_1.default.valid.state); //state
        yield EditProvider.country.clear();
        yield EditProvider.country.sendKeys(editProvider_json_1.default.valid.country); //country
        yield EditProvider.zip_Code.clear();
        yield EditProvider.zip_Code.sendKeys(editProvider_json_1.default.valid.zipCode); //zip code
        //select smart phone
        //  await EditProvider.phoneType_dropdown.click();
        //  await EditProvider.select_android.click();
        //enter specilization
        yield EditProvider.specilization.clear();
        yield EditProvider.specilization.sendKeys(editProvider_json_1.default.valid.specilization);
        //enter licence
        yield EditProvider.licence.clear();
        yield EditProvider.licence.sendKeys(editProvider_json_1.default.valid.licence);
        //upload image
        yield EditProvider.upload_ImgBtn.sendKeys(editProvider_json_1.default.img_path);
        yield protractor_1.browser.sleep(10000);
        //click on submit button
        yield EditProvider.submitBtn.click();
        yield protractor_1.browser.sleep(2000);
    });
});
//then verify the error messages
cucumber_1.Then('verify the success message after sumbit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //verify whether page returned to providerlist page and verify the success message
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.homepage_title);
        //verify succcess message
        expect(yield EditProvider.success_msg.isDisplayed()).to.equal(true);
        expect(yield EditProvider.success_msg.getText()).to.equal(editProvider_json_1.default.succsess_msg);
        yield protractor_1.browser.sleep(5000);
    });
});
//4.edit the details with valid data in required fields
cucumber_1.When('I\'m on edit provider page Enter valid data in required fields and click on submit then verify the success message', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.search_provider.sendKeys(editProvider_json_1.default.providerName); //enter the provider name
        yield EditProvider.edit_icon.click();
        yield protractor_1.browser.sleep(2000);
        yield EditProvider.firstName.clear();
        yield EditProvider.firstName.sendKeys(editProvider_json_1.default.valid_info2.first_name); //first name
        yield EditProvider.lastName.clear();
        yield EditProvider.lastName.sendKeys(editProvider_json_1.default.valid_info2.last_name); //last name
        yield EditProvider.mobile_num.clear();
        yield EditProvider.mobile_num.sendKeys(editProvider_json_1.default.valid_info2.mobile_number); //mobile number
        yield EditProvider.address.clear();
        yield EditProvider.address.sendKeys(editProvider_json_1.default.valid_info2.address); //address
        yield EditProvider.zip_Code.clear();
        yield EditProvider.zip_Code.sendKeys(editProvider_json_1.default.valid_info2.zipCode); //zip code
        //enter licence
        yield EditProvider.licence.clear();
        yield EditProvider.licence.sendKeys(editProvider_json_1.default.valid_info2.licence);
        //click on submit button
        yield EditProvider.submitBtn.click();
        yield protractor_1.browser.sleep(2000);
        //verify whether page returned to providerlist page and verify the success message
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.homepage_title);
        //verify succcess message
        expect(yield EditProvider.success_msg.isDisplayed()).to.equal(true);
        expect(yield EditProvider.success_msg.getText()).to.equal(editProvider_json_1.default.succsess_msg);
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRWRpdFByb3ZpZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3RlcHNEZWZpbml0aW9ucy9BZG1pbl9Nb2R1bGUvRWRpdFByb3ZpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFvQztBQUNwQyxtREFBZ0Q7QUFDaEQseURBQW9EO0FBQ3BELHlGQUFnRTtBQUNoRSwrREFBMEQ7QUFDMUQsaUVBQTREO0FBQzVELGlGQUFvRDtBQUNwRCxJQUFJLE1BQU0sR0FBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQ25DLElBQUksS0FBSyxHQUFDLElBQUksYUFBSyxFQUFFLENBQUM7QUFDdEIsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxXQUFXLEdBQUMsSUFBSSx5QkFBVyxFQUFFLENBQUM7QUFDbEMsSUFBSSxZQUFZLEdBQUMsSUFBSSwyQkFBWSxFQUFFLENBQUM7QUFDcEMseUZBQXlGO0FBQ3pGLGVBQUksQ0FBQyx1SEFBdUgsRUFBRTs7UUFFNUgsdUNBQXVDO1FBQ3ZDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQSx5QkFBeUI7UUFDaEcsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixpQ0FBaUM7UUFDbkMsZ0dBQWdHO1FBQ2hHLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUUzRSx5RkFBeUY7UUFDekYsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBLHNDQUFzQztRQUM3RyxnQkFBZ0I7UUFDaEIsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JDLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdEMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCwrQ0FBK0M7QUFDL0MsZUFBSSxDQUFDLHVEQUF1RCxFQUFFOztRQUMxRCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBZ0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckcsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JHLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdHLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3pHLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDJCQUFnQixDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN0RyxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsYUFBYSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBZ0IsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEcsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBRXJDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxlQUFJLENBQUMsNEZBQTRGLEVBQUM7O1FBQ2hHLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JDLE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUEsWUFBWTtRQUN2RixNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEMsTUFBTSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxXQUFXO1FBQ3BGLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBLFdBQVc7UUFDcEYsTUFBTSxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLE1BQU0sWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsZUFBZTtRQUM5RixNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQy9FLE1BQU0sWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLE1BQU07UUFDdEUsTUFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUEsT0FBTztRQUN6RSxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQy9FLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFVBQVU7UUFDakYsTUFBTSxZQUFZLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pDLE1BQU0sWUFBWSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsZ0JBQWdCO1FBQ2xHLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQyxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDL0UsNEJBQTRCO1FBQzVCLE1BQU0sV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUVwQyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsZUFBSSxDQUFDLHdDQUF3QyxFQUFFOztRQUM3QyxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDJCQUFnQixDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ25ILE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkgsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3pILE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDM0gsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN2SCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDJCQUFnQixDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pILE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDdEgsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywyQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6SCxNQUFNLENBQUMsTUFBTSxZQUFZLENBQUMsb0JBQW9CLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDJCQUFnQixDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BILE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0wsOENBQThDO0FBQzlDLGVBQUksQ0FBQywrRUFBK0UsRUFBRTs7UUFDbkYsK0NBQStDO1FBQy9DLE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQyxNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFBLFlBQVk7UUFDckYsTUFBTSxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BDLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUEsV0FBVztRQUNsRixNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEMsTUFBTSxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxXQUFXO1FBQ2xGLHNCQUFzQjtRQUN0QixNQUFNLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsNkNBQTZDO1FBQzdDLGlEQUFpRDtRQUNqRCwyQ0FBMkM7UUFDM0MsNENBQTRDO1FBQzNDLE1BQU0sWUFBWSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFekMsTUFBTSxZQUFZLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUMsTUFBTSxZQUFZLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxpQkFBaUI7UUFDOUQsTUFBTSxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLE1BQU0sWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsZUFBZTtRQUM1RixNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQzdFLE1BQU0sWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxNQUFNLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLE1BQU07UUFDcEUsTUFBTSxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLE1BQU0sWUFBWSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUEsT0FBTztRQUN2RSxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQzdFLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFVBQVU7UUFDL0Usb0JBQW9CO1FBQ3JCLGtEQUFrRDtRQUNsRCw4Q0FBOEM7UUFDN0MscUJBQXFCO1FBQ3JCLE1BQU0sWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN6QyxNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoRixlQUFlO1FBQ2YsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BFLGNBQWM7UUFDZCxNQUFNLFlBQVksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3JFLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0Isd0JBQXdCO1FBQ3hCLE1BQU0sWUFBWSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTdCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxnQ0FBZ0M7QUFDaEMsZUFBSSxDQUFDLHlDQUF5QyxFQUFFOztRQUM5QyxrRkFBa0Y7UUFDbEYsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVEsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNuRSx5QkFBeUI7UUFDekIsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEUsTUFBTSxDQUFDLE1BQU0sWUFBWSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMkJBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekYsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUU1QixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsdURBQXVEO0FBQ3ZELGVBQUksQ0FBQyxvSEFBb0gsRUFBQzs7UUFDeEgsTUFBTSxRQUFRLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFBLHlCQUF5QjtRQUNoRyxNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckMsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQSxZQUFZO1FBQzNGLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBLFdBQVc7UUFDeEYsTUFBTSxZQUFZLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLE1BQU0sWUFBWSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsMkJBQWdCLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUEsZUFBZTtRQUNsRyxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDbkMsTUFBTSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywyQkFBZ0IsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQ25GLE1BQU0sWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFlBQVksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFVBQVU7UUFDckYsZUFBZTtRQUNmLE1BQU0sWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNuQyxNQUFNLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMxRSx3QkFBd0I7UUFDeEIsTUFBTSxZQUFZLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0Isa0ZBQWtGO1FBQ2xGLE1BQU0sQ0FBQyxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFRLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDbkUseUJBQXlCO1FBQ3pCLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BFLE1BQU0sQ0FBQyxNQUFNLFlBQVksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDJCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBRTFGLENBQUM7Q0FBQSxDQUFDLENBQUMifQ==