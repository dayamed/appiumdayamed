"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const loginData_json_1 = __importDefault(require("../../TestData/loginData.json"));
const homePage_1 = require("../../pageobjects/homePage");
const addProvider_json_1 = __importDefault(require("../../TestData/addProvider.json"));
const addProvider_1 = require("../../pageobjects/addProvider");
let expect = require('chai').expect;
let Login = new login_1.login();
let Homepage = new homePage_1.homePage();
let AddProvider = new addProvider_1.addProvider();
//enter the url in browser and login with admin
cucumber_1.Given('navigate to the QA Dayamed web portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.waitForAngularEnabled(false);
        yield protractor_1.browser.manage().window().maximize();
        yield protractor_1.browser.get(loginData_json_1.default.URL);
        try {
            yield Login.Adv_Btn.click();
            yield Login.Proceed_Btn.click();
            protractor_1.browser.sleep(5000);
        }
        catch (error) {
            console.log(error);
        }
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //enter valid username
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //Enter valid password
        yield Login.loginButton.click(); //click on login button
        yield protractor_1.browser.sleep(5000);
    });
});
//1.navigate to the ADD provider page click on submit without any data in required fields then verify the error messages
cucumber_1.When('leave the all required fields as empty and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.addProvider_fromProviderPage.click();
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(addProvider_json_1.default.page_Title);
        //enter valid data in not required fields
        //select date of birth
        yield AddProvider.nickName.sendKeys(addProvider_json_1.default.valid.nick_name);
        yield AddProvider.DOB.click();
        yield AddProvider.month_yearBtn.click();
        yield AddProvider.Previos20yearsBtn.click();
        yield AddProvider.select_Year.click();
        yield AddProvider.select_Month.click();
        yield AddProvider.select_Date.click();
        //select ethnicity
        yield AddProvider.ethnicity_dropdown.click();
        yield AddProvider.select_ethnicity.click();
        //enter city name
        yield AddProvider.city.sendKeys(addProvider_json_1.default.valid.city);
        yield AddProvider.state.sendKeys(addProvider_json_1.default.valid.state);
        yield AddProvider.country.sendKeys(addProvider_json_1.default.valid.country);
        yield protractor_1.browser.sleep(5000);
        //select smart phone type
        yield AddProvider.phoneType_dropdown.click();
        yield AddProvider.select_android.click();
        //enter specilization
        yield AddProvider.specilization.sendKeys(addProvider_json_1.default.valid.specilization);
        //Click on submit button
        yield AddProvider.submitBtn.click();
        yield protractor_1.browser.sleep(2000);
    });
});
//verify the error messages:
cucumber_1.Then('verify the error messages', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield AddProvider.fname_ErrorMsg.getText()).to.equal(addProvider_json_1.default.error_Msg.fname_error);
        expect(yield AddProvider.lname_errorMsg.getText()).to.equal(addProvider_json_1.default.error_Msg.lname_error);
        expect(yield AddProvider.mobileNum_errorMsg.getText()).to.equal(addProvider_json_1.default.error_Msg.mobileNum_error);
        expect(yield AddProvider.email_errorMsg.getText()).to.equal(addProvider_json_1.default.error_Msg.email_error);
        expect(yield AddProvider.address_errorMsg.getText()).to.equal(addProvider_json_1.default.error_Msg.address_error);
        expect(yield AddProvider.zipcode_error.getText()).to.equal(addProvider_json_1.default.error_Msg.zipcode_error);
        expect(yield AddProvider.licence_error.getText()).to.equal(addProvider_json_1.default.error_Msg.licence_error);
        expect(yield AddProvider.pharmacy_error.getText()).to.equal(addProvider_json_1.default.error_Msg.pharmacy_error);
    });
});
//2.enter the invalid data in all fields and click on submit then verify the all error messages for the all fields
cucumber_1.When('enter the invalid data in all fields and click on submit button', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.addProvider_fromProviderPage.click();
        //enter the invalid data in all text fields
        yield AddProvider.firstName.sendKeys(addProvider_json_1.default.invalid.first_name); //first name
        yield AddProvider.lastName.sendKeys(addProvider_json_1.default.invalid.last_name); //last name
        yield AddProvider.nickName.sendKeys(addProvider_json_1.default.invalid.nick_name); //nick name
        yield AddProvider.mobile_num.sendKeys(addProvider_json_1.default.invalid.mobile_number); //mobile number
        yield AddProvider.Email.sendKeys(addProvider_json_1.default.invalid.email); //email address
        yield AddProvider.address.sendKeys(addProvider_json_1.default.invalid.address); //address
        yield AddProvider.city.sendKeys(addProvider_json_1.default.invalid.city); //city
        yield AddProvider.state.sendKeys(addProvider_json_1.default.invalid.state); //state
        yield AddProvider.country.sendKeys(addProvider_json_1.default.invalid.country); //country
        yield AddProvider.zip_Code.sendKeys(addProvider_json_1.default.invalid.zipCode); //zip code
        yield AddProvider.specilization.sendKeys(addProvider_json_1.default.invalid.specilization); //spectilization
        yield AddProvider.licence.sendKeys(addProvider_json_1.default.invalid.licence); //licence
        //And click on submit button
        yield AddProvider.submitBtn.click();
    });
});
//then verify the all error messages
cucumber_1.Then('verify the error messages for all fields', function () {
    return __awaiter(this, void 0, void 0, function* () {
        expect(yield AddProvider.invalidfname_ErrorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.fname_error);
        expect(yield AddProvider.invalidlname_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.lname_error);
        expect(yield AddProvider.invalidnickName_error.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.nickName_errorMsg);
        expect(yield AddProvider.invalidmobileNum_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.mobileNum_error);
        expect(yield AddProvider.invalidemail_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.email_error);
        expect(yield AddProvider.invalidaddress_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.address_error);
        expect(yield AddProvider.invalidcity_error.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.city_errorMsg);
        expect(yield AddProvider.invalidState_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.StateError_msg);
        expect(yield AddProvider.invalidCountry_errorMsg.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.countryErrorMsg);
        expect(yield AddProvider.invalidzipcode_error.getText()).to.equal(addProvider_json_1.default.invalid_ErrorMsg.zipcode_error);
        protractor_1.browser.sleep(5000);
    });
});
//3.Enter the valid data in all fields and click on submit button then verify the success message and verify whether provider added
cucumber_1.When('Enter valid data in all fields and click on submit', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.addProvider_fromProviderPage.click();
        //Enter valid data in all fields
        yield AddProvider.firstName.sendKeys(addProvider_json_1.default.valid.first_name1); //first name
        yield AddProvider.lastName.sendKeys(addProvider_json_1.default.valid.last_name); //last name
        yield AddProvider.nickName.sendKeys(addProvider_json_1.default.valid.nick_name); //nick name
        //select date of birth
        yield AddProvider.DOB.click();
        yield AddProvider.month_yearBtn.click();
        yield AddProvider.Previos20yearsBtn.click();
        yield AddProvider.select_Year.click();
        yield AddProvider.select_Month.click();
        yield AddProvider.select_Date.click();
        yield AddProvider.gender_Female.click();
        //select ethncity
        yield AddProvider.ethnicity_dropdown.click();
        yield AddProvider.select_ethnicity.click();
        yield AddProvider.mobile_num.sendKeys(addProvider_json_1.default.valid.mobile_number); //mobile number
        yield AddProvider.Email.sendKeys(addProvider_json_1.default.valid.email1); //email
        yield AddProvider.address.sendKeys(addProvider_json_1.default.valid.address); //address
        yield AddProvider.city.sendKeys(addProvider_json_1.default.valid.city); //city
        yield AddProvider.state.sendKeys(addProvider_json_1.default.valid.state); //state
        yield AddProvider.country.sendKeys(addProvider_json_1.default.valid.country); //country
        yield AddProvider.zip_Code.sendKeys(addProvider_json_1.default.valid.zipCode); //zip code
        //select smart phone
        yield AddProvider.phoneType_dropdown.click();
        yield AddProvider.select_android.click();
        //enter specilization
        yield AddProvider.specilization.sendKeys(addProvider_json_1.default.valid.specilization);
        //enter licence
        yield AddProvider.licence.sendKeys(addProvider_json_1.default.valid.licence);
        //select pharmacy
        yield AddProvider.pharmacy_dropdown.click();
        yield AddProvider.select_pharmacy.click();
        //upload image
        yield AddProvider.upload_ImgBtn.sendKeys(addProvider_json_1.default.img_path);
        yield protractor_1.browser.sleep(10000);
        //click on submit button
        // await AddProvider.submitBtn.click();
        yield protractor_1.browser.sleep(15000);
    });
});
// Then('verify the success message and verify', async function () {
//  //verify whether page returned to providerlist page and verify the success message
//  expect(await browser.getTitle()).to.equal(homepage.homepage_title);
//  await browser.sleep(3000);
//  //verify succcess message
//  expect(await AddProvider.success_msg.isDisplayed()).to.equal(true);
//  expect(await AddProvider.success_msg.getText()).to.equal(addProviderData.succsess_msg);
// });
//4.Add provider with valid data in required fields only.
cucumber_1.When('Enter valid data in required fields and click on submit then verify the success message', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.addProvider_fromProviderPage.click();
        //Enter valid data in all fields
        yield AddProvider.firstName.sendKeys(addProvider_json_1.default.valid.first_name2); //first name
        yield AddProvider.lastName.sendKeys(addProvider_json_1.default.valid.last_name); //last name
        yield AddProvider.mobile_num.sendKeys(addProvider_json_1.default.valid.mobile_number); //mobile number
        yield AddProvider.Email.sendKeys(addProvider_json_1.default.valid.email2); //email
        yield AddProvider.address.sendKeys(addProvider_json_1.default.valid.address); //address
        yield AddProvider.zip_Code.sendKeys(addProvider_json_1.default.valid.zipCode); //zip code
        //enter licence
        yield AddProvider.licence.sendKeys(addProvider_json_1.default.valid.licence);
        //select pharmacy
        yield AddProvider.pharmacy_dropdown.click();
        yield AddProvider.select_pharmacy.click();
        //click on submit button
        yield AddProvider.submitBtn.click();
        //   await browser.sleep(15000);
        //  //verify whether page returned to providerlist page and verify the success message
        //  expect(await browser.getTitle()).to.equal(homepage.homepage_title);
        //  //verify succcess message
        //  expect(await AddProvider.success_msg.isDisplayed()).to.equal(true);
        //  expect(await AddProvider.success_msg.getText()).to.equal(addProviderData.succsess_msg);
        yield protractor_1.browser.navigate().refresh();
    });
});
//5.enter the added provider name and verify whether displaye then click on view patient enter patient name then verify
cucumber_1.When('Enter the provider name which added and click on view patients', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.sleep(5000);
        yield Homepage.search_provider.sendKeys(addProvider_json_1.default.valid.first_name1); //enter the provider name
        yield protractor_1.browser.sleep(3000);
        expect(yield AddProvider.provider_card.isDisplayed()).to.equal(true); //verify the card on the page whether displayed 
    });
});
cucumber_1.Then('Enter the patient name and verify whether patient displayed', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield AddProvider.viewPatients.click(); //click on view patients
        yield protractor_1.browser.sleep(5000);
        yield AddProvider.searchPatient.sendKeys(addProvider_json_1.default.patientName); //Enter patient name in search patient
        yield protractor_1.browser.sleep(3000);
        //then verify whwther displayed on the page
        expect(yield AddProvider.petientCard.isDisplayed()).to.equal(true);
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQWRkUHJvdmlkZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9BZGRQcm92aWRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBb0M7QUFDcEMsbURBQWdEO0FBQ2hELG1GQUF3RDtBQUN4RCx5REFBb0Q7QUFDcEQsdUZBQThEO0FBQzlELCtEQUEwRDtBQUUxRCxJQUFJLE1BQU0sR0FBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO0FBQ25DLElBQUksS0FBSyxHQUFDLElBQUksYUFBSyxFQUFFLENBQUM7QUFDdEIsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxXQUFXLEdBQUMsSUFBSSx5QkFBVyxFQUFFLENBQUM7QUFDbEMsK0NBQStDO0FBQy9DLGdCQUFLLENBQUMsdUNBQXVDLEVBQUU7O1FBQzNDLG9CQUFPLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDcEMsTUFBTSxvQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzNDLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQyxJQUFHO1lBQ0MsTUFBTSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzVCLE1BQU0sS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNoQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwQjtRQUNKLE9BQU0sS0FBSyxFQUNSO1lBQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNuQjtRQUNELE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLHNCQUFzQjtRQUM5RSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQSxzQkFBc0I7UUFDOUUsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsdUJBQXVCO1FBQ3ZELE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0IsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILHdIQUF3SDtBQUN4SCxlQUFJLENBQUMsNERBQTRELEVBQUM7O1FBR2hFLE1BQU0sUUFBUSxDQUFDLDRCQUE0QixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3BELHVCQUF1QjtRQUN2QixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RFLHlDQUF5QztRQUV6QyxzQkFBc0I7UUFDdEIsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyRSxNQUFNLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUIsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hDLE1BQU0sV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVDLE1BQU0sV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN0QyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkMsTUFBTSxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBRXRDLGtCQUFrQjtRQUNsQixNQUFNLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QyxNQUFNLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMzQyxpQkFBaUI7UUFDakIsTUFBTSxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1RCxNQUFNLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlELE1BQU0sV0FBVyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEUsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQix5QkFBeUI7UUFDekIsTUFBTSxXQUFXLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsTUFBTSxXQUFXLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3pDLHFCQUFxQjtRQUNyQixNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlFLHdCQUF3QjtRQUN4QixNQUFNLFdBQVcsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEMsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUU1QixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsNEJBQTRCO0FBQzVCLGVBQUksQ0FBQywyQkFBMkIsRUFBRTs7UUFDaEMsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkcsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkcsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUMzRyxNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNuRyxNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3ZHLE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3BHLE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3hHLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxrSEFBa0g7QUFDaEgsZUFBSSxDQUFDLGlFQUFpRSxFQUFFOztRQUN0RSxNQUFNLFFBQVEsQ0FBQyw0QkFBNEIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsRCwyQ0FBMkM7UUFDM0MsTUFBTSxXQUFXLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFBLFlBQVk7UUFDckYsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBLFdBQVc7UUFDbEYsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFBLFdBQVc7UUFDbEYsTUFBTSxXQUFXLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBLGVBQWU7UUFDNUYsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBLGVBQWU7UUFDL0UsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDN0UsTUFBTSxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLE1BQU07UUFDcEUsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBLE9BQU87UUFDdkUsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDN0UsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFVBQVU7UUFDL0UsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBLGdCQUFnQjtRQUNoRyxNQUFNLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUEsU0FBUztRQUM3RSw0QkFBNEI7UUFDNUIsTUFBTSxXQUFXLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBRXhDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxvQ0FBb0M7QUFDcEMsZUFBSSxDQUFDLDBDQUEwQyxFQUFFOztRQUMvQyxNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakgsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pILE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3ZILE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUN6SCxNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakgsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3JILE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvRyxNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLDBCQUFlLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEgsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQywwQkFBZSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3ZILE1BQU0sQ0FBQyxNQUFNLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsMEJBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNsSCxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV0QixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsbUlBQW1JO0FBQ25JLGVBQUksQ0FBQyxvREFBb0QsRUFBRTs7UUFDekQsTUFBTSxRQUFRLENBQUMsNEJBQTRCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEQsZ0NBQWdDO1FBQ2hDLE1BQU0sV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQSxZQUFZO1FBQ3BGLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxXQUFXO1FBQ2hGLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxXQUFXO1FBQ2hGLHNCQUFzQjtRQUN0QixNQUFNLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDOUIsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hDLE1BQU0sV0FBVyxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzVDLE1BQU0sV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN0QyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDdkMsTUFBTSxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLE1BQU0sV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QyxpQkFBaUI7UUFDakIsTUFBTSxXQUFXLENBQUMsa0JBQWtCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDN0MsTUFBTSxXQUFXLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0MsTUFBTSxXQUFXLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBLGVBQWU7UUFDMUYsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFBLE9BQU87UUFDdEUsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDM0UsTUFBTSxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLE1BQU07UUFDbEUsTUFBTSxXQUFXLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBLE9BQU87UUFDckUsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFNBQVM7UUFDM0UsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLFVBQVU7UUFDN0Usb0JBQW9CO1FBQ3BCLE1BQU0sV0FBVyxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzdDLE1BQU0sV0FBVyxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN6QyxxQkFBcUI7UUFDckIsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5RSxlQUFlO1FBQ2YsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQywwQkFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRSxpQkFBaUI7UUFDakIsTUFBTSxXQUFXLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDNUMsTUFBTSxXQUFXLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFDLGNBQWM7UUFDZCxNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDbkUsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQix3QkFBd0I7UUFDeEIsdUNBQXVDO1FBQ3ZDLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFN0IsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILG9FQUFvRTtBQUNwRSxzRkFBc0Y7QUFDdEYsdUVBQXVFO0FBQ3ZFLDhCQUE4QjtBQUM5Qiw2QkFBNkI7QUFDN0IsdUVBQXVFO0FBQ3ZFLDJGQUEyRjtBQUUzRixNQUFNO0FBQ04seURBQXlEO0FBQ3pELGVBQUksQ0FBQyx5RkFBeUYsRUFBQzs7UUFDN0YsTUFBTSxRQUFRLENBQUMsNEJBQTRCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEQsZ0NBQWdDO1FBQ2hDLE1BQU0sV0FBVyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQSxZQUFZO1FBQ3BGLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxXQUFXO1FBQ2hGLE1BQU0sV0FBVyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQSxlQUFlO1FBQzFGLE1BQU0sV0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQSxPQUFPO1FBQ3RFLE1BQU0sV0FBVyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxTQUFTO1FBQzNFLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQSxVQUFVO1FBQzdFLGVBQWU7UUFDZCxNQUFNLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25FLGlCQUFpQjtRQUNqQixNQUFNLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM1QyxNQUFNLFdBQVcsQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUMsd0JBQXdCO1FBQ3hCLE1BQU0sV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN0QyxnQ0FBZ0M7UUFDaEMsc0ZBQXNGO1FBQ3RGLHVFQUF1RTtRQUN2RSw2QkFBNkI7UUFDN0IsdUVBQXVFO1FBQ3ZFLDJGQUEyRjtRQUUxRixNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxFQUFFLENBQUM7SUFFcEMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILHVIQUF1SDtBQUN2SCxlQUFJLENBQUMsZ0VBQWdFLEVBQUU7O1FBQ3JFLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsMEJBQWUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQSx5QkFBeUI7UUFDcEcsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLENBQUMsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLGdEQUFnRDtJQUd2SCxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsZUFBSSxDQUFDLDZEQUE2RCxFQUFFOztRQUVsRSxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSx3QkFBd0I7UUFDL0QsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLDBCQUFlLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQSxzQ0FBc0M7UUFDNUcsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQiwyQ0FBMkM7UUFDM0MsTUFBTSxDQUFDLE1BQU0sV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckUsQ0FBQztDQUFBLENBQUMsQ0FBQyJ9