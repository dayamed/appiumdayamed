"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const editProvider_1 = require("../../pageobjects/editProvider");
const homePage_1 = require("../../pageobjects/homePage");
const patients_1 = require("../../pageobjects/patients");
const patients_json_1 = __importDefault(require("../../TestData/patients.json"));
const patientReports_1 = require("../../pageobjects/patientReports");
const patientReports_json_1 = __importDefault(require("../../TestData/patientReports.json"));
let Homepage = new homePage_1.homePage();
let EditProvider = new editProvider_1.editProvider();
let Patients = new patients_1.patients();
let Reports = new patientReports_1.reports();
let notes = new patientReports_1.notesTreatement();
let Location = new patientReports_1.location();
let AdherenceReport = new patientReports_1.adherenceReport();
let expect = require('chai').expect;
const editProvider_json_1 = __importDefault(require("../../TestData/editProvider.json"));
cucumber_1.When('Enter provider and Click on view patient', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Homepage.search_provider.sendKeys(editProvider_json_1.default.providerName); //enter the provider name
        yield EditProvider.viewPatients.click(); //click on view patients
        yield protractor_1.browser.sleep(5000);
    });
});
cucumber_1.When('select patient and verify the reports page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Patients.patientSearchBox.sendKeys(patients_json_1.default.patient_name); //enter name
        yield protractor_1.browser.sleep(20000);
        yield Patients.reportsBtn.click();
        yield protractor_1.browser.sleep(20000);
        //verify the reports page title
        expect(yield protractor_1.browser.getTitle()).to.equal(patientReports_json_1.default.pageTitle);
    });
});
cucumber_1.Then('click on note and treatement then verify the popup', function () {
    return __awaiter(this, void 0, void 0, function* () {
        let name = yield Reports.profileName.getText();
        // let adherence=await Reports.adherence_OnProfileCard.getText();
        yield Reports.notesTreatement_Card.click();
        yield protractor_1.browser.sleep(5000);
        //verify the popup
        expect(yield notes.popup.isDisplayed()).to.equal(true);
        //verify the patient name on popup
        expect(yield notes.patientName.getText()).to.equal(name);
        //verify the adherence
        // expect(await notes.overallAdherence.getText()).to.equal(adherence);
        yield notes.close_icon.click();
    });
});
cucumber_1.When('I\'m on reward page click on location and verify the popup', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Reports.location_card.click();
        yield protractor_1.browser.sleep(5000);
        //verify the popup
        expect(yield Location.popup.isDisplayed()).to.equal(true);
        //verify the map view
        expect(yield Location.mapView.isDisplayed()).to.equal(true);
        //click on table view and verify
        yield Location.tableView_Btn.click();
        yield protractor_1.browser.sleep(2000);
        expect(yield Location.tableView.isDisplayed()).to.equal(true);
        yield Location.close_icon.click();
    });
});
//   When('I\'m on reports click on adherence reports card', function () {
//     // Write code here that turns the phrase above into concrete actions
//     return 'pending';
//   });
//   Then('verify the name and adherence and verify the adherence for current, prevous and future months', function () {
//     // Write code here that turns the phrase above into concrete actions
//     return 'pending';
//   });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGF0aWVudFJlcG9ydHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9QYXRpZW50UmVwb3J0cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUFzQztBQUN0QywyQ0FBcUM7QUFDckMsaUVBQThEO0FBQzlELHlEQUFzRDtBQUN0RCx5REFBc0Q7QUFDdEQsaUZBQXVEO0FBQ3ZELHFFQUFvRztBQUNwRyw2RkFBNkQ7QUFFN0QsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxZQUFZLEdBQUMsSUFBSSwyQkFBWSxFQUFFLENBQUM7QUFDcEMsSUFBSSxRQUFRLEdBQUMsSUFBSSxtQkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxPQUFPLEdBQUMsSUFBSSx3QkFBTyxFQUFFLENBQUM7QUFDMUIsSUFBSSxLQUFLLEdBQUMsSUFBSSxnQ0FBZSxFQUFFLENBQUE7QUFDL0IsSUFBSSxRQUFRLEdBQUMsSUFBSSx5QkFBUSxFQUFFLENBQUM7QUFDNUIsSUFBSSxlQUFlLEdBQUMsSUFBSSxnQ0FBZSxFQUFFLENBQUM7QUFDMUMsSUFBSSxNQUFNLEdBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUVuQyx5RkFBZ0U7QUFDaEUsZUFBSSxDQUFDLDBDQUEwQyxFQUFFOztRQUM3QyxNQUFNLFFBQVEsQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLDJCQUFnQixDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUEseUJBQXlCO1FBQ2hHLE1BQU0sWUFBWSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHdCQUF3QjtRQUNoRSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRTVCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxlQUFJLENBQUMsNENBQTRDLEVBQUM7O1FBRTlDLE1BQU0sUUFBUSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx1QkFBVyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUEsWUFBWTtRQUMvRSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLCtCQUErQjtRQUMvQixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyw2QkFBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBRW5FLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsb0RBQW9ELEVBQUU7O1FBQ3ZELElBQUksSUFBSSxHQUFFLE1BQU0sT0FBTyxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMvQyxpRUFBaUU7UUFDaEUsTUFBTSxPQUFPLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDM0MsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxQixrQkFBa0I7UUFDbEIsTUFBTSxDQUFDLE1BQU0sS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdkQsa0NBQWtDO1FBQ2xDLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELHNCQUFzQjtRQUN2QixzRUFBc0U7UUFDckUsTUFBTSxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBRW5DLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDSCxlQUFJLENBQUMsNERBQTRELEVBQUM7O1FBQ2hFLE1BQU0sT0FBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hCLGtCQUFrQjtRQUNsQixNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMxRCxxQkFBcUI7UUFDckIsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7UUFDM0QsZ0NBQWdDO1FBQ2hDLE1BQU0sUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNyQyxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlELE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUV0QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0wsMEVBQTBFO0FBQzFFLDJFQUEyRTtBQUMzRSx3QkFBd0I7QUFDeEIsUUFBUTtBQUNSLHdIQUF3SDtBQUN4SCwyRUFBMkU7QUFDM0Usd0JBQXdCO0FBQ3hCLFFBQVEifQ==