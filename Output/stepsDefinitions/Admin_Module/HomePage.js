"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../../pageobjects/login");
const loginData_json_1 = __importDefault(require("../../TestData/loginData.json"));
const homePage_json_1 = __importDefault(require("../../TestData/homePage.json"));
const homePage_1 = require("../../pageobjects/homePage");
const ptor_1 = require("protractor/built/ptor");
//import forgotPassword from "../../TestData/forgotPasswordPage.json";
let expect = require('chai').expect;
//expect.use(require('chai-smoothie'));
let Login = new login_1.login();
let Homepage = new homePage_1.homePage();
var assert = require('chai').assert;
//Enter the Url on browser and navigate to the Dayamed Qa Portal then login with valid admin credendetials
cucumber_1.Given('Navigate to the Dayamed QA portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.waitForAngularEnabled(false);
        yield protractor_1.browser.manage().window().maximize();
        yield protractor_1.browser.get(loginData_json_1.default.URL);
        try {
            yield Login.Adv_Btn.click();
            yield Login.Proceed_Btn.click();
            protractor_1.browser.sleep(5000);
        }
        catch (error) {
            console.log(error);
        }
        //login with Admin credentials
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //enter valid username
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //Enter valid password
        yield Login.loginButton.click(); //click on login button
        yield protractor_1.browser.sleep(5000);
    });
});
//Verify the all fields present on home page
cucumber_1.When('I\'m on homepage verify the all fields on page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //.Click on menu button and verify the dropdowns one by one 
        yield Homepage.menuBtn.click(); //click on menu button 
        //1.click on provider dropdown and click on provider list then verify the whether page navigate ti the provider list
        yield Homepage.ProviderDropdown.click(); //click om provider dropdown
        yield Homepage.provider_List.click(); // Click on provider list
        //await browser.sleep(3000);
        //verify the caregiver list page title 
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.homepage_title);
        yield protractor_1.browser.sleep(3000);
        //2.click on Add provider and verify the page title
        //await Homepage.menuBtn.click();//click on menu button
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.ProviderDropdown.click(); //click on provider
        yield Homepage.Add_Provider.click(); //click on add provider
        //verify the add provider page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.AddProvider_TitleName);
        yield protractor_1.browser.sleep(3000);
        //3.click on Caregiver list  link and verify whether page is moved to caregiver page 
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform(); //await Homepage.menuBtn.click(); //click on menu button
        yield Homepage.cargiverDropDown.click(); //click on caregiver dropdown
        yield Homepage.cargiver_list.click(); //click on caregiver list
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.CaregiverListPage_Title);
        yield protractor_1.browser.sleep(3000);
        //4.click on add caregiver link and verify whether the page moved to the add care giver page 
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform(); //await Homepage.menuBtn.click(); //click on menu button
        yield Homepage.cargiverDropDown.click(); //click on caregiver dropdown
        yield Homepage.AddCaregiver.click(); //click on caregiver list
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.AddCaregiver_pageTitle);
        yield protractor_1.browser.sleep(3000);
        //5.Click on add Administration and select Pharmacy list then verify whether page navigated to the Pharmacy list page
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.Administration_DropDown.click(); //click on Administration
        yield Homepage.PharmacyList.click(); //click on Pharmacy list
        //Then verify the page title of the pharmacy list
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.pharmacyList_PageTitle);
        yield protractor_1.browser.sleep(3000);
        //6.Click on Relations Management under udministration and verify the page title
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.Administration_DropDown.click(); //click on Administration
        yield Homepage.RelationsManagement.click(); //click on RelationsManagement 
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.RelationsManagement_PageTitle);
        yield protractor_1.browser.sleep(3000);
        //7.click on Terms and Conditions which is under Administration And verify the page title
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.Administration_DropDown.click(); //click on Administration
        yield Homepage.Terms_Conditions.click(); //click on terms and conditions
        //Then verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.TermsConditions_PageTitle);
        yield protractor_1.browser.sleep(10000);
        //8. click on Reports and sellect Adherence then verify the whether page navigated to the adherence page
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.Reports_DropDown.click(); //click on Reports
        yield Homepage.Adherence.click(); //click on Adherence
        //Then verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.Adherence_PagetTitle);
        yield protractor_1.browser.sleep(15000);
        //9. click on change password under account settings and verify the page whether its navigated to the login page
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.AccountSettings_DropDown.click(); //click on account settings
        yield Homepage.ChangePassword.click(); //click on change password
        //Then verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.changePassword_Title);
        yield protractor_1.browser.sleep(3000);
        //10. click account settings and select the logout then verify the whether it is navigated to the login page
        protractor_1.browser.actions().doubleClick(Homepage.menuBtn).perform();
        yield Homepage.AccountSettings_DropDown.click(); //click on account settings
        yield Homepage.Logout.click(); //click on Logout password
        protractor_1.browser.sleep(3000);
        //Then verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(loginData_json_1.default.LoginPage_Title);
        //9.Again login with admin credentials then click on menu ,Click on Account circle
        // and select the help link then verify whether its navigated to the help page
        yield Login.userName.sendKeys(loginData_json_1.default.valid_userName); //enter valid username
        yield Login.password.sendKeys(loginData_json_1.default.valid_password); //Enter valid password
        yield Login.loginButton.click(); //click on login button
        yield protractor_1.browser.sleep(5000);
        yield Homepage.account_circle.click(); //click on account circle
        yield Homepage.help.click(); //click on help link
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.help_pageTitle);
        //10.select change password from account circle and verify whether page navigated to the changepassword page
        yield Homepage.account_circle.click(); //click on account circle
        yield Homepage.account_ChangePassword.click(); //click on change password
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(homePage_json_1.default.changePassword_Title);
        //11. // click on account circle and click on Logout verify page navigated to the login page
        yield Homepage.account_circle.click(); //click on account circle
        yield Homepage.account_Logout.click(); //click on Logout
        protractor_1.browser.sleep(3000);
        //verify the page title
        expect(yield protractor_1.browser.getTitle()).to.equal(loginData_json_1.default.LoginPage_Title);
    });
});
// Enter the provider name in search provider and verify the whether provider card displayed
cucumber_1.When('enter the provider name and verify the page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        protractor_1.browser.sleep(5000);
        yield Homepage.search_provider.sendKeys(homePage_json_1.default.provider_Name); //enter the provider name
        yield protractor_1.browser.sleep(3000);
        expect(yield Homepage.provider_card.isDisplayed()).to.equal(true); //verify the card on the page whether displayed 
    });
});
cucumber_1.When('click on load more Providers then verify the count', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //1. verify the initial providers count on page, it should be 10 of max
        expect(yield Homepage.providers_count.count()).to.equal(10);
        //click on load more providers button untill all prroviders list displayed then verify the count
        for (let i = 2; expect(yield Homepage.loadMore_ProvidersBtn.isPresent()); i++) {
            yield protractor_1.browser.sleep(5000);
            yield Homepage.loadMore_ProvidersBtn.click(); //click on load more providers button
            yield protractor_1.browser.sleep(5000);
            let max_count = yield Homepage.providers_count.count();
            console.log(yield max_count);
            yield protractor_1.browser.actions().sendKeys(ptor_1.protractor.Key.END).perform();
            let element_status = expect(yield Homepage.loadMore_ProvidersBtn.isPresent());
            if (element_status) {
                expect(yield Homepage.providers_count.count()).to.equal(i * 10);
            }
            else {
                //verify the final count
                expect(yield Homepage.pagination.getText()).to.equal(homePage_json_1.default.ProvidersMax_Count);
                // expect(await Homepage.pagination.getText()).to.contain.text(homepageData.ProvidersMax_Count);
            }
        }
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSG9tZVBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zdGVwc0RlZmluaXRpb25zL0FkbWluX01vZHVsZS9Ib21lUGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBaUQ7QUFDakQsbURBQWdEO0FBQ2hELG1GQUF3RDtBQUN4RCxpRkFBd0Q7QUFDeEQseURBQW1EO0FBQ25ELGdEQUFtRDtBQUNuRCxzRUFBc0U7QUFDdEUsSUFBSSxNQUFNLEdBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUNuQyx1Q0FBdUM7QUFDdkMsSUFBSSxLQUFLLEdBQUMsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUN0QixJQUFJLFFBQVEsR0FBQyxJQUFJLG1CQUFRLEVBQUUsQ0FBQztBQUM1QixJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDO0FBRXBDLDBHQUEwRztBQUMxRyxnQkFBSyxDQUFDLG1DQUFtQyxFQUFFOztRQUN2QyxvQkFBTyxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ2xDLE1BQU0sb0JBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUMzQyxNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLHdCQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsSUFBRztZQUNDLE1BQU0sS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUM1QixNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEI7UUFDSixPQUFNLEtBQUssRUFDUjtZQUNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkI7UUFDRCw4QkFBOEI7UUFDOUIsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyx3QkFBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUEsc0JBQXNCO1FBQzlFLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLHNCQUFzQjtRQUM5RSxNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSx1QkFBdUI7UUFDdkQsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUVqQyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBQ0gsNENBQTRDO0FBQzlDLGVBQUksQ0FBQyxnREFBZ0QsRUFBRTs7UUFFbkQsNERBQTREO1FBQzVELE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHVCQUF1QjtRQUN0RCxvSEFBb0g7UUFDcEgsTUFBTSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSw0QkFBNEI7UUFDcEUsTUFBTyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEseUJBQXlCO1FBQy9ELDRCQUE0QjtRQUM1Qix1Q0FBdUM7UUFDdkMsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN2RSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTFCLG1EQUFtRDtRQUNuRCx1REFBdUQ7UUFDdkQsb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRTFELE1BQU0sUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsbUJBQW1CO1FBQzNELE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHVCQUF1QjtRQUMzRCxvQ0FBb0M7UUFDcEMsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzlFLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFMUIscUZBQXFGO1FBQ3JGLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFBLHdEQUF3RDtRQUNsSCxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLDZCQUE2QjtRQUNyRSxNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSx5QkFBeUI7UUFDOUQsdUJBQXVCO1FBQ3ZCLE1BQU0sQ0FBQyxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFZLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNoRixNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTFCLDZGQUE2RjtRQUM3RixvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQSx3REFBd0Q7UUFDbEgsTUFBTSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSw2QkFBNkI7UUFDckUsTUFBTSxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEseUJBQXlCO1FBQzdELHVCQUF1QjtRQUN2QixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBWSxDQUFDLHNCQUFzQixDQUFDLENBQUM7UUFDL0UsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUUzQixxSEFBcUg7UUFDckgsb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFELE1BQU0sUUFBUSxDQUFDLHVCQUF1QixDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEseUJBQXlCO1FBQ3hFLE1BQU0sUUFBUSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHdCQUF3QjtRQUM1RCxpREFBaUQ7UUFDakQsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQy9FLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFMUIsZ0ZBQWdGO1FBQ2hGLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxRCxNQUFNLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHlCQUF5QjtRQUN4RSxNQUFNLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLCtCQUErQjtRQUMxRSx1QkFBdUI7UUFDdkIsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBQ3RGLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFMUIseUZBQXlGO1FBQ3pGLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxRCxNQUFNLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHlCQUF5QjtRQUN4RSxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLCtCQUErQjtRQUN2RSw0QkFBNEI7UUFDNUIsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2xGLE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFM0Isd0dBQXdHO1FBQ3hHLG9CQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUMxRCxNQUFNLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLGtCQUFrQjtRQUMxRCxNQUFNLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSxvQkFBb0I7UUFDckQsNEJBQTRCO1FBQzVCLE1BQU0sQ0FBQyxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHVCQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUM3RSxNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTNCLGdIQUFnSDtRQUNoSCxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDMUQsTUFBTSxRQUFRLENBQUMsd0JBQXdCLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSwyQkFBMkI7UUFDM0UsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsMEJBQTBCO1FBQ2hFLDRCQUE0QjtRQUM1QixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBWSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDN0UsTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUUxQiw0R0FBNEc7UUFDNUcsb0JBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFELE1BQU0sUUFBUSxDQUFDLHdCQUF3QixDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsMkJBQTJCO1FBQzNFLE1BQU0sUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLDBCQUEwQjtRQUN4RCxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQiw0QkFBNEI7UUFDNUIsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsd0JBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUVyRSxrRkFBa0Y7UUFDbEYsOEVBQThFO1FBQzlFLE1BQU0sS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsd0JBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFBLHNCQUFzQjtRQUM5RSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQSxzQkFBc0I7UUFDOUUsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsdUJBQXVCO1FBQ3ZELE1BQU0sb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEseUJBQXlCO1FBQy9ELE1BQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLG9CQUFvQjtRQUNoRCx1QkFBdUI7UUFDdkIsTUFBTSxDQUFDLE1BQU0sb0JBQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUV2RSw0R0FBNEc7UUFDNUcsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEseUJBQXlCO1FBQy9ELE1BQU0sUUFBUSxDQUFDLHNCQUFzQixDQUFDLEtBQUssRUFBRSxDQUFDLENBQUEsMEJBQTBCO1FBQ3hFLHVCQUF1QjtRQUN2QixNQUFNLENBQUMsTUFBTSxvQkFBTyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyx1QkFBWSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFFN0UsNEZBQTRGO1FBQzVGLE1BQU0sUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHlCQUF5QjtRQUMvRCxNQUFNLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQSxpQkFBaUI7UUFDdkQsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsdUJBQXVCO1FBQ3ZCLE1BQU0sQ0FBQyxNQUFNLG9CQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLHdCQUFTLENBQUMsZUFBZSxDQUFDLENBQUM7SUFFdEUsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNMLDRGQUE0RjtBQUM1RixlQUFJLENBQUMsNkNBQTZDLEVBQUU7O1FBQ2xELG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsdUJBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBLHlCQUF5QjtRQUM3RixNQUFNLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxNQUFNLFFBQVEsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUEsZ0RBQWdEO0lBRWxILENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxlQUFJLENBQUMsb0RBQW9ELEVBQUM7O1FBRXhELHVFQUF1RTtRQUN2RSxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU1RCxnR0FBZ0c7UUFDaEcsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQ3hFO1lBQ0ksTUFBTSxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixNQUFNLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFBLHFDQUFxQztZQUNsRixNQUFPLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksU0FBUyxHQUFDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sU0FBUyxDQUFDLENBQUM7WUFDN0IsTUFBTSxvQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUUvRCxJQUFJLGNBQWMsR0FBQyxNQUFNLENBQUMsTUFBTSxRQUFRLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQTtZQUMzRSxJQUFHLGNBQWMsRUFDakI7Z0JBQ0EsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzdEO2lCQUdBO2dCQUNJLHdCQUF3QjtnQkFDeEIsTUFBTSxDQUFDLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsdUJBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO2dCQUN2RixnR0FBZ0c7YUFDakc7U0FFSjtJQUVILENBQUM7Q0FBQSxDQUFDLENBQUMifQ==