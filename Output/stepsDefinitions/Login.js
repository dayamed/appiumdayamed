"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../pageobjects/login");
const loginData_json_1 = __importDefault(require("../TestData/loginData.json"));
let expect = require('chai').expect;
let Login = new login_1.login();
cucumber_1.Given('I navigate to the Dayamed QA portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.get(loginData_json_1.default.URL);
    });
});
cucumber_1.When('I enter the  Email {string} on the login page', function (string) {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.userName.sendKeys(string);
    });
});
cucumber_1.When('I enter the Password {string} on the login page', function (string) {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.password.sendKeys(string);
    });
});
cucumber_1.When('I click on the login button', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield Login.loginButton.click();
        protractor_1.browser.sleep(5000);
    });
});
cucumber_1.Then('the {string} page is displayed', function (pagename) {
    return __awaiter(this, void 0, void 0, function* () {
        if (pagename == 'homePage') {
            yield expect((Login.appliedClaims).isDisplayed());
        }
        else if (pagename == 'assignedClaims') {
            yield expect((Login.assignClaimPage).isDisplayed());
        }
        else if (pagename == "Reports") {
            yield expect((Login.Reports).isDisplayed());
        }
        else if (pagename == 'Admin') {
            yield expect((Login.adminPage).isDisplayed());
        }
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTG9naW4uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwc0RlZmluaXRpb25zL0xvZ2luLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFxQztBQUNyQyxnREFBNkM7QUFDN0MsZ0ZBQXFEO0FBQ3JELElBQUksTUFBTSxHQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFFbkMsSUFBSSxLQUFLLEdBQUMsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUd0QixnQkFBSyxDQUFDLHFDQUFxQyxFQUFFOztRQUN6QyxNQUFNLG9CQUFPLENBQUMsR0FBRyxDQUFDLHdCQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVMLGVBQUksQ0FBQywrQ0FBK0MsRUFBQyxVQUFnQixNQUFNOztRQUV2RSxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsaURBQWlELEVBQUMsVUFBZ0IsTUFBTTs7UUFFM0UsTUFBTSxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUV4QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDZCQUE2QixFQUFDOztRQUM3QixNQUFNLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxnQ0FBZ0MsRUFBRSxVQUFlLFFBQVE7O1FBRTFELElBQUcsUUFBUSxJQUFFLFVBQVUsRUFDdkI7WUFDSSxNQUFPLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1NBQ3REO2FBQ0ksSUFBRyxRQUFRLElBQUUsZ0JBQWdCLEVBQ2xDO1lBQ0MsTUFBTyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNyRDthQUNJLElBQUcsUUFBUSxJQUFJLFNBQVMsRUFDN0I7WUFDRSxNQUFPLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1NBQzlDO2FBQ0ksSUFBRyxRQUFRLElBQUUsT0FBTyxFQUN6QjtZQUNDLE1BQU8sTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7U0FDL0M7SUFFSCxDQUFDO0NBQUEsQ0FBQyxDQUFDIn0=