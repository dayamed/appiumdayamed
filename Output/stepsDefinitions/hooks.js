"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../pageobjects/login");
let Login = new login_1.login();
//This hook will be executed before each scenario
cucumber_1.Before(function () {
    protractor_1.browser.driver.manage().window().maximize();
});
/* //This hook will be executed before scenarios tagged with @sanity
Before({tags: "@sanity"}, function () {
    browser.driver.manage().window().maximize();
  });

//This hook will be executed before scenarios tagged with @regression
Before({tags: "@regression"}, function () {
    browser.driver.manage().window().maximize();
});   */
//This hook will be executed after each of the scenario
cucumber_1.After(function (scenario) {
    return __awaiter(this, void 0, void 0, function* () {
        if (scenario.result.status == cucumber_1.Status.FAILED || scenario.result.status == cucumber_1.Status.PASSED) {
            //code to take screenshot
            const screenshot = yield protractor_1.browser.takeScreenshot();
            this.attach(screenshot, "image/png");
        }
    });
});
/*   After(async function () {
    await Login.userImage.click();
    browser.sleep(1000);
    await Login.Logout.click();
  });   */ 
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9va3MuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwc0RlZmluaXRpb25zL2hvb2tzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsdUNBQStDO0FBQy9DLDJDQUFxQztBQUNyQyxnREFBNkM7QUFDN0MsSUFBSSxLQUFLLEdBQUMsSUFBSSxhQUFLLEVBQUUsQ0FBQztBQUV0QixpREFBaUQ7QUFDakQsaUJBQU0sQ0FBQztJQUNILG9CQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRSxDQUFDO0FBQ2hELENBQUMsQ0FBQyxDQUFDO0FBRUg7Ozs7Ozs7O1FBUVE7QUFHUix1REFBdUQ7QUFDdkQsZ0JBQUssQ0FBQyxVQUFnQixRQUFROztRQUMxQixJQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFFLGlCQUFNLENBQUMsTUFBTSxJQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFFLGlCQUFNLENBQUMsTUFBTSxFQUFDO1lBQzVFLHlCQUF5QjtZQUN6QixNQUFNLFVBQVUsR0FBRyxNQUFNLG9CQUFPLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDbEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUMsV0FBVyxDQUFDLENBQUM7U0FDdkM7SUFDSCxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUw7Ozs7VUFJVSJ9