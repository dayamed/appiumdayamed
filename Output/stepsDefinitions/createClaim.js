"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const claimCreation_1 = require("../pageobjects/claimCreation");
const claimData_json_1 = __importDefault(require("../TestData/claimData.json"));
const loginData_json_1 = __importDefault(require("../TestData/loginData.json"));
const login_1 = require("../pageObjects/login");
let expect = require('chai').expect;
let createClaim = new claimCreation_1.claimCreation();
let Login = new login_1.login();
cucumber_1.Given('I log in to the reimbursement site with associate role', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.get(loginData_json_1.default.URL);
        yield Login.userName.sendKeys(claimData_json_1.default.associateEmail);
        yield Login.loginButton.click();
    });
});
cucumber_1.When('I click on New Claim under Reimbursement section', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //clicking on Reimbursement link
        yield createClaim.reimburseLink.click();
        //clikcing on newClaim link under Reimbursement
        yield createClaim.newClaimLink.click();
    });
});
cucumber_1.Then('I add the Late Night Claim with each Nature of Spend option', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //adding "WeekDay claim" under LateNight Claim
        yield createClaim.weekDayClaimLW();
        //adding "Weekend Food" under LateNight Claim
        yield createClaim.weekendClaimLW();
        //adding "LateNight Conveyance" under LateNight Claim
        yield createClaim.conveyanceClaimLW();
    });
});
cucumber_1.Then('Send the Late Night Claim to Approver for approval', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield createClaim.sendForApproval.click();
    });
});
//getting claim number from claim success alert
function getClaimNumber() {
    return __awaiter(this, void 0, void 0, function* () {
        return createClaim.claimSuccessAlert.getText().then(function (claimMessage) {
            let claimNumber = claimMessage.substr(41, 12);
            return claimNumber;
        });
    });
}
cucumber_1.Then("I navigate to view claims page and verify claim status {string}", function (status) {
    return __awaiter(this, void 0, void 0, function* () {
        yield expect(createClaim.claimSuccessAlert.isPresent()).to.eventually.equal(true);
        const claimNumber = yield getClaimNumber();
        yield createClaim.claimOk.click();
        yield expect(createClaim.viewClaimLink.isDisplayed());
        yield createClaim.viewClaimLink.click();
        yield expect(createClaim.applyClaim.isDisplayed());
        createClaim.claimNumbers_VC.then(function (itemList) {
            //comparing the Claim Number in View Claim Page 
            for (let i = 0; i < itemList.length; i++) {
                itemList[i].getText().then(function (vcNumber) {
                    if (vcNumber == claimNumber) {
                        expect(vcNumber).to.equal(claimNumber);
                    }
                });
            }
            ;
        });
        //verifying the claim status in view claim page
        yield createClaim.statusVerify(status);
    });
});
cucumber_1.Then('I verify that view claim section contains all the Latenight claims as per submitted nature of spend', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //clicking on view claim button in view claim page
        yield createClaim.viewClaimIcon.click().then(function () {
            return __awaiter(this, void 0, void 0, function* () {
                //clicking on weekday claim details in view claim page after the claim send for approval
                yield createClaim.viewClaimWD();
                protractor_1.browser.sleep(1000);
                //clicking on weekend claim details in view claim page after the claim send for approval
                yield createClaim.viewClaimWE();
                protractor_1.browser.sleep(1000);
                //verifying on conveyance claim details in view claim page after the claim send for approval
                yield createClaim.viewClaimCE();
            });
        });
    });
});
cucumber_1.Then('I logout from reimbursement site as associate role', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield createClaim.logout();
    });
});
cucumber_1.When('I login to the reimbursement site with approver role', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield createClaim.approverLogin();
    });
});
cucumber_1.Then('I approve the claim from the assigned claims page', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //navigating to assigned claims page, selecting the claim and verifying the claim data
        // AC - Assigned Claims
        yield createClaim.selectClaim_AC();
        yield createClaim.cNumber.getText().then(function (claimNumber) {
            return __awaiter(this, void 0, void 0, function* () {
                //selcting approve radio button
                yield createClaim.approveClaim.click();
                yield createClaim.Comments.sendKeys('approving the claim').then(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        //approving the claim
                        yield createClaim.Submit.click();
                    });
                });
                //verifying claim number in approve, verify and paid success alert
                let number = yield createClaim.successAlert();
                expect(number).to.equal(claimNumber);
            });
        });
    });
});
cucumber_1.Then('I logout from Approver Role and re-login with Associate Role and verify the claim status {string}', function (status) {
    return __awaiter(this, void 0, void 0, function* () {
        //Logout from Approver role
        yield createClaim.logout();
        //login to associate role
        yield createClaim.associateLogin();
        //navigating to view claims page
        yield createClaim.viewClaims_AR();
        //verifying the claim Number
        yield createClaim.VC_claimNumber();
        //verifying the Claim Status in view claim page after approving the claim
        yield createClaim.statusVerify(status);
    });
});
cucumber_1.Then('I Logout from Associate Role and Login with Verifier1 Role', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //logout from Associate Role
        yield createClaim.logout();
        //login with Verifier 1 Role
        yield createClaim.verifier1_Login();
    });
});
cucumber_1.Then('I Navigate to Assigned Claims page and verify the claim', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //navigating to assigned claims page, selecting the claim and verifying the claim data
        yield createClaim.selectClaim_AC();
        yield createClaim.cNumber.getText().then(function (claimNumber) {
            return __awaiter(this, void 0, void 0, function* () {
                //selcting Verify radio button
                yield createClaim.verifyClaim.click();
                yield createClaim.Comments.sendKeys('verifying the claim').then(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        //verifying the claim 
                        yield createClaim.Submit.click();
                    });
                });
                let number = yield createClaim.successAlert();
                expect(number).to.equal(claimNumber);
            });
        });
    });
});
cucumber_1.Then('I logout from Verifier1 Role and re-login with Associate Role and verify the claim status {string}', function (status) {
    return __awaiter(this, void 0, void 0, function* () {
        //logout from Verifier1 Role
        yield createClaim.logout();
        //re-login with Associate Role
        yield createClaim.associateLogin();
        //navigating to view claims page
        yield createClaim.viewClaims_AR();
        //verifying the claim Number
        yield createClaim.VC_claimNumber();
        //validating the Claim Status in view claim page after verifying the claim
        yield createClaim.statusVerify(status);
    });
});
cucumber_1.Then('I Logout from Associate Role and login with Verifier2 Role', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //logout from Associate Role
        yield createClaim.logout();
        //login with Verifier-2 Role
        yield createClaim.verifier2_Login();
    });
});
cucumber_1.Then('I Navigate to Assigned Claims page and Pay for the claim', function () {
    return __awaiter(this, void 0, void 0, function* () {
        //navigating to assigned claims page, selecting the claim and verifying the claim data
        yield createClaim.selectClaim_AC();
        yield createClaim.cNumber.getText().then(function (claimNumber) {
            return __awaiter(this, void 0, void 0, function* () {
                //selcting approve radio button
                yield createClaim.payClaim.click();
                yield createClaim.Comments.sendKeys('Paying for the claim').then(function () {
                    return __awaiter(this, void 0, void 0, function* () {
                        //paying for the claim    
                        yield createClaim.Submit.click();
                    });
                });
                let number = yield createClaim.successAlert();
                expect(number).to.equal(claimNumber);
            });
        });
    });
});
cucumber_1.Then('I logout from Verifier2 Role and re-login with Associate Role and verify the claim status {string}', function (status) {
    return __awaiter(this, void 0, void 0, function* () {
        //logout from Verifier2 Role
        yield createClaim.logout();
        //re-login with Associate Role
        yield createClaim.associateLogin();
        //navigating to view claims page
        yield createClaim.viewClaims_AR();
        //verifying the claim Number
        yield createClaim.VC_claimNumber();
        //verifying the Claim Status in view claim page after paid for the claim
        yield createClaim.statusVerify(status);
    });
});
//logout from the reimbursement site
cucumber_1.Then('I Logout from the Reimbursement Site', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield createClaim.logout();
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlQ2xhaW0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zdGVwc0RlZmluaXRpb25zL2NyZWF0ZUNsYWltLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFrRDtBQUNsRCxnRUFBNkQ7QUFDN0QsZ0ZBQW1EO0FBQ25ELGdGQUFtRDtBQUNuRCxnREFBNkM7QUFFN0MsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUNwQyxJQUFJLFdBQVcsR0FBRyxJQUFJLDZCQUFhLEVBQUUsQ0FBQztBQUN0QyxJQUFJLEtBQUssR0FBRyxJQUFJLGFBQUssRUFBRSxDQUFDO0FBR3hCLGdCQUFLLENBQUMsd0RBQXdELEVBQUU7O1FBQzlELE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQyxNQUFNLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHdCQUFTLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDeEQsTUFBTSxLQUFLLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2xDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsa0RBQWtELEVBQUU7O1FBQ3ZELGdDQUFnQztRQUNoQyxNQUFNLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFeEMsK0NBQStDO1FBQy9DLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLDZEQUE2RCxFQUFFOztRQUNsRSw4Q0FBOEM7UUFDOUMsTUFBTSxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkMsNkNBQTZDO1FBQzdDLE1BQU0sV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5DLHFEQUFxRDtRQUNyRCxNQUFNLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQ3hDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsb0RBQW9ELEVBQUU7O1FBQ3pELE1BQU0sV0FBVyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsK0NBQStDO0FBQy9DLFNBQWUsY0FBYzs7UUFDM0IsT0FBTyxXQUFXLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsWUFBWTtZQUN4RSxJQUFJLFdBQVcsR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM5QyxPQUFPLFdBQVcsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7Q0FBQTtBQUVELGVBQUksQ0FBQyxpRUFBaUUsRUFBRSxVQUFnQixNQUFNOztRQUM1RixNQUFNLE1BQU0sQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNsRixNQUFNLFdBQVcsR0FBRyxNQUFNLGNBQWMsRUFBRSxDQUFDO1FBRTNDLE1BQU0sV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxNQUFNLE1BQU0sQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7UUFDdEQsTUFBTSxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hDLE1BQU0sTUFBTSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUVuRCxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLFFBQVE7WUFDakQsZ0RBQWdEO1lBQ2hELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN4QyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsUUFBUTtvQkFDM0MsSUFBSSxRQUFRLElBQUksV0FBVyxFQUFFO3dCQUMzQixNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztxQkFDeEM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUFBLENBQUM7UUFDSixDQUFDLENBQUMsQ0FBQztRQUNILCtDQUErQztRQUMvQyxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFekMsQ0FBQztDQUFBLENBQUMsQ0FBQTtBQUVGLGVBQUksQ0FBQyxxR0FBcUcsRUFBRTs7UUFDMUcsa0RBQWtEO1FBQ2xELE1BQU0sV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLENBQUM7O2dCQUMzQyx3RkFBd0Y7Z0JBQ3hGLE1BQU0sV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUNoQyxvQkFBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFcEIsd0ZBQXdGO2dCQUN4RixNQUFNLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDaEMsb0JBQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BCLDRGQUE0RjtnQkFDNUYsTUFBTSxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbEMsQ0FBQztTQUFBLENBQUMsQ0FBQTtJQUNKLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsb0RBQW9ELEVBQUU7O1FBQ3pELE1BQU0sV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQzdCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsc0RBQXNELEVBQUU7O1FBQzNELE1BQU0sV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3BDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsbURBQW1ELEVBQUU7O1FBQ3hELHNGQUFzRjtRQUN0Rix1QkFBdUI7UUFDdkIsTUFBTSxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkMsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixXQUFXOztnQkFFbEUsK0JBQStCO2dCQUMvQixNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3ZDLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUM7O3dCQUM5RCxxQkFBcUI7d0JBQ3JCLE1BQU0sV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDbkMsQ0FBQztpQkFBQSxDQUFDLENBQUE7Z0JBRUYsa0VBQWtFO2dCQUNsRSxJQUFJLE1BQU0sR0FBRyxNQUFNLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDOUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFdkMsQ0FBQztTQUFBLENBQUMsQ0FBQTtJQUNKLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsbUdBQW1HLEVBQUUsVUFBZ0IsTUFBTTs7UUFDOUgsMkJBQTJCO1FBQzNCLE1BQU0sV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRTNCLHlCQUF5QjtRQUN6QixNQUFNLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQyxnQ0FBZ0M7UUFDaEMsTUFBTSxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFbEMsNEJBQTRCO1FBQzVCLE1BQU0sV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5DLHlFQUF5RTtRQUN6RSxNQUFNLFdBQVcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUdILGVBQUksQ0FBQyw0REFBNEQsRUFBRTs7UUFDakUsNEJBQTRCO1FBQzVCLE1BQU0sV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzNCLDRCQUE0QjtRQUM1QixNQUFNLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLHlEQUF5RCxFQUFFOztRQUM5RCxzRkFBc0Y7UUFDdEYsTUFBTSxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkMsTUFBTSxXQUFXLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFnQixXQUFXOztnQkFFbEUsOEJBQThCO2dCQUM5QixNQUFNLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3RDLE1BQU0sV0FBVyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUM7O3dCQUM5RCxzQkFBc0I7d0JBQ3RCLE1BQU0sV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDbkMsQ0FBQztpQkFBQSxDQUFDLENBQUE7Z0JBRUYsSUFBSSxNQUFNLEdBQUcsTUFBTSxXQUFXLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQzlDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7U0FBQSxDQUFDLENBQUE7SUFDSixDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUgsZUFBSSxDQUFDLG9HQUFvRyxFQUFFLFVBQWdCLE1BQU07O1FBQy9ILDRCQUE0QjtRQUM1QixNQUFNLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMzQiw4QkFBOEI7UUFDOUIsTUFBTSxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkMsZ0NBQWdDO1FBQ2hDLE1BQU0sV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRWxDLDRCQUE0QjtRQUM1QixNQUFNLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUVuQywwRUFBMEU7UUFDMUUsTUFBTSxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsNERBQTRELEVBQUU7O1FBQ2pFLDRCQUE0QjtRQUM1QixNQUFNLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUUzQiw0QkFBNEI7UUFDNUIsTUFBTSxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDdEMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywwREFBMEQsRUFBRTs7UUFDL0Qsc0ZBQXNGO1FBQ3RGLE1BQU0sV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5DLE1BQU0sV0FBVyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBZ0IsV0FBVzs7Z0JBRWxFLCtCQUErQjtnQkFDL0IsTUFBTSxXQUFXLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNuQyxNQUFNLFdBQVcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUMsSUFBSSxDQUFDOzt3QkFDL0QsMEJBQTBCO3dCQUMxQixNQUFNLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ25DLENBQUM7aUJBQUEsQ0FBQyxDQUFBO2dCQUVGLElBQUksTUFBTSxHQUFHLE1BQU0sV0FBVyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUM5QyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN2QyxDQUFDO1NBQUEsQ0FBQyxDQUFBO0lBRUosQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyxvR0FBb0csRUFBRSxVQUFnQixNQUFNOztRQUUvSCw0QkFBNEI7UUFDNUIsTUFBTSxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0IsOEJBQThCO1FBQzlCLE1BQU0sV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRW5DLGdDQUFnQztRQUNoQyxNQUFNLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVsQyw0QkFBNEI7UUFDNUIsTUFBTSxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFFbkMsd0VBQXdFO1FBQ3hFLE1BQU0sV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN6QyxDQUFDO0NBQUEsQ0FBQyxDQUFBO0FBRUYsb0NBQW9DO0FBQ3BDLGVBQUksQ0FBQyxzQ0FBc0MsRUFBRTs7UUFDM0MsTUFBTSxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDN0IsQ0FBQztDQUFBLENBQUMsQ0FBQSJ9