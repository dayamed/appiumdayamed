"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../pageobjects/login");
const logindata_json_1 = __importDefault(require("../TestData/logindata.json"));
let expect = require('chai').expect;
let RLogin = new login_1.login();
cucumber_1.Given('I navigate to the reimbursement portall', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.get(logindata_json_1.default.URL);
    });
});
cucumber_1.When('I enter {string} on the login pagee', function (string) {
    return __awaiter(this, void 0, void 0, function* () {
        yield RLogin.UserEmail.sendKeys(string);
    });
});
cucumber_1.When('I click on the login buttonn', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield RLogin.loginButton.click();
        protractor_1.browser.sleep(5000);
    });
});
cucumber_1.Then('the {string} page is displayedd', function (pagename) {
    return __awaiter(this, void 0, void 0, function* () {
        if (pagename == 'HomePage') {
            yield expect((RLogin.homePage).isDisplayed());
        }
        else if (pagename == 'AssignedClaims') {
            yield expect((RLogin.AssignClaimPage).isDisplayed());
        }
        else if (pagename == "Reports") {
            yield expect((RLogin.Reports).isDisplayed());
        }
        else if (pagename == 'Admin') {
            yield expect((RLogin.AdminPage).isDisplayed());
        }
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQVJUX2xvZ2luLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc3RlcHNEZWZpbml0aW9ucy9BUlRfbG9naW4udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSx1Q0FBNkM7QUFDN0MsMkNBQXFDO0FBRXJDLGdEQUE2QztBQUM3QyxnRkFBcUQ7QUFDckQsSUFBSSxNQUFNLEdBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUVuQyxJQUFJLE1BQU0sR0FBQyxJQUFJLGFBQUssRUFBRSxDQUFDO0FBR3ZCLGdCQUFLLENBQUMseUNBQXlDLEVBQUU7O1FBQzdDLE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsd0JBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQyxDQUFDO0NBQUEsQ0FBQyxDQUFDO0FBRUwsZUFBSSxDQUFDLHFDQUFxQyxFQUFDLFVBQWdCLE1BQU07O1FBRTdELE1BQU0sTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7SUFFNUMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQyw4QkFBOEIsRUFBQzs7UUFDOUIsTUFBTSxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2pDLG9CQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7Q0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsaUNBQWlDLEVBQUUsVUFBZSxRQUFROztRQUUzRCxJQUFHLFFBQVEsSUFBRSxVQUFVLEVBQ3ZCO1lBQ0ksTUFBTyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUNsRDthQUNJLElBQUcsUUFBUSxJQUFFLGdCQUFnQixFQUNsQztZQUNDLE1BQU8sTUFBTSxDQUFDLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7U0FDdEQ7YUFDSSxJQUFHLFFBQVEsSUFBSSxTQUFTLEVBQzdCO1lBQ0UsTUFBTyxNQUFNLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUMvQzthQUNJLElBQUcsUUFBUSxJQUFFLE9BQU8sRUFDekI7WUFDQyxNQUFPLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1NBQ2hEO0lBRUgsQ0FBQztDQUFBLENBQUMsQ0FBQyJ9