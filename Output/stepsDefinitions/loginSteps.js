"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const login_1 = require("../pageobjects/login");
let lgn = new login_1.login();
cucumber_1.Given('i navigate to the reimbursement portal', function () {
    return __awaiter(this, void 0, void 0, function* () {
        yield protractor_1.browser.get("http://192.168.1.101:9001");
    });
});
cucumber_1.When('i login with the {string} role', function (uName) {
    return __awaiter(this, void 0, void 0, function* () {
        yield lgn.nameEditBox.sendKeys(uName);
        yield lgn.loginButton.click();
    });
});
cucumber_1.Then('home page is displayed', function () {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(lgn.homePage.isDisplayed());
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW5TdGVwcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3N0ZXBzRGVmaW5pdGlvbnMvbG9naW5TdGVwcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBLHVDQUE2QztBQUM3QywyQ0FBbUM7QUFHbkMsZ0RBQTZDO0FBRTdDLElBQUksR0FBRyxHQUFDLElBQUksYUFBSyxFQUFFLENBQUM7QUFDcEIsZ0JBQUssQ0FBQyx3Q0FBd0MsRUFBRTs7UUFDNUMsTUFBTSxvQkFBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO0lBQ2pELENBQUM7Q0FBQSxDQUFDLENBQUM7QUFDTCxlQUFJLENBQUMsZ0NBQWdDLEVBQUMsVUFBZ0IsS0FBSzs7UUFDdkQsTUFBTSxHQUFHLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0QyxNQUFNLEdBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDbEMsQ0FBQztDQUFBLENBQUMsQ0FBQztBQUNILGVBQUksQ0FBQyx3QkFBd0IsRUFBRTs7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDM0MsQ0FBQztDQUFBLENBQUMsQ0FBQyJ9