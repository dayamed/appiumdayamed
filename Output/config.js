// An example configuration file
exports.config = {
    // The address of a running selenium server.
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    // set to "custom" instead of cucumber.
    framework: 'custom',
    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        browserName: 'chrome'
    },
    specs: ['../features/Admin_FeatureFiles/patientReports.feature'],
    cucumberOpts: {
        // require step definitions
        // tags:"@TC_07",
        format: 'json:Output/cucumberreport.json',
        require: [
            './stepsDefinitions/Admin_Module/*.js' // accepts a glob 
        ]
    },
    plugins: [{
            package: 'protractor-multiple-cucumber-html-reporter-plugin',
            options: {
                // read the options part
                automaticallyGenerateReport: true,
                removeExistingJsonReportFile: true
            }
        }],
    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true,
    },
    onPrepare: function () {
        // Load chai assertions
        const chai = require('chai');
        const chaiAsPromised = require('chai-as-promised');
        // Load chai-as-promised support
        chai.use(chaiAsPromised);
        // Initialise should API (attaches as a property on Object)
        chai.should();
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLGdDQUFnQztBQUNoQyxPQUFPLENBQUMsTUFBTSxHQUFHO0lBQ2YsNENBQTRDO0lBQzVDLGtEQUFrRDtJQUNsRCxhQUFhLEVBQUMsSUFBSTtJQUVsQix1Q0FBdUM7SUFDdkMsU0FBUyxFQUFFLFFBQVE7SUFDbkIsMkNBQTJDO0lBQzNDLGFBQWEsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLCtCQUErQixDQUFDO0lBQy9ELHVEQUF1RDtJQUN2RCxZQUFZLEVBQUU7UUFDWixXQUFXLEVBQUUsUUFBUTtLQUV0QjtJQUNELEtBQUssRUFBRSxDQUFDLHVEQUF1RCxDQUFDO0lBRWxFLFlBQVksRUFBRTtRQUNaLDJCQUEyQjtRQUM3QixpQkFBaUI7UUFDZixNQUFNLEVBQUMsaUNBQWlDO1FBQ3hDLE9BQU8sRUFBRTtZQUNQLHNDQUFzQyxDQUFDLGtCQUFrQjtTQUMxRDtLQUNGO0lBQ0QsT0FBTyxFQUFFLENBQUM7WUFDUixPQUFPLEVBQUUsbURBQW1EO1lBQzVELE9BQU8sRUFBQztnQkFDSix3QkFBd0I7Z0JBQ3hCLDJCQUEyQixFQUFFLElBQUk7Z0JBQ2pDLDRCQUE0QixFQUFFLElBQUk7YUFDckM7U0FDRixDQUFDO0lBQ0Esd0NBQXdDO0lBQ3hDLGVBQWUsRUFBRTtRQUNmLFVBQVUsRUFBRSxJQUFJO0tBQ2pCO0lBRUQsU0FBUyxFQUFFO1FBRVQsdUJBQXVCO1FBQ3ZCLE1BQU0sSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM3QixNQUFNLGNBQWMsR0FBRyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUVuRCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUV6QiwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2YsQ0FBQztDQUVILENBQUMifQ==