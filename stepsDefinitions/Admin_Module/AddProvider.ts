import { Given, Then, When } from "cucumber";
import { browser} from "protractor";
import { login } from "../../pageobjects/login";
import  loginData  from "../../TestData/loginData.json";
import {homePage} from "../../pageobjects/homePage";
import addProviderData from "../../TestData/addProvider.json";
import {addProvider} from "../../pageobjects/addProvider";
import homepage from "../../TestData/homePage.json";
let expect =require('chai').expect;
let Login=new login();
let Homepage=new homePage();
let AddProvider=new addProvider();
//enter the url in browser and login with admin
Given('navigate to the QA Dayamed web portal', async function () {
    browser.waitForAngularEnabled(false)
    await browser.manage().window().maximize();
    await browser.get(loginData.URL);
    try{
        await Login.Adv_Btn.click();
        await Login.Proceed_Btn.click();
        browser.sleep(5000);
       }
    catch(error)
       {
        console.log(error);
       }
       await Login.userName.sendKeys(loginData.valid_userName);//enter valid username
       await Login.password.sendKeys(loginData.valid_password);//Enter valid password
       await Login.loginButton.click();//click on login button
       await browser.sleep(5000);
  });
  //1.navigate to the ADD provider page click on submit without any data in required fields then verify the error messages
  When('leave the all required fields as empty and click on submit',async function () {

   
    await Homepage.addProvider_fromProviderPage.click();
    //verify the page title
    expect(await browser.getTitle()).to.equal(addProviderData.page_Title);
    //enter valid data in not required fields

    //select date of birth
    await AddProvider.nickName.sendKeys(addProviderData.valid.nick_name);
    await AddProvider.DOB.click();
    await AddProvider.month_yearBtn.click();
    await AddProvider.Previos20yearsBtn.click();
    await AddProvider.select_Year.click();
    await AddProvider.select_Month.click();
    await AddProvider.select_Date.click();

    //select ethnicity
    await AddProvider.ethnicity_dropdown.click();
    await AddProvider.select_ethnicity.click();
    //enter city name
    await AddProvider.city.sendKeys(addProviderData.valid.city);
    await AddProvider.state.sendKeys(addProviderData.valid.state);
    await AddProvider.country.sendKeys(addProviderData.valid.country);
    await browser.sleep(5000);
    //select smart phone type
    await AddProvider.phoneType_dropdown.click();
    await AddProvider.select_android.click();
    //enter specilization
    await AddProvider.specilization.sendKeys(addProviderData.valid.specilization);
    //Click on submit button
    await AddProvider.submitBtn.click();
    await browser.sleep(2000);
    
  });
  //verify the error messages:
  Then('verify the error messages', async function () {
    expect(await AddProvider.fname_ErrorMsg.getText()).to.equal(addProviderData.error_Msg.fname_error);
    expect(await AddProvider.lname_errorMsg.getText()).to.equal(addProviderData.error_Msg.lname_error);
    expect(await AddProvider.mobileNum_errorMsg.getText()).to.equal(addProviderData.error_Msg.mobileNum_error);
    expect(await AddProvider.email_errorMsg.getText()).to.equal(addProviderData.error_Msg.email_error);
    expect(await AddProvider.address_errorMsg.getText()).to.equal(addProviderData.error_Msg.address_error);
    expect(await AddProvider.zipcode_error.getText()).to.equal(addProviderData.error_Msg.zipcode_error);
    expect(await AddProvider.licence_error.getText()).to.equal(addProviderData.error_Msg.licence_error);
    expect(await AddProvider.pharmacy_error.getText()).to.equal(addProviderData.error_Msg.pharmacy_error);
  });
//2.enter the invalid data in all fields and click on submit then verify the all error messages for the all fields
  When('enter the invalid data in all fields and click on submit button', async function () {
    await Homepage.addProvider_fromProviderPage.click();
      //enter the invalid data in all text fields
      await AddProvider.firstName.sendKeys(addProviderData.invalid.first_name);//first name
      await AddProvider.lastName.sendKeys(addProviderData.invalid.last_name);//last name
      await AddProvider.nickName.sendKeys(addProviderData.invalid.nick_name);//nick name
      await AddProvider.mobile_num.sendKeys(addProviderData.invalid.mobile_number);//mobile number
      await AddProvider.Email.sendKeys(addProviderData.invalid.email);//email address
      await AddProvider.address.sendKeys(addProviderData.invalid.address);//address
      await AddProvider.city.sendKeys(addProviderData.invalid.city);//city
      await AddProvider.state.sendKeys(addProviderData.invalid.state);//state
      await AddProvider.country.sendKeys(addProviderData.invalid.country);//country
      await AddProvider.zip_Code.sendKeys(addProviderData.invalid.zipCode);//zip code
      await AddProvider.specilization.sendKeys(addProviderData.invalid.specilization);//spectilization
      await AddProvider.licence.sendKeys(addProviderData.invalid.licence);//licence
      //And click on submit button
      await AddProvider.submitBtn.click();
       
  });
  //then verify the all error messages
  Then('verify the error messages for all fields', async function () {
    expect(await AddProvider.invalidfname_ErrorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.fname_error);
    expect(await AddProvider.invalidlname_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.lname_error);
    expect(await AddProvider.invalidnickName_error.getText()).to.equal(addProviderData.invalid_ErrorMsg.nickName_errorMsg);
    expect(await AddProvider.invalidmobileNum_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.mobileNum_error);
    expect(await AddProvider.invalidemail_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.email_error);
    expect(await AddProvider.invalidaddress_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.address_error);
    expect(await AddProvider.invalidcity_error.getText()).to.equal(addProviderData.invalid_ErrorMsg.city_errorMsg);
    expect(await AddProvider.invalidState_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.StateError_msg);
    expect(await AddProvider.invalidCountry_errorMsg.getText()).to.equal(addProviderData.invalid_ErrorMsg.countryErrorMsg);
    expect(await AddProvider.invalidzipcode_error.getText()).to.equal(addProviderData.invalid_ErrorMsg.zipcode_error);
    browser.sleep(5000);
    
  });
  //3.Enter the valid data in all fields and click on submit button then verify the success message and verify whether provider added
  When('Enter valid data in all fields and click on submit', async function () {
    await Homepage.addProvider_fromProviderPage.click();
    //Enter valid data in all fields
    await AddProvider.firstName.sendKeys(addProviderData.valid.first_name1);//first name
    await AddProvider.lastName.sendKeys(addProviderData.valid.last_name);//last name
    await AddProvider.nickName.sendKeys(addProviderData.valid.nick_name);//nick name
    //select date of birth
    await AddProvider.DOB.click();
    await AddProvider.month_yearBtn.click();
    await AddProvider.Previos20yearsBtn.click();
    await AddProvider.select_Year.click();
    await AddProvider.select_Month.click();
    await AddProvider.select_Date.click();
    await AddProvider.gender_Female.click();
    //select ethncity
    await AddProvider.ethnicity_dropdown.click();
    await AddProvider.select_ethnicity.click();
    await AddProvider.mobile_num.sendKeys(addProviderData.valid.mobile_number);//mobile number
    await AddProvider.Email.sendKeys(addProviderData.valid.email1);//email
    await AddProvider.address.sendKeys(addProviderData.valid.address);//address
    await AddProvider.city.sendKeys(addProviderData.valid.city);//city
    await AddProvider.state.sendKeys(addProviderData.valid.state);//state
    await AddProvider.country.sendKeys(addProviderData.valid.country);//country
    await AddProvider.zip_Code.sendKeys(addProviderData.valid.zipCode);//zip code
    //select smart phone
    await AddProvider.phoneType_dropdown.click();
    await AddProvider.select_android.click();
    //enter specilization
    await AddProvider.specilization.sendKeys(addProviderData.valid.specilization);
    //enter licence
    await AddProvider.licence.sendKeys(addProviderData.valid.licence);
    //select pharmacy
    await AddProvider.pharmacy_dropdown.click();
    await AddProvider.select_pharmacy.click();
    //upload image
    await AddProvider.upload_ImgBtn.sendKeys(addProviderData.img_path);
    await browser.sleep(10000);
    //click on submit button
    // await AddProvider.submitBtn.click();
    await browser.sleep(15000);
  
  });
  // Then('verify the success message and verify', async function () {
  //  //verify whether page returned to providerlist page and verify the success message
  //  expect(await browser.getTitle()).to.equal(homepage.homepage_title);
  //  await browser.sleep(3000);
  //  //verify succcess message
  //  expect(await AddProvider.success_msg.isDisplayed()).to.equal(true);
  //  expect(await AddProvider.success_msg.getText()).to.equal(addProviderData.succsess_msg);

  // });
  //4.Add provider with valid data in required fields only.
  When('Enter valid data in required fields and click on submit then verify the success message',async function () {
    await Homepage.addProvider_fromProviderPage.click();
    //Enter valid data in all fields
    await AddProvider.firstName.sendKeys(addProviderData.valid.first_name2);//first name
    await AddProvider.lastName.sendKeys(addProviderData.valid.last_name);//last name
    await AddProvider.mobile_num.sendKeys(addProviderData.valid.mobile_number);//mobile number
    await AddProvider.Email.sendKeys(addProviderData.valid.email2);//email
    await AddProvider.address.sendKeys(addProviderData.valid.address);//address
    await AddProvider.zip_Code.sendKeys(addProviderData.valid.zipCode);//zip code
    //enter licence
     await AddProvider.licence.sendKeys(addProviderData.valid.licence);
    //select pharmacy
    await AddProvider.pharmacy_dropdown.click();
    await AddProvider.select_pharmacy.click();
    //click on submit button
    await AddProvider.submitBtn.click();
  //   await browser.sleep(15000);
  //  //verify whether page returned to providerlist page and verify the success message
  //  expect(await browser.getTitle()).to.equal(homepage.homepage_title);
  //  //verify succcess message
  //  expect(await AddProvider.success_msg.isDisplayed()).to.equal(true);
  //  expect(await AddProvider.success_msg.getText()).to.equal(addProviderData.succsess_msg);

   await browser.navigate().refresh();
 
  });

  //5.enter the added provider name and verify whether displaye then click on view patient enter patient name then verify
  When('Enter the provider name which added and click on view patients', async function () {
    browser.sleep(5000);
    await Homepage.search_provider.sendKeys(addProviderData.valid.first_name1);//enter the provider name
    await browser.sleep(3000);
    expect(await AddProvider.provider_card.isDisplayed()).to.equal(true);//verify the card on the page whether displayed 
   

  });
  Then('Enter the patient name and verify whether patient displayed', async function () {

    await AddProvider.viewPatients.click();//click on view patients
    await browser.sleep(5000);
    await AddProvider.searchPatient.sendKeys(addProviderData.patientName);//Enter patient name in search patient
    await browser.sleep(3000);
    //then verify whwther displayed on the page
    expect(await AddProvider.petientCard.isDisplayed()).to.equal(true);
  });

