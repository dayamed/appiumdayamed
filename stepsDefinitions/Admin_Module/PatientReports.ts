import { Then, When } from "cucumber";
import { browser } from "protractor";
import { editProvider } from "../../pageobjects/editProvider";
import { homePage } from "../../pageobjects/homePage";
import { patients } from "../../pageobjects/patients";
import patientData from "../../TestData/patients.json";
import { reports,notesTreatement,location,adherenceReport } from "../../pageobjects/patientReports";
import ReportsData from "../../TestData/patientReports.json";

let Homepage=new homePage();
let EditProvider=new editProvider();
let Patients=new patients();
let Reports=new reports();
let notes=new notesTreatement()
let Location=new location();
let AdherenceReport=new adherenceReport();
let expect =require('chai').expect;

import editProviderData from "../../TestData/editProvider.json";
When('Enter provider and Click on view patient', async function () {
    await Homepage.search_provider.sendKeys(editProviderData.providerName);//enter the provider name
    await EditProvider.viewPatients.click();//click on view patients
    await browser.sleep(5000); 
  
  });
When('select patient and verify the reports page',async function () {
    
    await Patients.patientSearchBox.sendKeys(patientData.patient_name);//enter name
    await browser.sleep(20000);
    await Patients.reportsBtn.click();
    await browser.sleep(20000); 
    //verify the reports page title
    expect(await browser.getTitle()).to.equal(ReportsData.pageTitle);

  });
  Then('click on note and treatement then verify the popup', async function () {
      let name= await Reports.profileName.getText();
     // let adherence=await Reports.adherence_OnProfileCard.getText();
      await Reports.notesTreatement_Card.click();
      await browser.sleep(5000);
      //verify the popup
      expect(await notes.popup.isDisplayed()).to.equal(true);
      //verify the patient name on popup
      expect(await notes.patientName.getText()).to.equal(name);
      //verify the adherence
     // expect(await notes.overallAdherence.getText()).to.equal(adherence);
      await notes.close_icon.click();

  });
  When('I\'m on reward page click on location and verify the popup',async function () {
    await Reports.location_card.click();
    await browser.sleep(5000);
      //verify the popup
      expect(await Location.popup.isDisplayed()).to.equal(true);
      //verify the map view
      expect(await Location.mapView.isDisplayed()).to.equal(true)
      //click on table view and verify
      await Location.tableView_Btn.click();
      await browser.sleep(2000);
      expect(await Location.tableView.isDisplayed()).to.equal(true);
      await Location.close_icon.click();
    
  });
//   When('I\'m on reports click on adherence reports card', function () {
//     // Write code here that turns the phrase above into concrete actions
//     return 'pending';
//   });
//   Then('verify the name and adherence and verify the adherence for current, prevous and future months', function () {
//     // Write code here that turns the phrase above into concrete actions
//     return 'pending';
//   });

