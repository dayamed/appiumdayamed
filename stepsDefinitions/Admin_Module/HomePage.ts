import { Given, When, Then } from "cucumber";
import { browser, by, element} from "protractor";
import { login } from "../../pageobjects/login";
import  loginData  from "../../TestData/loginData.json";
import homepageData from "../../TestData/homePage.json";
import {homePage} from "../../pageobjects/homePage"
import { protractor } from "protractor/built/ptor";
//import forgotPassword from "../../TestData/forgotPasswordPage.json";
let expect =require('chai').expect;
//expect.use(require('chai-smoothie'));
let Login=new login();
let Homepage=new homePage();
var assert = require('chai').assert;

//Enter the Url on browser and navigate to the Dayamed Qa Portal then login with valid admin credendetials
Given('Navigate to the Dayamed QA portal', async function () {
    browser.waitForAngularEnabled(false)
      await browser.manage().window().maximize();
      await browser.get(loginData.URL);
      try{
          await Login.Adv_Btn.click();
          await Login.Proceed_Btn.click();
          browser.sleep(5000);
         }
      catch(error)
         {
          console.log(error);
         }
         //login with Admin credentials
         await Login.userName.sendKeys(loginData.valid_userName);//enter valid username
         await Login.password.sendKeys(loginData.valid_password);//Enter valid password
         await Login.loginButton.click();//click on login button
         await browser.sleep(5000);
         
  });
  //Verify the all fields present on home page
When('I\'m on homepage verify the all fields on page', async function () {
    
    //.Click on menu button and verify the dropdowns one by one 
    await Homepage.menuBtn.click();//click on menu button 
    //1.click on provider dropdown and click on provider list then verify the whether page navigate ti the provider list
    await Homepage.ProviderDropdown.click();//click om provider dropdown
    await  Homepage.provider_List.click();// Click on provider list
    //await browser.sleep(3000);
    //verify the caregiver list page title 
    expect(await browser.getTitle()).to.equal(homepageData.homepage_title);
    await browser.sleep(3000);

    //2.click on Add provider and verify the page title
    //await Homepage.menuBtn.click();//click on menu button
    browser.actions().doubleClick(Homepage.menuBtn).perform();

    await Homepage.ProviderDropdown.click();//click on provider
    await Homepage.Add_Provider.click();//click on add provider
    //verify the add provider page title
    expect(await browser.getTitle()).to.equal(homepageData.AddProvider_TitleName);
    await browser.sleep(3000);

    //3.click on Caregiver list  link and verify whether page is moved to caregiver page 
    browser.actions().doubleClick(Homepage.menuBtn).perform();//await Homepage.menuBtn.click(); //click on menu button
    await Homepage.cargiverDropDown.click();//click on caregiver dropdown
    await Homepage.cargiver_list.click();//click on caregiver list
    //verify the page title
    expect(await browser.getTitle()).to.equal(homepageData.CaregiverListPage_Title);
    await browser.sleep(3000);

    //4.click on add caregiver link and verify whether the page moved to the add care giver page 
    browser.actions().doubleClick(Homepage.menuBtn).perform();//await Homepage.menuBtn.click(); //click on menu button
    await Homepage.cargiverDropDown.click();//click on caregiver dropdown
    await Homepage.AddCaregiver.click();//click on caregiver list
    //verify the page title
    expect(await browser.getTitle()).to.equal(homepageData.AddCaregiver_pageTitle);
    await browser.sleep(3000);

   //5.Click on add Administration and select Pharmacy list then verify whether page navigated to the Pharmacy list page
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.Administration_DropDown.click();//click on Administration
   await Homepage.PharmacyList.click();//click on Pharmacy list
   //Then verify the page title of the pharmacy list
   expect(await browser.getTitle()).to.equal(homepageData.pharmacyList_PageTitle);
   await browser.sleep(3000);

   //6.Click on Relations Management under udministration and verify the page title
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.Administration_DropDown.click();//click on Administration
   await Homepage.RelationsManagement.click();//click on RelationsManagement 
   //verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.RelationsManagement_PageTitle);
   await browser.sleep(3000);

   //7.click on Terms and Conditions which is under Administration And verify the page title
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.Administration_DropDown.click();//click on Administration
   await Homepage.Terms_Conditions.click();//click on terms and conditions
   //Then verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.TermsConditions_PageTitle);
   await browser.sleep(10000);

   //8. click on Reports and sellect Adherence then verify the whether page navigated to the adherence page
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.Reports_DropDown.click();//click on Reports
   await Homepage.Adherence.click();//click on Adherence
   //Then verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.Adherence_PagetTitle);
   await browser.sleep(15000);

   //9. click on change password under account settings and verify the page whether its navigated to the login page
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.AccountSettings_DropDown.click();//click on account settings
   await Homepage.ChangePassword.click();//click on change password
   //Then verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.changePassword_Title);
   await browser.sleep(3000);

   //10. click account settings and select the logout then verify the whether it is navigated to the login page
   browser.actions().doubleClick(Homepage.menuBtn).perform();
   await Homepage.AccountSettings_DropDown.click();//click on account settings
   await Homepage.Logout.click();//click on Logout password
   browser.sleep(3000);
   //Then verify the page title
   expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);

   //9.Again login with admin credentials then click on menu ,Click on Account circle
   // and select the help link then verify whether its navigated to the help page
   await Login.userName.sendKeys(loginData.valid_userName);//enter valid username
   await Login.password.sendKeys(loginData.valid_password);//Enter valid password
   await Login.loginButton.click();//click on login button
   await browser.sleep(5000);
   await Homepage.account_circle.click();//click on account circle
   await Homepage.help.click();//click on help link
   //verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.help_pageTitle);

   //10.select change password from account circle and verify whether page navigated to the changepassword page
   await Homepage.account_circle.click();//click on account circle
   await Homepage.account_ChangePassword.click();//click on change password
   //verify the page title
   expect(await browser.getTitle()).to.equal(homepageData.changePassword_Title);

   //11. // click on account circle and click on Logout verify page navigated to the login page
   await Homepage.account_circle.click();//click on account circle
   await Homepage.account_Logout.click();//click on Logout
   browser.sleep(3000);
   //verify the page title
   expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);
    
  });
// Enter the provider name in search provider and verify the whether provider card displayed
When('enter the provider name and verify the page', async function () {
  browser.sleep(5000);
  await Homepage.search_provider.sendKeys(homepageData.provider_Name);//enter the provider name
  await browser.sleep(3000);
  expect(await Homepage.provider_card.isDisplayed()).to.equal(true);//verify the card on the page whether displayed 

  });
When('click on load more Providers then verify the count',async function () {

  //1. verify the initial providers count on page, it should be 10 of max
  expect(await Homepage.providers_count.count()).to.equal(10);
  
  //click on load more providers button untill all prroviders list displayed then verify the count
  for(let i=2;expect(await Homepage.loadMore_ProvidersBtn.isPresent());i++)
  { 
      await browser.sleep(5000);
      await Homepage.loadMore_ProvidersBtn.click();//click on load more providers button
      await  browser.sleep(5000);
      let max_count=await Homepage.providers_count.count();
      console.log(await max_count);
      await browser.actions().sendKeys(protractor.Key.END).perform();

      let element_status=expect(await Homepage.loadMore_ProvidersBtn.isPresent())
      if(element_status)
      {
      expect(await Homepage.providers_count.count()).to.equal(i*10);
      }

     else
       {
           //verify the final count
           expect(await Homepage.pagination.getText()).to.equal(homepageData.ProvidersMax_Count);
          // expect(await Homepage.pagination.getText()).to.contain.text(homepageData.ProvidersMax_Count);
        }
        
    }
  
  });