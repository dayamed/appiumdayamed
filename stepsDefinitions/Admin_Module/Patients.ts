
import { Given, Then, When } from "cucumber";
import { browser} from "protractor";
import { login } from "../../pageobjects/login";
import {homePage} from "../../pageobjects/homePage";
import editProviderData from "../../TestData/editProvider.json";
import patientData from "../../TestData/patients.json";
import {addProvider} from "../../pageobjects/addProvider";
import {editProvider} from "../../pageobjects/editProvider";
import homepage from "../../TestData/homePage.json";
import  {addPhysician, patients} from "../../pageobjects/patients";
import { protractor } from "protractor/built/ptor";
let expect =require('chai').expect;
let Login=new login();
let Homepage=new homePage();
let AddProvider=new addProvider();
let EditProvider=new editProvider();
let Patients=new patients();
let AddPhysician=new addPhysician();

When('Click on view patient under any provider', async function () {
  await Homepage.search_provider.sendKeys(editProviderData.providerName);//enter the provider name
  await EditProvider.viewPatients.click();//click on view patients
  await browser.sleep(5000);  
  });
Then('verify the view and lazy loading', async function () {

//1.verify patients page view
   expect(await browser.getTitle()).to.equal(patientData.patientpage_title);
  //2.verify the list view, it should be displayed in list view
   expect(await Patients.patient_listview.isPresent()).to.equal(true);
   //3.click on grid view and verify the view
   await Patients.gridView_Btn.click();
   await browser.sleep(5000);
   expect(await Patients.patient_listview.isPresent()).to.equal(false);
   await Patients.listView_Btn.click();
   await browser.sleep(2000);

  //verify the lazy loading
  //1. verify the initial patients count on page, it should be 10 of max
  expect(await Patients.patientsCount.count()).to.equal(10);
  
  //click on load more patients button untill all prroviders list displayed then verify the count
  // for(let i=2;expect(await Patients.loadMore_PatientBtn.isPresent());i++)
  // { 
  //     await browser.sleep(5000);
  //     await Patients.loadMore_PatientBtn.click();//click on load more patient button
  //     await  browser.sleep(5000);
  //     let max_count=await Patients.patientsCount.count();
  //     console.log(await max_count);
  //     await browser.actions().sendKeys(protractor.Key.END).perform();

  //     let element_status=expect(await Patients.loadMore_PatientBtn.isPresent())
  //     if(element_status)
  //     {
  //     expect(await Patients.patientsCount.count()).to.equal(i*10);
  //     }

  //    else
  //      {
  //          //verify the final count
  //          expect(await Patients.pagination.getText()).to.equal(patientData.max_count);
  //         // expect(await Homepage.pagination.getText()).to.contain.text(homepageData.ProvidersMax_Count);
  //       }
        
  //   }
  
  });
  When('Enter patient name then verify whether it is displayed and click on delete button', async function () {
    //1.Enter patient name and verify whether it is displayed
    await Patients.patientSearchBox.sendKeys(patientData.patient_name);//enter name
    await browser.sleep(20000);
    expect(await Patients.patientDetails.getText()).to.equal(patientData.fullName);//verify the name displayed
    //2.Then click on delete button
    await Patients.delete_icon.click();
  });
  Then('verify the popup',async function () {
    await browser.sleep(3000);
    expect(await Patients.delete_popup.isDisplayed()).to.equal(true);//verify whether popup is displayed
    expect(await Patients.question_onDeletepopup.getText()).to.equal(patientData.question_ondeletePopup);//verify the question
    //3.click on cancel button then vrify whether it closed
    await Patients.cancelBtn_onDeletepopup.click();
    await browser.sleep(3000);
    expect(await Patients.delete_popup.isPresent()).to.equal(false);

  });
//Edit provider validations
//1.Edit provider with blank data in required fields then verify the error messages
When('I\'m on edit patient page verify the disabled fields then leave the all required fields as empty and click on submit',async function () {
 //clear the data in required fields
 await Patients.edit_icon.click();
 await browser.sleep(5000);
 await Patients.firstName.clear();
 await Patients.lastName.clear();
 await Patients.mobile_num.clear();
 await Patients.emergencyContact_num.clear();
 await Patients.emergencyContact_name.clear();
 await Patients.health_planID.clear();
 await Patients.insuranceAgency.clear();
 await Patients.address.clear();
 await Patients.zipCode.clear();
 await Patients.diagnosis.clear();
 //Click on submit button
 await Patients.submitBtn.click();


});
//then verify the error messages
Then('verify the error messages for required fields on edit patient page',async function () {
  
  expect(await Patients.fname_ErrorMsg.getText()).to.equal(patientData.error_Msg.fname_error);
  expect(await Patients.lname_errorMsg.getText()).to.equal(patientData.error_Msg.lname_error);
  expect(await Patients.mobileNum_errorMsg.getText()).to.equal(patientData.error_Msg.mobileNum_error);
  expect(await Patients.ermergency_contactNumErr.getText()).to.equal(patientData.error_Msg.ermergency_contactNumErr);
  expect(await Patients.emergency_contactNameErr.getText()).to.equal(patientData.error_Msg.emergency_contactNameErr);
  expect(await Patients.health_planIDErr.getText()).to.equal(patientData.error_Msg.health_planIDErr);
  expect(await Patients.insuranceAgency_ErrMsg.getText()).to.equal(patientData.error_Msg.insuranceAgency_ErrMsg);
  expect(await Patients.address_errorMsg.getText()).to.equal(patientData.error_Msg.address_error);
  expect(await Patients.zipcode_error.getText()).to.equal(patientData.error_Msg.zipcode_error);
  expect(await Patients.diagnosis_ErrMsg.getText()).to.equal(patientData.error_Msg.diagnosis_ErrMsg);
  await browser.navigate().refresh();
  await browser.sleep(7000);
});
//2.Edit the all fields with invalid data then verify the error messages
When('I\'m on edit patient page enter the invalid data in all fields and click on submit button', async function () {
  //edit fields with invalid data
  await Patients.firstName.click();
 await Patients.firstName.clear();
 await Patients.firstName.sendKeys(patientData.invalid.first_name);
 await Patients.lastName.click();
 await Patients.lastName.clear();
 await Patients.lastName.sendKeys(patientData.invalid.last_name);
 await Patients.nickName.clear();
 await Patients.nickName.sendKeys(patientData.invalid.nick_name);
 await Patients.mobile_num.clear();
 await Patients.mobile_num.sendKeys(patientData.invalid.mobile_number);
 await Patients.emergencyContact_num.clear();
 await Patients.emergencyContact_num.sendKeys(patientData.invalid.emergency_ContactNum);
 await Patients.emergencyContact_name.clear();
 await Patients.emergencyContact_name.sendKeys(patientData.invalid.emergency_ContactName);
 await Patients.health_planID.clear();
 await Patients.health_planID.sendKeys(patientData.invalid.health_planID);
 await Patients.insuranceAgency.clear();
 await Patients.insuranceAgency.sendKeys(patientData.invalid.insuranceAgency);
 await Patients.medicare_PlanID.clear();
 await Patients.medicare_PlanID.sendKeys(patientData.invalid.medicare_planId);
 await Patients.address.clear();
 await Patients.address.sendKeys(patientData.invalid.address);
 await Patients.city.clear();
 await Patients.city.sendKeys(patientData.invalid.city);
 await Patients.state.clear();
 await Patients.state.sendKeys(patientData.invalid.state);
 await Patients.zipCode.clear();
 await Patients.zipCode.sendKeys(patientData.invalid.zipCode);
 await Patients.diagnosis.clear();
 await Patients.diagnosis.sendKeys(patientData.invalid.dignosis);
 //Click on submit button
 await Patients.submitBtn.click();

});
//then verify the error messages
Then('verify the error messages on edit patient page',async function () {
  expect(await Patients.invalidfname_ErrorMsg.getText()).to.equal(patientData.invalid_ErrorMsg.fname);
  expect(await Patients.invalidlname_errorMsg.getText()).to.equal(patientData.invalid_ErrorMsg.lname);
  expect(await Patients.invalidnickName_error.getText()).to.equal(patientData.invalid_ErrorMsg.nickName);
  expect(await Patients.invalidmobileNum_errorMsg.getText()).to.equal(patientData.invalid_ErrorMsg.mobileNum_error);
  expect(await Patients.invalidermergency_contactNumErr.getText()).to.equal(patientData.invalid_ErrorMsg.emergency_contactNum);
  expect(await Patients.invalidemergency_contactNameErr.getText()).to.equal(patientData.invalid_ErrorMsg.emergency_contactName);
  expect(await Patients.invalidhealth_planIDErr.getText()).to.equal(patientData.invalid_ErrorMsg.health_PlanID);
  expect(await Patients.invalidinsuranceAgency_ErrMsg.getText()).to.equal(patientData.invalid_ErrorMsg.insuranceAgency);
  expect(await Patients.invalidMedicarePlanID.getText()).to.equal(patientData.invalid_ErrorMsg.medicare_planId);
  expect(await Patients.invalidaddress_errorMsg.getText()).to.equal(patientData.invalid_ErrorMsg.address_error);
  expect(await Patients.invalidcity_error.getText()).to.equal(patientData.invalid_ErrorMsg.city);
  expect(await Patients.invalidState_errorMsg.getText()).to.equal(patientData.invalid_ErrorMsg.State);
  expect(await Patients.invalidzipcode_error.getText()).to.equal(patientData.invalid_ErrorMsg.zipcode_error);
  expect(await Patients.invaliddiagnosis_ErrMsg.getText()).to.equal(patientData.invalid_ErrorMsg.diagnosis);
  await browser.navigate().refresh();
  await browser.sleep(7000);
});
//3.edit the all the fields with valid data and click on submit then verify the success message
When('I\'m on edit patient page Enter valid data in all fields and click on submit',async function () {
 //edit the fields with valid data
 await Patients.firstName.click();
 await Patients.firstName.clear();
 await Patients.firstName.sendKeys(patientData.valid_info1.fname);
 await Patients.lastName.click();
 await Patients.lastName.clear();
 await Patients.lastName.sendKeys(patientData.valid_info1.lname);
 await Patients.nickName.clear();
 await Patients.nickName.sendKeys(patientData.valid_info1.nickName);

 //edit DOB
 await Patients.DOB.click();
 await Patients.select_Date4.click();
 await Patients.gender_Male.click();
 await Patients.medication_dropdown.click();
 await Patients._7dayPill.click();
 await Patients.contryCode_dropdown.click();
 await browser.sleep(2000);
 await Patients.IND_code.click();
 await Patients.mobile_num.clear();
 await Patients.mobile_num.sendKeys(patientData.valid_info1.mobileNum);
 await Patients.emergencyContact_num.clear();
 await Patients.emergencyContact_num.sendKeys(patientData.valid_info1.emergency_contactNum);
 await Patients.emergencyContact_name.clear();
 await Patients.emergencyContact_name.sendKeys(patientData.valid_info1.emergency_contactName);
 await browser.sleep(5000);
 await Patients.contact_relation.click();
 await Patients.option_father.click();//FATHER
 await Patients.health_planID.clear();
 await Patients.health_planID.sendKeys(patientData.valid_info1.healthPlanId);
 await Patients.insuranceAgency.clear();
 await Patients.insuranceAgency.sendKeys(patientData.valid_info1.insuranceAgency);
 await Patients.medicare_PlanID.clear();
 await Patients.medicare_PlanID.sendKeys(patientData.valid_info1.medicare_planId);
 await Patients.address.clear();
 await Patients.address.sendKeys(patientData.valid_info1.address);
 await Patients.city.clear();
 await Patients.city.sendKeys(patientData.valid_info1.city);
 await Patients.state.clear();
 await Patients.state.sendKeys(patientData.valid_info1.State);
 await Patients.zipCode.clear();
 await Patients.zipCode.sendKeys(patientData.valid_info1.zipcode);
 await Patients.diagnosis.clear();
 await Patients.diagnosis.sendKeys(patientData.valid_info1.diagnosis);
 //smart phone type
 await Patients.phoneType_dropdown.click();
 await Patients.select_ios.click();
 await Patients.upload_ImgBtn.sendKeys(patientData.img_path);
// await Patients.email_notificationSwitch.click();
 //await Patients.text_notificationSwitch.click();
 //Click on submit button
 await Patients.submitBtn.click();
 await browser.sleep(8000);


});
// Then('verify the success message after sumbit patient details',async function () {
//  // Page should returned to patient list page and success message should displayed
//  expect(await browser.getTitle()).to.equal(patientData.patientpage_title);
//  //verify the success message
//  expect(await Patients.successMsg.isDisplayed()).to.equal(true);
//  expect(await Patients.successMsg.getText()).to.equal(patientData.succsess_msg);

//});
When('I\'m on edit patient page Enter valid data in required fields and click on submit then verify the success message',async function () {
  await Patients.patientSearchBox.sendKeys(patientData.patient_name);//enter name
  await browser.sleep(20000);
  await Patients.edit_icon.click();
  await browser.sleep(5000);
  await Patients.firstName.click();
  await Patients.firstName.clear();
  await Patients.firstName.sendKeys(patientData.valid_info2.fname);
  await Patients.lastName.click();
  await Patients.lastName.clear();
  await Patients.lastName.sendKeys(patientData.valid_info2.lname);
  await browser.sleep(2000);
  await Patients.DOB.click();
  await browser.sleep(2000);
  await Patients.select_Date5.click();
  await Patients.contryCode_dropdown.click();
  await Patients.USA_code.click();
  await Patients.mobile_num.clear();
  await Patients.mobile_num.sendKeys(patientData.valid_info2.mobileNum);
  await Patients.emergencyContact_num.clear();
  await Patients.emergencyContact_num.sendKeys(patientData.valid_info2.emergency_contactNum);
  await Patients.emergencyContact_name.clear();
  await Patients.emergencyContact_name.sendKeys(patientData.valid_info2.emergency_contactName);
  await Patients.health_planID.clear();
  await Patients.health_planID.sendKeys(patientData.valid_info2.healthPlanId);
  await Patients.insuranceAgency.clear();
  await Patients.insuranceAgency.sendKeys(patientData.valid_info2.insuranceAgency);
  await Patients.address.clear();
  await Patients.address.sendKeys(patientData.valid_info2.address);
  await Patients.zipCode.clear();
  await Patients.zipCode.sendKeys(patientData.valid_info2.zipcode);
  await Patients.diagnosis.clear();
  await Patients.diagnosis.sendKeys(patientData.valid_info2.diagnosis);
  await Patients.submitBtn.click();

});
//1.add physician with blank data
Then('I\'m on edit patient page click on add physician and click on submit without entering any data', async function () {
  await Patients.patientSearchBox.sendKeys(patientData.patient_name);//enter name
  await browser.sleep(20000);
  await Patients.edit_icon.click();
  await browser.sleep(5000);
  //leave fields as empty then click on submit button
  await AddPhysician.add_physicianBtn.click();
  await browser.sleep(2000);
  await browser.actions().sendKeys(protractor.Key.END).perform();
  await AddPhysician.submitBtn.click();
});
Then('verify the error messages on add physician page',async function () {
  expect(await AddPhysician.fname_ErrorMsg.getText()).to.equal(patientData.addPhysician.error_Msg.fname_error);
  expect(await AddPhysician.lname_errorMsg.getText()).to.equal(patientData.addPhysician.error_Msg.lname_error);
  expect(await AddPhysician.mobileNum_errorMsg.getText()).to.equal(patientData.addPhysician.error_Msg.mobileNum_error);
  expect(await AddPhysician.email_errorMsg.getText()).to.equal(patientData.addPhysician.error_Msg.Email_Error);
  expect(await AddPhysician.address_errorMsg.getText()).to.equal(patientData.addPhysician.error_Msg.address_error);
  expect(await AddPhysician.zipCode_error.getText()).to.equal(patientData.addPhysician.error_Msg.zipcode_error);
  await AddPhysician.cancelBtn.click();
});
//2.Enter invalid data in all fields then verify the error messages

When('I\'m on edit patient page click on add physician, enter invalid data in all fields then verify the error messages',async function () {
//Enter invalid data in all
 await AddPhysician.add_physicianBtn.click();
 await browser.sleep(2000);
 await AddPhysician.firstName.sendKeys(patientData.addPhysician.invalid.firstName);
 await AddPhysician.lastName.sendKeys(patientData.addPhysician.invalid.lastName);
 await AddPhysician.nickName.sendKeys(patientData.addPhysician.invalid.nickName);
 await AddPhysician.npi_Num.sendKeys(patientData.addPhysician.invalid.NPI_num);
 await AddPhysician.dea_Num.sendKeys(patientData.addPhysician.invalid.DEA_Num);
 await AddPhysician.mobile_num.sendKeys(patientData.addPhysician.invalid.mobile_num);
 await AddPhysician.Email.sendKeys(patientData.addPhysician.invalid.email);
 await AddPhysician.address.sendKeys(patientData.addPhysician.invalid.address);
 await AddPhysician.city.sendKeys(patientData.addPhysician.invalid.city);
 await AddPhysician.state.sendKeys(patientData.addPhysician.invalid.state);
 await AddPhysician.country.sendKeys(patientData.addPhysician.invalid.country);
 await AddPhysician.zipCode.sendKeys(patientData.addPhysician.invalid.zipcode);
 await browser.actions().sendKeys(protractor.Key.END).perform();
  await AddPhysician.submitBtn.click();
 //verify the all error messages:
 expect(await AddPhysician.invalidfname_ErrorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.fname);
 expect(await AddPhysician.invalidlname_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.lname);
 expect(await AddPhysician.invalidnickName_error.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.nickName);
 expect(await AddPhysician.invalidmobileNum_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.mobileNum);
 expect(await AddPhysician.invalidEmail_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.Email);
 expect(await AddPhysician.invalidaddress_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.address);
 expect(await AddPhysician.invalidcity_error.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.city);
 expect(await AddPhysician.invalidState_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.state);
 expect(await AddPhysician.invalidCoutry_errorMsg.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.country);
 expect(await AddPhysician.invalidzipcode_error.getText()).to.equal(patientData.addPhysician.invalid_ErrMsg.zipcode);
 await AddPhysician.cancelBtn.click();
 
});
When('I\'m on add physician page enter the valid data in all the fields and click on submit then verify the success message',async function () {
  await AddPhysician.add_physicianBtn.click();
  await browser.sleep(2000);
  await AddPhysician.firstName.sendKeys(patientData.addPhysician.valid_info1.firstName);
  await AddPhysician.lastName.sendKeys(patientData.addPhysician.valid_info1.lastName);
  await AddPhysician.nickName.sendKeys(patientData.addPhysician.valid_info1.nickName);
  await AddPhysician.npi_Num.sendKeys(patientData.addPhysician.valid_info1.NPI_num);
  await AddPhysician.gender_Female.click();
  await AddPhysician.dea_Num.sendKeys(patientData.addPhysician.valid_info1.DEA_Num);
  await AddPhysician.mobile_num.sendKeys(patientData.addPhysician.valid_info1.mobile_num);
  await AddPhysician.Email.sendKeys(patientData.addPhysician.valid_info1.email);
  await AddPhysician.address.sendKeys(patientData.addPhysician.valid_info1.address);
  await AddPhysician.city.sendKeys(patientData.addPhysician.valid_info1.city);
  await AddPhysician.state.sendKeys(patientData.addPhysician.valid_info1.state);
  await AddPhysician.country.sendKeys(patientData.addPhysician.valid_info1.country);
  await AddPhysician.zipCode.sendKeys(patientData.addPhysician.valid_info1.zipcode);
  await AddPhysician.phoneType_dropdown.click();
  await AddPhysician.select_android.click();
  await browser.sleep(2000);
  await browser.actions().sendKeys(protractor.Key.END).perform();
  await AddPhysician.email_notificationSwitch.click();//enabled
  await AddPhysician.text_notificationSwitch.click();//enabled
  await AddPhysician.submitBtn.click();

  //verify the success message:
   await browser.sleep(5000);
   expect(await AddPhysician.success_msg.isDisplayed()).to.equal(true);

});

When('I\'m on add physician page enter the valid data in required fields and click on submit then verify the success message',async function () {
//enter the valid data in required fields
  await AddPhysician.add_physicianBtn.click();
  await browser.sleep(2000);
  await AddPhysician.firstName.sendKeys(patientData.addPhysician.valid_info2.firstName);
  await AddPhysician.lastName.sendKeys(patientData.addPhysician.valid_info2.lastName);
  await AddPhysician.mobile_num.sendKeys(patientData.addPhysician.valid_info2.mobile_num);
  await AddPhysician.Email.sendKeys(patientData.addPhysician.valid_info2.email);
  await AddPhysician.address.sendKeys(patientData.addPhysician.valid_info2.address);
  await AddPhysician.zipCode.sendKeys(patientData.addPhysician.valid_info2.zipcode);
  await browser.actions().sendKeys(protractor.Key.END).perform();
  await AddPhysician.submitBtn.click();//click on submit

   //verify the success message:
   await browser.sleep(5000);
   expect(await AddPhysician.success_msg.isDisplayed()).to.equal(true);

});