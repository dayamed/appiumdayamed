import { Given, When, Then } from "cucumber";
import { browser} from "protractor";
import { login } from "../../pageobjects/login";
import  loginData  from "../../TestData/loginData.json";
import homepage from "../../TestData/homePage.json";
import forgotPassword from "../../TestData/forgotPasswordPage.json";
let expect =require('chai').expect;

let Login=new login();

Given('I navigate to the Dayamed QA portal', async function() {
  browser.waitForAngularEnabled(false)
      await browser.manage().window().maximize();
      await browser.get(loginData.URL);
      try{
          await Login.Adv_Btn.click();
          await Login.Proceed_Btn.click();
          browser.sleep(5000);
         }
      catch(error)
         {
          console.log(error);
         }
    
  });
  //verify the all fileds present on the page:
  When('I\'m on login page verify the fileds',async function () {
   // browser.waitForAngularEnabled(true);
   browser.sleep(5000);
   //verify the login page title
    expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);
   // verify the all fields whethr they are present aaaaare not 
    expect(await Login.heading.isPresent()).to.equal(true);//heading
    expect(await Login.logo.isPresent()).to.equal(true);//Dayamed Logo
    expect(await Login.userName.isPresent()).to.equal(true);//user name Text field
    expect(await Login.password.isPresent()).to.equal(true);//password text field
    expect(await Login.forgot_password.isPresent()).to.equal(true);//forgot password link
    expect(await Login.visibiltyBtn.isPresent()).to.equal(true);//password visibility Icon
    expect(await Login.loginButton.isPresent()).to.equal(true);//login button
  });
  //login with invaluid credentials:

  Then('enter the invalid credentials,click on login and verify the error messages', async function () {
   
    //1.leave the username and password as empty and click on login then verify the error messages:
    await Login.loginButton.click();//click on login button
    browser.sleep(5000);
    expect(await Login.username_error_message.getText()).to.equal(loginData.Username_Error_Message);//verify the error message under username field
    expect(await Login.password_error_message.getText()).to.equal(loginData.password_Error_Message);//verify the error ,essage under the password field
    await browser.navigate().refresh();
    //2.enter valid username and leave the password filed as empty then click on login
    await Login.userName.sendKeys(loginData.valid_userName);//enter only password
    await Login.loginButton.click();//click on login button
    browser.sleep(5000);
    expect(await Login.password_error_message.getText()).to.equal(loginData.password_Error_Message);//verify the error ,essage under the password field
    await browser.navigate().refresh();

    //3.login by entering the passord and leave the username as empty
    await Login.password.sendKeys(loginData.valid_password);//enter password
    await Login.loginButton.click();//click on login
    browser.sleep(5000);
    expect(await Login.username_error_message.getText()).to.equal(loginData.Username_Error_Message);//verify the error message under username field
    await browser.navigate().refresh();

    //4.login with the username wich is not exist and invalid password then verify the error message:
     await Login.userName.sendKeys(loginData.notExist_userName);//enter invalid username
     await Login.password.sendKeys(loginData.invalid_password);//enter invalid password
     await Login.loginButton.click();// click on login button
     browser.sleep(2000);
     expect(await Login.error_msg_bothinvalid.getText()).to.equal(loginData.error_Msg_invalidData);//verify the error message.
     expect(await Login.toast_mesg_forInvalidData.getText()).to.equal(loginData.toastMesg_forInvalidData);//verify the toast message
     await browser.navigate().refresh();

    //5.Login with valid username and invalid password
     await Login.userName.sendKeys(loginData.valid_userName);//valid username
     await Login.password.sendKeys(loginData.notExist_userName);//invalid password
     await Login.loginButton.click();
     browser.sleep(2000);
     expect(await Login.error_msg_bothinvalid.getText()).to.equal(loginData.error_Msg_invalidData);//verify the error message.
     expect(await Login.toast_mesg_forInvalidData.getText()).to.equal(loginData.toastMesg_forInvalidData);//verify the toast message
     await browser.navigate().refresh();

    //6.Login with invalid username and valid password
    await Login.userName.sendKeys(loginData.notExist_userName);//invalid username
    await Login.password.sendKeys(loginData.valid_password);//valid password
    await Login.loginButton.click();
    browser.sleep(2000);
    expect(await Login.error_msg_bothinvalid.getText()).to.equal(loginData.error_Msg_invalidData);//verify the error message.
  
    expect(await Login.toast_mesg_forInvalidData.getText()).to.equal(loginData.toastMesg_forInvalidData);//verify the toast message
    await browser.navigate().refresh();

    //7.Enter invalid username and navigate to the other field then verify the error message under username filedcd 
    await Login.userName.sendKeys(loginData.invalid_userName);//Enter invalid username
    await Login.password.click()//click on the password
    browser.sleep(2000);
    expect(await Login.invalid_username_errorMsg.getText()).to.equal(loginData.invalid_UserName_Error);//verify the error message.
  
  });

  //login with valid credentials:

  Then('enter valid credentials and click on login then verify the homepage', async function () {
    await Login.userName.sendKeys(loginData.valid_userName);//enter valid username
    await Login.password.sendKeys(loginData.valid_password);//Enter valid password
    await Login.loginButton.click();//click on login button
    //page should navigate to the home page, verify the admin home page
    browser.sleep(5000);
    expect(await browser.getTitle()).to.equal(homepage.homepage_title);//verify the home page title
    
  });
  // verify the functionality of the password visibilty icon
  When('enter the password and verify the password visibily icon functionality.',async function () {
    await Login.password.sendKeys(loginData.invalid_password);//Enter password
    expect(await Login.password.getAttribute('type')).to.equal('password')//verify password,it should be invisible
    await Login.visibiltyBtn.click();//click on icon
    expect(await Login.password.getAttribute('type')).to.equal('text')//verify password,it should be visible
  });
  //verify the forgot password link
  When('Click on forgot password link and verify the page title.', async function () {
    await Login.forgot_password.click();// click on forgot password
    expect(await browser.getTitle()).to.equal(forgotPassword.forgotPassword_PageTitle);
  });
  
  
  

