import { Given, When, Then } from "cucumber";
import { browser} from "protractor";
import { login } from "../../pageobjects/login";
import  loginData  from "../../TestData/loginData.json";
import {Forgot_Password} from "../../pageobjects/Forgot_Password"
import forgotPassword from "../../TestData/forgotPasswordPage.json";
let expect =require('chai').expect;

let Login=new login();
let forgotpwd=new Forgot_Password();
//Enter Url on browser and navigate to the Dayamed QA web portal 
 Given('I navigate to the Dayamed QA web portal', async function () {
  browser.waitForAngularEnabled(false)
  await browser.manage().window().maximize();
  await browser.get(loginData.URL);
  try{
      await Login.Adv_Btn.click();
      await Login.Proceed_Btn.click();
      browser.sleep(5000);
     }
  catch(error)
     {
      console.log(error);
     }
    
  });
  // verify the page title and fields present on the page
  When('verify the all fields on the page',async  function () {
    await Login.forgot_password.click();// click on forgot password to navigate to the forgaot password page
    expect(await browser.getTitle()).to.equal(forgotPassword.forgotPassword_PageTitle);//verify the page title
    expect(await forgotpwd.forgotpassword_PageHeading.isPresent()).to.equal(true);//hesding
    expect(await forgotpwd.logo.isPresent()).to.equal(true);//Logo
    expect(await forgotpwd.Email_TextField.isPresent()).to.equal(true);//Email Text field
    expect(await forgotpwd.submitBtn.isPresent()).to.equal(true);//sumbit button 
    expect(await forgotpwd.Goback_toLogin.isPresent()).to.equal(true);//Go back to Login Link
    // Click on "Go back to Login Link" and verify whether it returned to Login page 
    await forgotpwd.Goback_toLogin.click();//click on "Go back to Login Link"
    expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);//verify the page title
    //return to the forgot password page again
    // await browser.navigate().forward();// 
    await Login.forgot_password.click();
    await browser.sleep(5000);
    
  });
  // Try to click on submit  with Invalid Email
  Then('Enter the invalid Email and click on submit then verify the error messages', async function () {

     //1.Leave the Email fiels as empty and click on sumbit
     await forgotpwd.submitBtn.click();//click on submit button
     browser.sleep(5000);
     expect(await forgotpwd.email_ErrorMessage.isDisplayed()).to.equal(true);//verify whether error message displayed
     expect(await forgotpwd.email_ErrorMessage.getText()).to.equal(forgotPassword.Email_ErrorMessage);//verify the error message 
     await browser.navigate().refresh();// page refresh

    //2.Enter Invalid username and verify the error message
    await forgotpwd.Email_TextField.sendKeys(loginData.invalid_userName);//Enter the Email
    await forgotpwd.submitBtn.click();//Click on submit button
    expect(await forgotpwd.invalid_ErrorMessage.isDisplayed()).to.equal(true);//verify whether message displayed
    expect(await forgotpwd.invalid_ErrorMessage.getText()).to.equal(forgotPassword.Invalid_ErrorMessage);//verify the message
    await browser.navigate().refresh();

    //3.Enter the Email which does not exixt and click on submit then verify the toast message
    await forgotpwd.Email_TextField.sendKeys(loginData.notExist_userName);//Enter the Email which not exist
    await forgotpwd.submitBtn.click();//click on submit button
    browser.sleep(2000);
    expect(await forgotpwd.notExist_ToastMessage.isDisplayed()).to.equal(true);//verify whether toast message displayed
    expect(await forgotpwd.notExist_ToastMessage.getText()).to.equal(forgotPassword.NotExist_ToastMessage);//verify the toast message

  });
  // click on submit  with valid Email and verify the Success toast Message
  // Then('Enter the valid Email and click on submit then verify the the success message', async function () {
  //   await forgotpwd.Email_TextField.sendKeys(loginData.valid_userName);//enter valid Email
  //   await forgotpwd.submitBtn.click();// Click on Submit Button
  //   //page should returned to login page and diaplayed the message
  //   expect(await browser.getTitle()).to.equal(loginData.LoginPage_Title);//verify the page title
  //   expect(await forgotpwd.success_ToastMessage.isDisplayed()).to.equal(true);//verify whether Success toast Message diaplayed
  //   expect(await forgotpwd.success_ToastMessage.getText()).to.equal(forgotPassword.Success_Message);//verify the message
  

  // });