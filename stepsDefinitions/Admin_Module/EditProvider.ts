import { Given, Then, When } from "cucumber";
import { browser} from "protractor";
import { login } from "../../pageobjects/login";
import {homePage} from "../../pageobjects/homePage";
import editProviderData from "../../TestData/editProvider.json";
import {addProvider} from "../../pageobjects/addProvider";
import {editProvider} from "../../pageobjects/editProvider";
import homepage from "../../TestData/homePage.json";
let expect =require('chai').expect;
let Login=new login();
let Homepage=new homePage();
let AddProvider=new addProvider();
let EditProvider=new editProvider();
//1.leave the required fields as empty and click on sumbit then verify the error messages
When('I\'m on edit provider page verify the disabled fields then leave the all required fields as empty and click on submit', async function () {
  
  //enter provider and click on edit icon
  await Homepage.search_provider.sendKeys(editProviderData.providerName);//enter the provider name
  await browser.sleep(5000);
  await EditProvider.edit_icon.click();
  await browser.sleep(2000);
  //then verify the disabled fields
// expect(await EditProvider.Email.isEnabled()).to.equal(false);//email field should be desabled
expect(await EditProvider.Email.getAttribute('disabled')).to.equal('true');

//expect(await EditProvider.pharmacy_dropdown.getAttribute('disabled')).to.equal('true');
expect(await EditProvider.pharmacy_dropdown.isEnabled()).to.equal(false);//pharmacy dropdown should be disabled
  //clear the data
  await EditProvider.firstName.clear();
  await EditProvider.lastName.clear();
  await EditProvider.mobile_num.clear();
  await EditProvider.address.clear();
  await EditProvider.zip_Code.clear();
  await EditProvider.licence.clear();
  await EditProvider.submitBtn.click();
});
// verify the error messages for rquired fields
Then('verify the error messages for which we leave as empty', async function () {
    expect(await EditProvider.fname_ErrorMsg.getText()).to.equal(editProviderData.error_Msg.fname_error);
    expect(await EditProvider.lname_errorMsg.getText()).to.equal(editProviderData.error_Msg.lname_error);
    expect(await EditProvider.mobileNum_errorMsg.getText()).to.equal(editProviderData.error_Msg.mobileNum_error);
    expect(await EditProvider.address_errorMsg.getText()).to.equal(editProviderData.error_Msg.address_error);
    expect(await EditProvider.zipcode_error.getText()).to.equal(editProviderData.error_Msg.zipcode_error);
    expect(await EditProvider.licence_error.getText()).to.equal(editProviderData.error_Msg.licence_error);
    await browser.navigate().refresh();

  });
When('I\'m on edit provider page enter the invalid data in all fields and click on submit button',async function () {
  await browser.sleep(5000);
  await EditProvider.firstName.clear();
  await EditProvider.firstName.sendKeys(editProviderData.invalid.first_name);//first name
  await EditProvider.lastName.clear();
  await EditProvider.lastName.sendKeys(editProviderData.invalid.last_name);//last name
  await EditProvider.nickName.clear();
  await EditProvider.nickName.sendKeys(editProviderData.invalid.nick_name);//nick name
  await EditProvider.mobile_num.clear();
  await EditProvider.mobile_num.sendKeys(editProviderData.invalid.mobile_number);//mobile number
  await EditProvider.address.clear();
  await EditProvider.address.sendKeys(editProviderData.invalid.address);//address
  await EditProvider.city.clear();
  await EditProvider.city.sendKeys(editProviderData.invalid.city);//city
  await EditProvider.state.clear();
  await EditProvider.state.sendKeys(editProviderData.invalid.state);//state
  await EditProvider.country.clear();
  await EditProvider.country.sendKeys(editProviderData.invalid.country);//country
  await EditProvider.zip_Code.clear();
  await EditProvider.zip_Code.sendKeys(editProviderData.invalid.zipCode);//zip code
  await EditProvider.specilization.clear();
  await EditProvider.specilization.sendKeys(editProviderData.invalid.specilization);//spectilization
  await EditProvider.licence.clear();
  await EditProvider.licence.sendKeys(editProviderData.invalid.licence);//licence
  //And click on submit button
  await AddProvider.submitBtn.click();
    
  });
  Then('verify the error messages on edit page', async function () {
    expect(await EditProvider.invalidfname_ErrorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.fname_error);
    expect(await EditProvider.invalidlname_errorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.lname_error);
    expect(await EditProvider.invalidnickName_error.getText()).to.equal(editProviderData.invalid_ErrorMsg.nickName_errorMsg);
    expect(await EditProvider.invalidmobileNum_errorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.mobileNum_error);
    expect(await EditProvider.invalidaddress_errorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.address_error);
    expect(await EditProvider.invalidcity_error.getText()).to.equal(editProviderData.invalid_ErrorMsg.city_errorMsg);
    expect(await EditProvider.invalidState_errorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.StateError_msg);
    expect(await EditProvider.invalidCountry_errorMsg.getText()).to.equal(editProviderData.invalid_ErrorMsg.countryErrorMsg);
    expect(await EditProvider.invalidzipcode_error.getText()).to.equal(editProviderData.invalid_ErrorMsg.zipcode_error);
    await browser.navigate().refresh();
  });
//3.edit details with valid data in all fields
When('I\'m on edit provider page Enter valid data in all fields and click on submit', async function () {
   //edit the fileds with valid data in all fields
   await EditProvider.firstName.clear();
   await EditProvider.firstName.sendKeys(editProviderData.valid.first_name);//first name
   await EditProvider.lastName.clear();
   await EditProvider.lastName.sendKeys(editProviderData.valid.last_name);//last name
   await EditProvider.nickName.clear();
   await EditProvider.nickName.sendKeys(editProviderData.valid.nick_name);//nick name
   //select date of birth
   await EditProvider.DOB.click();
  //  await EditProvider.month_yearBtn.click();
  //  await EditProvider.Previos20yearsBtn.click();
  //  await EditProvider.select_Year.click();
  //  await EditProvider.select_Month.click();
   await EditProvider.select_Date.click();
   await EditProvider.gender_Female.click();
  
   await EditProvider.ethnicity_dropdown.click();
   await EditProvider.select_ethnicity.click(); //select ethncity
   await EditProvider.mobile_num.clear();
   await EditProvider.mobile_num.sendKeys(editProviderData.valid.mobile_number);//mobile number
   await EditProvider.address.clear();
   await EditProvider.address.sendKeys(editProviderData.valid.address);//address
   await EditProvider.city.clear();
   await EditProvider.city.sendKeys(editProviderData.valid.city);//city
   await EditProvider.state.clear();
   await EditProvider.state.sendKeys(editProviderData.valid.state);//state
   await EditProvider.country.clear();
   await EditProvider.country.sendKeys(editProviderData.valid.country);//country
   await EditProvider.zip_Code.clear();
   await EditProvider.zip_Code.sendKeys(editProviderData.valid.zipCode);//zip code
   //select smart phone
  //  await EditProvider.phoneType_dropdown.click();
  //  await EditProvider.select_android.click();
   //enter specilization
   await EditProvider.specilization.clear();
   await EditProvider.specilization.sendKeys(editProviderData.valid.specilization);
   //enter licence
   await EditProvider.licence.clear();
   await EditProvider.licence.sendKeys(editProviderData.valid.licence);
   //upload image
   await EditProvider.upload_ImgBtn.sendKeys(editProviderData.img_path);
   await browser.sleep(10000);
   //click on submit button
   await EditProvider.submitBtn.click();
   await browser.sleep(2000);

});
//then verify the error messages
Then('verify the success message after sumbit', async function () {
  //verify whether page returned to providerlist page and verify the success message
  expect(await browser.getTitle()).to.equal(homepage.homepage_title);
  //verify succcess message
  expect(await EditProvider.success_msg.isDisplayed()).to.equal(true);
  expect(await EditProvider.success_msg.getText()).to.equal(editProviderData.succsess_msg);
  await browser.sleep(5000);

});
//4.edit the details with valid data in required fields
When('I\'m on edit provider page Enter valid data in required fields and click on submit then verify the success message',async function () {
  await Homepage.search_provider.sendKeys(editProviderData.providerName);//enter the provider name
  await EditProvider.edit_icon.click();
  await browser.sleep(2000);
  await EditProvider.firstName.clear();
  await EditProvider.firstName.sendKeys(editProviderData.valid_info2.first_name);//first name
  await EditProvider.lastName.clear();
  await EditProvider.lastName.sendKeys(editProviderData.valid_info2.last_name);//last name
  await EditProvider.mobile_num.clear();
  await EditProvider.mobile_num.sendKeys(editProviderData.valid_info2.mobile_number);//mobile number
  await EditProvider.address.clear();
  await EditProvider.address.sendKeys(editProviderData.valid_info2.address);//address
  await EditProvider.zip_Code.clear();
  await EditProvider.zip_Code.sendKeys(editProviderData.valid_info2.zipCode);//zip code
  //enter licence
  await EditProvider.licence.clear();
  await EditProvider.licence.sendKeys(editProviderData.valid_info2.licence);
  //click on submit button
  await EditProvider.submitBtn.click();
  await browser.sleep(2000);
 //verify whether page returned to providerlist page and verify the success message
 expect(await browser.getTitle()).to.equal(homepage.homepage_title);
 //verify succcess message
 expect(await EditProvider.success_msg.isDisplayed()).to.equal(true);
 expect(await EditProvider.success_msg.getText()).to.equal(editProviderData.succsess_msg);

});

 