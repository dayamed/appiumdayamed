import {After, Before, Status} from "cucumber";
import { browser } from "protractor";
import { login } from "../pageobjects/login";
let Login=new login();

//This hook will be executed before each scenario
Before(function () {
    browser.driver.manage().window().maximize();
});

/* //This hook will be executed before scenarios tagged with @sanity
Before({tags: "@sanity"}, function () {
    browser.driver.manage().window().maximize();
  });   

//This hook will be executed before scenarios tagged with @regression
Before({tags: "@regression"}, function () {    
    browser.driver.manage().window().maximize();
});   */


//This hook will be executed after each of the scenario
After(async function (scenario) {
    if(scenario.result.status==Status.FAILED||scenario.result.status==Status.PASSED){
        //code to take screenshot
        const screenshot = await browser.takeScreenshot();
        this.attach(screenshot,"image/png");
    }
  });

/*   After(async function () {
    await Login.userImage.click();
    browser.sleep(1000);
    await Login.Logout.click();
  });   */